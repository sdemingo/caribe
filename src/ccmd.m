Ccmd : module
{

	PATH	: con "ccmd.dis";
	NS	: con "/n/caribe";
	HOME	: con "/usr/sdemingo/lib/caribe";

	MAX_VOL_ARGS	: con 4;
	MIN_NUM_ARGS	: con 2;

	Ctlcmd: type ref fn(argv: array of string): string;

	Ctl: adt
	{
		name:	string;
		cmd:	Ctlcmd;
	};

	ctlcmds: array of Ctl;

	init 	:fn(d:Dat);
	start	:fn(next:chan of int,flags:ref Utils->Cmdargs);
	exec	:fn(cmd:string):string;

};


