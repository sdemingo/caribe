implement Cproxy;

include "mods.m";
	Connection,FileIO,Rwrite,Rread,fprint,sprint,read,write,NEWPGRP:import sys;
	stderr,stdout,log,CSP_PORT,CARIBE_VOLS,sysname,panic,cloclog,
		writeline,splitaddr,cproxylog,killgrp:import utils;
	Attributes,Service	: import registries;
	cspsrv,Cmsg,Cop,cspopen,csprpc:import csp;
	Evt: import cevent;
	Tree:import ctree;
	getenv:import env;
	OP,INIT,SYNC,CORR:import creplica;
	Tmsg, Rmsg :import styx;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	Xfs,xreply:import xsrv;
	Ptab:import procs;



MAX_MVOLS	: con 16;
POOL_REFRESH	: con 3000;
VERS_REFRESH	: con 2000;

CMD_MOUNT	: con "mount";
CMD_UNMOUNT	: con "unmount";
CMD_UPDATE	: con "update";


mtab	: array of ref Mvol;
mitems	: int;
mvreq	: chan of string;
mvrep	: chan of ref Mvol;

evchan	: chan of ref Evt;
mod_pid:int;

	#
	#	Module interface
	#	



init (d:Dat):string
{
	initmods(d->mods);
	return start();
}



start ():string
{
	ex:=chan of string;
	spawn runmod(ex);
	return <-ex;
}

runmod(e:chan of string)
{
	mod_pid	= sys->pctl(sys->NEWPGRP ,nil);
	mvreq	= chan of string;
	mvrep	= chan of ref Mvol;
	mtab	= array [MAX_MVOLS] of ref Mvol;
	mitems=0;

	evchan=chan of ref Evt;

	spawn mtabproc (mvreq,mvrep);
	spawn eventproc(evchan);

	e<-=nil;
}


halt():string
{
	# Kill all mvols threads
	for (i:=0; i<mitems; i++)
		if (mtab[i]!=nil) 
			mtab[i].free();


	# Kill the rest of the threads of cproxy
	killgrp(mod_pid);

	return nil;
}


mount(volume:string):string
{
	err:string=nil;

	# Busco el proxy en el mtab a traves de mtabproc

	mv:=getmvol(volume);
	if (mv==nil)
		return sprint("Cproxy cannot mount volume %s",volume);

	reply_c:=chan of string;
	mv.ctl_c<-=(CMD_MOUNT,reply_c);
	err=<-reply_c;

	if (err!=nil)
		log (cproxylog, sprint("cproxy->mount() end with errors: %s",err));

	return err;
}


unmount (volume:string):string
{
	err:string=nil;

	# Busco el proxy en el mtab a traves de mtabproc

	mv:=getmvol(volume);
	if (mv==nil)
		return sprint("Cproxy cannot mount volume %s",volume);

	log (cproxylog, sprint("Unmount and killing proxy for %s",volume));
	killgrp(mv.gid);

	return err;
}




update (volume:string):string
{
	err:string=nil;

	# Busco el proxy en el mtab a traves de mtabproc

	## log (stdout,"Inicio cproxy->update");
	mv:=getmvol(volume);
	if (mv==nil)
		return sprint("Cproxy cannot update volume %s",volume);

	reply_c:=chan of string;
	mv.ctl_c<-=(CMD_UPDATE,reply_c);
	err=<-reply_c;
	## log (stdout,"Fin cproxy->update");

	return err;
}






	#
	# Private implemetation
	#


Mcon.new(mv:ref Mvol, c : sys->Connection ):ref Mcon
{
	m:=ref Mcon;

	m.exit_c=chan of int;
	m.gid=sys->pctl(NEWPGRP,nil);

	mv.con_procs.add(m.gid);

	navigator:=Navigator.new(mv.nav_c);
	m.fd=sys->open(c.dir + "/data", Sys->ORDWR);
	(m.tc,m.srv)= Styxserver.new(m.fd,navigator,big 0);
	(m.addr,m.port)=utils->getclient(c);

	dc := chan of ref Tmsg;
	spawn recvproc (m,dc);
	spawn dispacher (mv,m,dc);

	return m;

}



Mvol.new (volume:string):ref Mvol
{
	mv:=ref Mvol;

	mv.volume=volume;
	c:=chan of string;
	spawn newvolproc(mv,c);
	<-c;

	return mv;	
}


newvolproc(mv: ref Mvol,c:chan of string)
{
	err:string = nil;

	mv.gid=sys->pctl(sys->NEWPGRP ,nil);
	mv.con_procs=Ptab.new();

	mv.ctl_c=chan of (string,chan of string);
	mv.replica=utils->CARIBE_NS+"/"+mv.volume+"/replica";
	mv.target="";

	(tree,navc):=ctree->start(mv.replica);
	mv.nav_c=navc;
	mv.xfs=Xfs.new("cproxy",tree);

	mv.broken=0;
	mv.srvport=utils->PROXY_PORT+mitems;

	spawn ctlproc(mv);

	#
	# Busco y monto una replica final a la que reenviar los mensajes
	#

	reply_c:=chan of string;
	mv.ctl_c<-=(CMD_MOUNT,reply_c); 
	err=<-reply_c;	


	#
	# Ahora, arranco el servidor styx que me dara acceso hacia el 
	# xfs para comunicarme con la replica que he encontrado
	#

	ec:=chan of string;
	spawn styxsrv(mv,ec);
	err=<-ec;
	if (err!=nil)
	{
		mv=nil;
		c<-=sprint("New mvol: Error at served proxy of %s. %s",mv.volume,err);
		return;
	}	


	#
	# Ahora monto el styxserver local en /files
	#

	mv.srv=utils->CARIBE_NS+"/"+mv.volume+"/files";
	srvaddr:="tcp!"+utils->sysname()+"!"+string mv.srvport;
	err=creplica->mount(srvaddr,mv.srv);
	if (err!=nil)
	{
		mv=nil;
		c<-=sprint("New mvol: Error at mount replica %s. %s",mv.volume,err);;
		return;
	}

	log (cproxylog, "New mvol: styxserver mounted at /files");

	spawn versproc(mv);
	
	c<-=nil;
}



Mvol.free(mv:self ref Mvol)
{

	log (cproxylog, sprint("Freeing mvol of %s",mv.volume));

	# unmount /replica
	if (creplica->unmount(mv.replica)!=nil)
		log(cproxylog,sprint("Unmount replica to %s failed",mv.volume));

	# unmount /files
	if (creplica->unmount(mv.srv)!=nil)
		log(cproxylog,sprint("Unmount cproxy to %s failed",mv.volume));

	# Kill opened mcons on this Mvol
	c:=mv.con_procs.get();
	for (i:=0; i<len(c); i++)
		killgrp(c[i]);

	log (cproxylog, sprint("Free mvol of %s",mv.volume));

	# Kill this Mvol
	killgrp(mv.gid);
}



#
# Este proceso se encarga de gestionar el array de Mvols y evitar
# asi condiciones de carrera. 
#

mtabproc(reqchan:chan of string, repchan: chan of ref Mvol)
{
	for(;;)
	{
		volume:=<-reqchan;

		if (volume==nil)
			break;

		mv: ref Mvol = nil;
		for (i:=0; i<mitems; i++)
			if ((mtab[i]!=nil) && (mtab[i].volume==volume))
				mv=mtab[i];

		if ((mv==nil) && (mitems<MAX_MVOLS))
		{
			mv=Mvol.new(volume);
			mtab[mitems]=mv;
			mitems++;
		}
		
		# contestamos con el mvol pedido

		repchan<-=mv;
	}
}


getmvol(volume:string):ref Mvol
{
	mvreq<-=volume;
	mv:=<-mvrep;
	return mv;	
}




#
# Este proceso se encarga de gestionar el proxy. 
#

ctlproc(mv:ref Mvol)
{
	for (;;)
	{
		(cmd,reply_c):=<-mv.ctl_c;
		if (cmd==CMD_MOUNT)
			mount_cmd(mv,reply_c);

		if (cmd==CMD_UPDATE)
			update_cmd(mv,reply_c);
	}
}

#
# Este proceso se encarga de controlar la versión de la replica
# y actualizar el propio proxy si nos hemos quedado desactualizados
#

versproc(mv:ref Mvol)
{
	# Hemos de conseguir un fd hacia el servicio csp de la replica
	# y enviarle periodicamente un VERS_REQ como hacen los propios modulos

	(v,err):=versreq(mv);

	for (;;)
	{
		(v_new,err):=versreq(mv);
		if (v_new!=v)
		{
			reply_c:=chan of string;
			mv.ctl_c<-=(CMD_UPDATE,reply_c);
			err=<-reply_c;
			if (err!=nil)
				log(cproxylog,sprint("Update cproxy error %s",err));
		}
		v=v_new;
		sys->sleep(VERS_REFRESH);	
	}
}






update_cmd(mv:ref Mvol, reply_c:chan of string)
{
	if (mv!=nil)
		reply_c<-=mv.xfs.xupload();
	else
		reply_c<-=nil;
}



mount_cmd(mv:ref Mvol, reply_c:chan of string)
{
	mv.broken=1;
		
	log (cproxylog,sprint("mountproc: searching a new replica of %s",mv.volume));
	mr:ref Registries->Service;
	err:string=nil;

	for(;;){
		log (cproxylog,"pooling .... ");
		(mr,err)=pool(mv.volume);
		if ((mr!=nil) && (err==nil))
			break;
		log(cproxylog,err);
		sys->sleep(POOL_REFRESH);
	}

	log (cproxylog,sprint("mountproc: find new replica on %s",mr.addr));

	# Monto la nueva replica sobre mv.replica (/replica)
	(p,addr,nvol):=splitaddr(mr.addr);	#c, peer name, vol name
	port:=mr.attrs.get("port");
	fs:="tcp!"+addr+"!"+string port;

	if (creplica->unmount(mv.replica)!=nil)
		log(cproxylog,sprint("Unmount replica to %s failed",mv.volume));

	err=creplica->mount(fs,mv.replica);
	if (err!=nil)
	{
		reply_c<-=err;
	}
	else
	{

		# Actualizo los ficheros de /replica para que las peticiones
		# que me llegan a traves de /files y que son redigiridas 
		# a /replica ya muestren la info actualizada

		mv.xfs.xupload();
		log (cproxylog,"Actualizo /replica con la nueva replica");

		mv.broken=0;
		mv.target=addr;

		rep_csp:="tcp!"+addr+"!"+string (utils->CSP_PORT);
		fd:=cspopen(rep_csp);
		mv.rep_fd=fd;

		reply_c<-=nil;

		log (cproxylog,sprint("mountproc: replica %s is now mounted on /replica",addr));
	}
}



styxsrv(mv:ref Mvol,error:chan of string)
{
	(e,conn) := sys->announce("tcp!*!"+string mv.srvport);
	if (e<0)
	{
		error<-="announced failed";
		exit;	
	}

	error<-=nil;

	log (cproxylog, "styxserver starting ...");

	while ()
	{
		(er,c) := sys->listen(conn);
		if (er<0)
		{
			log (cproxylog,"listen failed");
			exit;	
		}		
		spawn clientproc(mv,c);
	}
}




clientproc(mv:ref Mvol,conn:Connection)
{
	mcon:=Mcon.new(mv,conn);
	<-mcon.exit_c;
	log (cproxylog,sprint("End connection from client %s",mcon.addr));

	mv.con_procs.del(mcon.gid);
	killgrp(mcon.gid);
}


recvproc(mc:ref Mcon, disc:chan of ref Tmsg)
{
	while((tmsg:=<-mc.tc)!=nil)
		disc<-=tmsg;
}


tryagain(mv:ref Mvol)
{
	reply_c:=chan of string;

	for(;;)
	{
		mv.ctl_c<-=(CMD_MOUNT,reply_c);
		err:=<-reply_c;
		if (err==nil)
			break;	
		log(cproxylog,err);
	}
}




dispacher(mv:ref Mvol,mc:ref Mcon, disc:chan of ref Tmsg)
{
	end:=0;

	for(;;){
		if (end)
			break;

		gm:=<-disc;

		#log (cproxylog,sprint("<- %s",gm.text()));

		err:string = nil;

		if (mv.broken)
			xreply(mc.srv,ref Rmsg.Error(gm.tag, "broken replica pipe. Wait"));
		else
		{
			pick m := gm {	
			Read =>
				err=mv.xfs.xread (mc.srv,m);

			Write =>
				err=mv.xfs.xwrite (mc.srv,m);

			Create =>
				err=mv.xfs.xcreate (mc.srv,m);

			Remove =>
				err=mv.xfs.xremove(mc.srv,m);

			Wstat =>
				err=mv.xfs.xwstat (mc.srv,m);
	
			Clunk =>
				err=mv.xfs.xclunk (mc.srv,m);
				if ((len mc.srv.allfids())==0)
					end=1;

			Stat =>
				err=mv.xfs.xstat(mc.srv,m);
	
			Walk =>
				err=mv.xfs.xwalk(mc.srv,m);

			Open =>	
				err=mv.xfs.xopen(mc.srv,m);

			Attach =>
				err=mv.xfs.xattach(mc.srv,m);

			* =>
				mc.srv.default(m);
			}#pick

			if ((err!=nil) && (!mv.broken))
			{
				log(cproxylog,sprint("CProxy dispacher: %s. Broken connection to replica",err));
				spawn tryagain(mv);
			}
		}
	}

	mc.exit_c<-=end;	# Finalizamos el Mcon
}



pool(volume:string):(ref Registries->Service,string)
{
	err:string=nil;
	loc:=cloc->where(sysname());
	if (loc==nil)
		return (nil,"No loc where look for replicas");

	area: list of ref Registries->Service;

	# Busco un peer adecuado. Uso esa prioridad:
	# 	1) Me busco a mi mismo como replica de ese volumen
	#	2) Busco a cualquier replica de mi isla
	#	3) Busco a cualquier replica en cualquier otra isla

	log (cproxylog, "Pooling myself to find a new replica ...");
	area=cns->find(("vol",volume)::("sys",utils->sysname())::
			("status",string OP)::nil);
	if (area!=nil){
		rep:=hd area;
		if (rep!=nil)
		{
			err=mountreq(volume,rep.addr);
			if (err==nil)
				return (rep,err);
		}	
	}

	log (cproxylog, "Pooling island to find a new replica ...");
	area=cns->find(("vol",volume)::("loc",loc)::("status",string OP)::nil);
	if (area!=nil){
		for (rep:=hd area; area!=nil; area=tl area)
		{
			err=mountreq(volume,rep.addr);
			if (err==nil)
				return (rep,err);
		}		
	}

	log (cproxylog, "Pooling the sea to find a new replica ...");
	area=cns->find(("vol",volume)::("status",string OP)::nil);
	if (area!=nil){
		for (rep:=hd area; area!=nil; area=tl area)
		{
			err=mountreq(volume,rep.addr);
			if (err==nil)
				return (rep,err);
		}		
	}	

	return 	(nil,"All replicas rejected REPL_MOUNT request");
}






# 
#	Peticiones CSP a la replica
#


mountreq(volume:string,addr:string):string
{
	log (cproxylog,sprint("Send REPL_MOUNT to %s",addr));

	(nil,raddr,nvol):=splitaddr(addr);		#c, peer name, vol name
	mcsp:="tcp!"+raddr+"!"+string (utils->CSP_PORT);
	fd:=cspopen(mcsp);
	if (fd==nil)
		return "No replica fd to mount";

	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_MOUNT,data));

	log (cproxylog,sprint("Receive REPL_MOUNT reply from %s",addr));

	if ((rm!=nil) && (rm.cmd==csp->OK))
		return nil;
	else
		return "Failed vers request to replica";
}



versreq(mv:ref Mvol):(big,string)
{
	if (mv.rep_fd==nil)
		return (big -1,"Replica fd is nil");

	data:=str->quoted(utils->sysname()::mv.volume::nil);
	(err,rm):=csprpc(mv.rep_fd,ref Cmsg(csp->REPL_VERS,data));
	if ((rm==nil) || (rm.cmd!=csp->OK))
		return (big -1,"Failed vers request to peer");

	v:=big rm.data;
	return (v,nil);
}







# Only for debug

fid2file(srv: ref Styxserver, tree:ref Ctree->Tree, fid:int):string
{
	c := srv.getfid(fid);
	if (c != nil)
		return tree.getpath(c.path);
	else
		return nil;
}




event(e:ref Cevent->Evt)
{
	if (evchan!=nil)
		evchan<-=e;
}



eventproc(evchan:chan of ref Evt)
{
	for (;;)
	{
		e:=<-evchan;	
		eventcc(e);
	}
}




eventcc(e:ref Evt)
{
	if (e==nil)
		return;

	#log (cproxylog,sprint("Evento %s",e.text()));

	case (e.cod)
	{
	cevent->REPL_STATUS_CHANGED =>
		ea:=e.argv();
		if (ea==nil) 
			return;	

		vol:=ea[0];
		cc:=ea[1];
		newstatus:=int ea[2];
		if (newstatus==creplica->OP)
		{
			mount(vol);
		}
			#mv:=getmvol(vol);
			#if ((mv!=nil) && (mv.target != utils->sysname()))
			#{
			#	log(cproxylog,"STATUS_CHANGED: Mi replica actual es remota. Busco nuevas replicas OP");
			#	mount(vol);
			#}
			#else
			#{
			#	log(cproxylog,"STATUS_CHANGED: Mi replica actual es local. Ignoro nuevas");#
			#}
		#}




	cevent->REPL_TREE_DUMP =>	# For debug
		ea:=e.argv();
		if (ea==nil) 
			return;	
		vol:=ea[0];
		mv:=getmvol(vol);
	}
	

}


