 implement Cloc;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,fprint,sprint:import sys;
	stderr,stdout,log,CSP_PORT,sysname,panic,cloclog,splitaddr,
			Cmdargs,killgrp:import utils;
	Service,Attributes:import registries;
	find : import cns;
	rand : import rnd;
	Hash: import hashtable;
	cspopen, Cmsg,csprpc: import csp;
	Evt, LOC_CHANGED,CNS_OFFLINE: import cevent;



mod_pid	:int;
itable: ref Hash[string];
itlock	:chan of int;
evchan	: chan of ref Evt;

ISLAND_MAX_NUM		: con 100;
ISLAND_MIN_SIZE		: con 5;
ISLAND_MAX_ID		: con 100;
ISLAND_AREA		: con 500;	# ms
NUM_SAMPLES_REPLACED	: con 10;

ITABLE_UPDATE_TIME	: con 2000;	# Actualizar distancias

NAT_CHECK_TIME		: con 60000;


init (d:Dat,flags:ref Cmdargs):string
{
	initmods(d->mods);
	return start(flags);
}


start(flags:ref Utils->Cmdargs):string
{
	spawn runmod(flags);
	return nil;
}

halt()
{
	killgrp(mod_pid);
}


runmod(flags:ref Utils->Cmdargs)
{
	mod_pid=sys->pctl(sys->NEWPGRP ,nil);
	#itable=hash->new(ISLAND_MAX_NUM);
	itable=Hash[string].new(ISLAND_MAX_NUM);
	if (flags.static)
	{
		log(cloclog,"Static mode. Island reallocation is off");
		return;	
	}

	if (flags.nat)
	{
		log(cloclog,"Nat mode. Caribe behind a NAT router");
		spawn natmode();
	}

	myloc:=getloc();
	if (myloc==nil)
		panic("cloc failed. It needs a first loc");

	log (cloclog, sprint ("Localización inicial en isla %d",int myloc));

	# evchan=chan of ref Evt;		# no se usa

	spawn itupdate(itable);
}




sync_itable(itable:ref Hash[string])
{
	#log(cloclog,"Fill&Clear itable");
	# Fill itable with locs found in cns

	att:=("name","csp")::("loc","*")::nil;
	nd:=find(att);
	if (nd!=nil)
	{
		while (nd!=nil)
		{
			n:=hd nd;
			loc_b:=n.attrs.get("loc");
			if (itable.get(loc_b)==nil)	# la isla no esta en la itable
				itable.set(loc_b,string UNKNOW);
			nd=tl nd;
		}
	}

	# Clear itable with locs not found in cns

	h:=itable.all();
	while (h!=nil)
	{
		n:=hd h;
		loc:=n.key;
		l:=find(("name","csp")::("loc",loc)::nil);
		if (l==nil)
			itable.del(loc);
		h=tl h;
	}
}



itupdate(itable: ref Hash[string])
{
	log(cloclog,"Starting island reallocation system");
	samples:=0;

	for(;;)
	{
		sync_itable(itable);	# Sincronizamos itable y registry
		is:=itable.all();

		while (is!=nil)
		{
			n:=hd is;
			loc:=n.key;
			e:=polldist(itable,loc);	# sondeamos la distancia a loc
			if (e!=nil)
				log(cloclog,e);	
			is=tl is;
		}
		samples++;
		if (samples==NUM_SAMPLES_REPLACED)
		{
			placed(itable);
			samples=0;
		}

		#log(stdout,sprint("itable updated\n%s",ittext(itable)));
		
		sys->sleep(ITABLE_UPDATE_TIME);
	}
}


ittext (itable: ref Hash[string]):string
{
	s:="";
	s=s+"\nitable distances:\n";
	is:=itable.all();	
	while (is!=nil)
	{
		n:=hd is;

		# Buscar algunos nodos de esa isla (los candidatos p.e)
		# e imprimirlos junto con la lista

		l:=candidates(n.key);
		
		for (strl:=""; l!=nil; l=tl l)
			strl=strl+(hd l).addr+",";
		
		s=s+sprint("%s	%s\n",n.key,n.val);	
		is=tl is;
	}
	return s;
}




placed(itable: ref Hash[string])
{
	is:=itable.all();

	best:=UNKNOW;
	myloc:=getloc();
	old:=myloc;		
	newlocs:list of string = nil;

	# Localizo las islas mas cercanas. Aquellas que 
	# cumplen la regla de cercania: ( dist_a_isla < ISLAND_AREA )

	while (is!=nil)
	{
		island:=hd is;
		d:=int(island.val);
		if ((d < best) 
			&& (d < ISLAND_AREA) 
			&& (island.key!=myloc)){
			newlocs=island.key::newlocs;
		}
		is=tl is;
	}

	if (newlocs!=nil)
	{
		# Eligo una de entre las mas cercanas
		new:=chooseloc(myloc,newlocs);
		if (old!=new)
		{
			log (cloclog,"Me mudo a una isla cercana");
			changeloc(old,new);
		}
	}
	else
	{
		# No hay islas cercanas. 
		my_island:=itable.get(myloc);
		d:=int(my_island);
		if (d >= ISLAND_AREA)
		{
			log (cloclog,"Distancia a mi isla excesiva. Me mudo a una propia");
			changeloc(old,newlabel());
		}

	}
}


changeloc(old:string,new:string)
{
	if ((new==nil) || (old==nil))
		return;	

	if (setloc(new)<0)
		panic("cloc failed at update loc file");

	log (stdout,sprint("Cambio de loc a isla %s",new));
	ccmd->exec("restart");
}


chooseloc(now:string, newlocs :list of string):string
{

	# Encontre una o varias cerca
	# Deberia unirme a la mas grande

	bigger:=islandnodes(now);
	new:=now;
	alocs:=newlocs;
	while (alocs!=nil)
	{
		# Si la candidata tiene mas nodos
		# que la mayor encontrada entonces
		# por ahora esa sera mi eleccion

		cand:=hd alocs;
		ncand:=islandnodes(cand);
		if ( ncand > bigger)
		{
			bigger=ncand;
			new=cand;
		}
		alocs=tl alocs;
	}

	alocs=newlocs;

	# No encontre ninguna mas grande. Las cercanas a mi, son 
	# islas más pequeñas o iguales que la mia. Haré que todos 
	# elijan la de id más pequeño

	if (now==new) 
	{
		id:=int ISLAND_MAX_ID;
		while (alocs!=nil)
		{
			if ( int (hd alocs) < id )
				id=int (hd alocs);
			alocs=tl alocs;
		}
		new=string id;
	}

	return new;
}



polldist (itable: ref Hash[string], island : string):string
{	
	chosen:=candidates(island);
	nchosen:int;
	if (chosen==nil)
		nchosen=0;
	else
		nchosen=len chosen;

	nnodes:=islandnodes(island);

	av:int;

	## 1.Medimos la distancia a estos y calculamos la media (M)
	## sin ponderar. Media actual

	if (nchosen==0)
	{
		# Si no encontramos candidatos a una isla la distancia
		# media actual es UNKNOW a no ser que sea nuestra
		# propia isla
		if (island==getloc())
			av=0;
		else
			av=UNKNOW;
	}
	else
	{
		#log (cloclog, sprint ("Distancia a isla %s",island));
		av=0;
		while (chosen!=nil)
		{
			node:=hd chosen;
			(nil,addr,nil):=splitaddr(node.addr);
			d2node:=beacon (addr);
			#log (cloclog, sprint ("\tDistancia a isla %s(%s): %d",island,addr,d2node));
			av=av+d2node;
			chosen=tl chosen;
		}
		av=int (av/nchosen);
		#log (cloclog, sprint ("\tDistancia media a isla %s: %d",island,av));
	}

	#log (cloclog, sprint ("\tDistancia media a isla %s: %d",island,av));


	## 2. Media ponderada con anteriores distancias
	
	hv:=itable.get(island);
	if (hv==nil)	# La isla ya no esta en el mapa de islas
		return sprint ("Island %s not found in the itable",island);

	d_old:=int(hv);
	
	#log (cloclog, sprint ("\tAlfa para nnodes=%d: %f",nnodes,alfa(nnodes)));
	d:=waverage(d_old,av,alfa(nnodes));

	itable.set(island,string(d));

	#log (cloclog, sprint ("\tDistancia media pond a isla %s: %d",island,hv.i));

	return nil;
}


waverage ( d_old: int, d_now:int,alfa:real):int
{
	return  int ((real (d_old) * alfa) + (real d_now)*(1.0-alfa));
}


alfa (n:int):real
{
	if (n==1)	# only one member
		return 0.0;
	if (n<MEDIUM_ISLAND_SIZE)	# small size
		return 0.2;
	if (n<LARGE_ISLAND_SIZE)	#medium size
		return 0.7;
	return 0.85;	# huge size
}


islandnodes(island:string):int
{
	# Deberiamos también discriminar por nombre de volumen?

	nd:=find(("name","csp")::("loc",island)::nil);
	if (nd==nil)
		return 0;
	return len nd;
}


candidates(island:string):list of ref Service
{
	nd:=find(("name","csp")::("loc",island)::nil);
	cand:list of ref Service = nil;

	if (nd==nil)
		return cand;

	schoice:=1;
	if (len nd > ISLAND_MIN_SIZE)
		schoice=0;

	#log (cloclog, sprint("Buscamos candidatos entre los %d nodos de la isla",len nd));
	while (nd!=nil)
	{
		n:=hd nd;
		(nil,addr,nil):=splitaddr(n.addr);
		if (  ((schoice) || (rand(1)))  && (sysname()!=addr )  )
		{
			#log (cloclog, sprint("Añadimos candidato para poolear %s",addr));
			cand=n::cand;
		}
		nd=tl nd;
	}
	#log (cloclog, sprint("Encontrados %d candidatos",len cand));

	return cand;
}


beacon( host : string ): int
{
	addr:="tcp!"+host+"!"+string CSP_PORT;
	fd:=cspopen(addr);
	if (fd==nil)
		return UNKNOW;

	stamp1:=sys->millisec();
	(err,r):=csprpc(fd,ref Cmsg(csp->LOC_BEACON,utils->sysname()));
	stamp2:=sys->millisec();
	if (r==nil)
		return UNKNOW;		
	if (r.cmd!=csp->OK)
		return UNKNOW;

	return stamp2 - stamp1;
}




setloc(loc:string):int
{
	fd:ref sys->FD;
	(er,d):=sys->stat(utils->CARIBE_LOC);
	if (er<0)
		fd=sys->create(utils->CARIBE_LOC,sys->OWRITE,8r644);
	else
		fd=sys->open(utils->CARIBE_LOC,sys->OWRITE | sys->OTRUNC);

	if (fd==nil)
		return -1;

	buf:=array of byte loc;
	n:=sys->write(fd,buf, len buf);
	if (n!=len buf)
		return -1;

	return 0;
}


getloc():string
{
	fd:=sys->open(utils->CARIBE_LOC,sys->OREAD);
	if (fd==nil)
		return nil;

	buf:=array [LOC_MAX_LEN] of byte;
	n:=sys->read(fd,buf, len buf);
	if (n<0)
		return nil;

	return string buf[:n];
}


newlabel():string
{
	id:=0;
	sv:list of ref Registries->Service;
	do
	{
		id=rand (ISLAND_MAX_ID);
		sv:=find(("loc",string id)::nil);
	}
	while (sv!=nil);
	
	return string id;
}







###
### 	 Public methods
###


where(addr:string):string
{
	if (addr==utils->sysname())
	{

		loc:=getloc();
		if (loc==nil)
			panic("cloc failed. No loc file");

		return loc;
	}
	else
	{
		## ERROR: debo encontrar de otra
		## manera la loc de una maquina
		## que no soy yo a partir de su addr

		att:=("addr",addr)::nil;
		sv:=find (att);
		if (sv==nil)
			return nil;
		else
			return (hd sv).addr;
	}
}



distance (island:string):int
{
	hv:=itable.get(island);
	if (hv!=nil)
		return int(hv);
	else
		return UNKNOW;		
}


map():list of (string,int)
{
	li:=itable.all();
	lo:list of (string,int) = nil;
	while (li!=nil)
	{
		n:=hd li;
		lo=(n.key,int(n.val))::lo;
		li=tl li;
	}
	return lo;
}



sort(srvlist:list of ref Service):list of ref Service
{
	aux:=srvlist;
	dsort : array of ref Service;
	dsort = array [len srvlist] of ref Service;

	dnode:=0;
	dmin:=0;

	while (aux!=nil)		
	{
		node:=hd aux;
		dsort=sort_insertion(node,dsort);
		aux=tl aux;
	}

	lsort : list of ref Service = nil;
	for (i:=(len dsort)-1;i>=0;i--)
		lsort=dsort[i]::lsort;

	return lsort;
}


sort_insertion (node:ref Service, sort: array of ref Service):array of ref Service
{
	dnode:=distance(node.attrs.get("loc"));
	dmin:=0;
	pos:=0;

	if (sort[0]==nil)
	{
		pos=0;
	}
	else
	{
		for (i:=0;i<len sort;i++)
		{
			if (sort[i]==nil)
			{
				pos=i;
				break;
			}

			dmin=distance(sort[i].attrs.get("loc"));
			if (dnode < dmin)
			{
				pos=i;
				break;
			}
		}
	}

	for (i:=(len sort)-1; i>pos; i--)	
		sort[i]=sort[i-1];
	sort[pos]=node;

	return sort;
}





print():string
{
	return ittext(itable);
}





##
##	Gestion de eventos
##



event(e:ref Cevent->Evt)
{
	#if (evchan!=nil)
	#	evchan<-=e;
}



eventproc(evchan:chan of ref Evt)
{

}



natmode ()
{
	log (cloclog,"Nat mode active. Not implementing");

	ip:=utils->natip();
	for (;;)
	{
		sys->sleep(NAT_CHECK_TIME);
		newip:=utils->natip();
		if (newip!=ip)
			changeloc(nil,nil);
	}
}