implement Cns;

include "mods.m";
	Registry,Service, Registered,Attributes:import registries;
	FD,sprint: import sys;
	stderr,cnslog, stdout,log,killgrp:import utils;
	Evt : import cevent;
	OP,INIT,SYNC,CORR:import creplica;
	Hash: import hashtable;



DbValue: adt{
	srv: ref Service;
	fd: ref FD;
};

RDbValue: type ref DbValue;

DB_INIT_SIZE:	con 10;
TIMEOUT_LOCK:	con 10000;

db: ref Hash[RDbValue];
vers:int;
mod_pid:int;

# Channels to Operations
reqc: chan of (string,string, list of (string,string));
repc: chan of (ref Service, string);




init (d:Dat)
{
	initmods(d->mods);
	registries = load Registries Registries->PATH;
	registries->init();
	start();
}


start()
{
	spawn runmod();
}


runmod()
{
	mod_pid=sys->pctl(sys->NEWPGRP ,nil);
	vers=0;

	reqc=chan of (string,string, list of (string,string)); 
	repc=chan of (ref Service, string);

	spawn regproc(reqc,repc);
	spawn versproc();	
}


halt()
{
	killgrp(mod_pid);
}



key ( s: list of string):string
{
	a:=s;
	out:="";
	while (a!=nil)
	{
		w:=hd a;
		out=out+w+"!";
		a=tl a;
	}

	return out[:len out-1];
}




version():int
{
	return vers;
}



register (key:string, att: list of (string,string)):ref Service
{
	reqc<-=("register",key,att);
	(srv,err):=<-repc;
	return srv;
}


update (key:string, atts: list of (string,string)): string
{
	reqc<-=("update",key,atts);
	(srv,err):=<-repc;
	return err;
}



unregister(key:string):string
{
	reqc<-=("unregister",key,nil);
	(srv,err):=<-repc;
	return err;
}



find (att: list of (string,string)):list of ref Service
{
	(att1,val1):=hd att;
	if (att1=="addr")
	{
		(s,e):=findaddr(val1);
		if (e!=nil)
			return nil;
		else
			return s::nil;
	}

	reg:=Registry.new("/mnt/registry");
	if (reg==nil)
		return nil;

	(svc,e):=reg.find(att);
	if (svc==nil)
		return nil;

	return sort_services(svc);
}



#
#	Threads
#




regproc(req:chan of (string,string, list of (string,string)),
	rep:chan of (ref Service, string))
{
	db=Hash[RDbValue].new(DB_INIT_SIZE);

	for (;;){
		(op,key,att):=<-req;

		srv:ref Service;
		err:string=nil;

		case (op)
		{
		"register"=>
			(srv,err)=register_op(key,att);
		"update" =>
			(srv,err)=update_op(key,att);
		"unregister" =>
			(srv,err)=unregister_op(key);
		}

		if (err!=nil)
			log (cnslog,sprint("Error in cns op: %s\n",err));

		rep<-=(srv,err);	
	}



}



versproc()
{
	for(;;)
	{
		sz:=8;
		f:=array [sz] of byte;
		fd:=sys->open("/mnt/registry/event",sys->OREAD);
		if (fd==nil)
			continue;
	
		n:=sys->read(fd,f,sz);
		if (n>0)
		{
			v := string f[:n];
			vers=int v;
		}
	}
}






###
### Private Operations
###

register_op (key:string, att: list of (string,string)):(ref Service,string)
{
	log (cnslog,sprint("Registering service: %s",key));

	e:string=nil;
	dbv2:=db.get(key);

	if (dbv2!=nil)	# El nodo ya existe en nuestra db
	{
		(nil,e)=update_op(key,att);
		if (e!=nil)
			return (nil,e);
		else
			return (dbv2.srv,nil);
	}
	
	
	s:= ref Service;
	s.addr=key;
	s.attrs=Attributes.new(att);
	fd:ref FD;


	(c,addr,srv):=utils->splitaddr(key);
	if (srv=="csp")
	{
		(fd,e)=remote_register(s);
		if (e!=nil)
			return (nil,e);
	}
	else
	{
		(fd,e)=remote_register(s);
		if (e!=nil)
			return (nil,e);

		# Ahora alguna entrada mas con status init. Si la hay,
		# aborto el proceso de registro

		l:=find (("status",string INIT)::nil);
		if ((l!=nil) && (len(l)>1))	# hay otra replica iniciando
		{
			return (nil,"Other replica with status INIT. Aborted");
		}
	}

	
	dbv:=ref DbValue;
	dbv.fd=fd;
	dbv.srv=s;
	db.set(key,dbv);

	return (s,nil);
}





remote_register(srv: ref Service):(ref FD,string)
{

	reg:=Registry.new("/mnt/registry");
	if (reg==nil)
	{
		log (cnslog,sprint("Cns offline mode"));
		return (nil,"Remote registry not respond. Offline mode");
	}

	eu:=reg.unregister(srv.addr);  # Por si existe y la estamos actualizando

	# Registramos
	(r, e) := reg.register(srv.addr, srv.attrs, 0);
	if(e != nil)
	{	
		log (cnslog,sprint("Cns offline mode"));		
		return (nil,"Remote registry not respond. Offline mode");
	}

	return (r.fd,nil);
}



update_op (key:string, atts: list of (string,string)): (ref Service,string)
{
	log (cnslog,sprint("Updating service: %s",key));

	dbv:=db.get(key);
	if (dbv==nil){
			return (nil,"Key service not found in local database");
	}

	dbv.srv.attrs=Attributes.new(atts);	# actualizamos los atributos en nuestra db
	aux:=atts;
	if (dbv!=nil)
	{
		att,val,s:string = nil;
		toks:list of string=nil;
		while (aux!=nil){
			(a,v):=hd aux;
			toks=a::v::toks;
			aux=tl aux;
		}
		s=str->quoted(toks);

		if (dbv.fd!=nil){	# actualizo en el registry
			data:=array of byte s;	
			n:=sys->write(dbv.fd,data,len data);
			if (n>=0){
				log(cnslog,sprint("Registry info updated for : %s",key));
				return (dbv.srv,nil);
			}else{
				return (nil,sprint("Failed at update: %r"));
			}
		}
	}

	return (nil,"Key service not found in local database");		
}


unregister_op(key:string):(ref Service,string)
{
	log (cnslog,sprint("Unregistering service: %s",key));

	reg:=Registry.new("/mnt/registry");
	if (reg==nil)
	{
		log (cnslog,sprint("Cns offline mode"));
		return (nil,"Cns offline mode");
	}
	db.del(key);
	return (nil,reg.unregister(key));
}





sort_services(s:list of ref Service):list of ref Service
{
	final: list of ref Service = nil;
	sorted: list of ref Service = nil;

	aux:=s;

	while (aux!=nil)
	{
		node:=hd aux;
		loc:=node.attrs.get("loc");

		# debo ahora comparar loc con todos los almacenados en sorted
		aux2:=sorted;
		minors: list of ref Service = nil;
		while (aux2!=nil)
		{
			node2:=hd aux2;
			loc2:=node2.attrs.get("loc");
			if (loc < loc2)
			{
				# entonces iserto aqui
				aux2=node::aux2;
			}
			else
			{
					
			}
			
			aux2=tl aux2;
		}
	
		aux=tl aux;
	}
	
	return s;
}


findaddr (addr:string): (ref Service,string)
{
	# buscamos en nuestra hash por si la addr es nuestra
	dbv:=db.get(addr);
	if (dbv!=nil)
		return (dbv.srv,nil);

	# buscamos en el registro central
	reg:=Registry.new("/mnt/registry");
	if (reg==nil)
		return (nil, "Cns: registry is nil");

	(svc,e):=reg.services();
	if (svc==nil)
		return (nil, sprint("Cns: service %s not found.",addr));

	while (svc!=nil)
	{
		s:=hd svc;
		if (s.addr==addr)
			return (s,nil);
		svc=tl svc;

	}
	return (nil,sprint("Cns: service %s not found.",addr));
}












