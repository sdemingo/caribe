implement Auto;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,fprint,sprint:import sys;
	stderr,ccmdlog,log,stdout,sysname,autolog,splitaddr:import utils;
	Service,Attributes: import registries;
	cspsrv,Cmsg,Cop,cspopen,csprpc:import csp;
	Db,Dbptr,Dbentry	: import attrdb;
	Replica			: import creplica;
	free			: import cnode;
	Evt		: import cevent;




init (d:Dat)
{
	initmods(d->mods);
}


# necesito un autolog propio para este modulo

quota (rep: ref Replica)
{
	#
	# Control de quota (en kilobytes)
	#
	
	if ( rep.size() > rep.meta.quota)
	{
		log (autolog,sprint ("Quota overflow in volume %s",rep.volume));
		free(rep.volume);
	}	
}



replication (rep: ref Replica)
{
	#
	# Control de replicas minimas y maximas
	#

	area:=cns->find(("vol",rep.volume)::nil);

	if (len area > rep.meta.maxrep)
	{
		e:=rep.volume::"Too many replicas"::nil;
		free(rep.volume);
	}

	if (len area < rep.meta.minrep)
	{
		e:=rep.volume::string len area::nil;
		cnode->event (Evt.new(cevent->REPL_LOW_REPLCTN,e));
	}
}




newcopy (rep: ref Replica):string
{

	# Por ahora no intento solicitar nuevas copias fuera de
	# mi isla. Solo confio en equipos dentro de mi isla. En el 
	# futuro, podria solicitar alguna garantia como un certificado
	# cifrado o similar para pedirle a un nodo que arrancara 
	# una copia

	isle:=cns->find(("loc",rep.loc)::nil);
	if (isle!=nil)	
	{	
		while (isle!=nil)
		{
			c:=hd isle;
			isle=tl isle;
			(p,paddr,nvol):=splitaddr(c.addr);
			if (paddr == utils->sysname())
				continue;
	
			mcsp:="tcp!"+paddr+"!"+string (utils->CSP_PORT);
			fd:=cspopen(mcsp);
			if (fd==nil)
				continue;

			data:=str->quoted(utils->sysname()::rep.volume::nil);
			(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_INIT_NEW,data));
			if ((rm!=nil ) && (rm.cmd==csp->OK))
				return nil;
		}	
	}

	return "Not found any node to start new replica";
}





newpeer (rep: ref Replica):string
{
	#
	# Encuentra el mejor peer
	#

	# log (autolog,sprint ("Busco nuevo peer para %s",rep.volume));

	err: string =nil;
	new:=ref Service;

	loc:=cloc->where(sysname());
	if (loc==nil)
		return "Auto need a valid loc to search a peer";

	#
	# Tanto en mi isla como fuera de ella deberia ir probando con varios
	# nodos segun me fueran rechazando estos
	# 

	isle:=cns->find(("vol",rep.volume)::("loc",loc)::("status","3")::nil);
	while (isle!=nil)
	{
		new=hd isle;
		if (is_new_peer(rep,new))
		{
			if ((err=request_peer(rep.volume, new.addr))==nil)
			{
				rep.peer=new;
				return nil;	# Tengo nuevo peer.
			}
		}
		isle=tl isle;
	}	

	area:=cns->find(("vol",rep.volume)::("status","3")::nil);
	area=cloc->sort(area);	# falta probar
	while (area!=nil)	
	{
		new=hd area;
		if (is_new_peer(rep,new))
		{
			if ((err=request_peer(rep.volume, new.addr))==nil)
			{
				rep.peer=new;
				return nil;	# Tengo nuevo peer.
			}
		}
		area=tl area;
	}

	return err;
}










request_peer(vol:string, addr:string):string
{
	(p,paddr,nvol):=splitaddr(addr);	#c, peer name, vol name
	mcsp:="tcp!"+paddr+"!"+string (utils->CSP_PORT);
	fd:=cspopen(mcsp);
	if (fd==nil)
		return "Failed csp comunication to the peer";

	data:=str->quoted(utils->sysname()::vol::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_PEER,data));
	if ((rm==nil ) || (rm.cmd==csp->ERROR))
		return "No Master found";

	return nil;
}




is_new_peer(rep: ref Replica, new: ref Service):int
{
	# Comprobamos si ya estabamos usando un peer y este 
	# ha cambiado avisamos a node para que renegociemos la coherencia
	# Si aun no tenemos ningun master, no hay nada que verificar
	
	if (new == nil)
		return 0;	# no hay nadie nuevo

	(nil,naddr,nil):=splitaddr(new.addr);
	if (sysname() == naddr)
		return 0;	# el nuevo soy yo mismo

	if (rep.peer == nil)
		return 1;

	nloc:=new.attrs.get("loc");
	mloc:=rep.peer.attrs.get("loc");

	if ((rep.peer.addr == new.addr) && (nloc == mloc))
		return 0;

	return 1;
}






