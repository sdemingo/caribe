implement Cevent;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,sprint:import sys;
	cspsrv,Cmsg:import csp;
	stderr,stdout,ccmdlog,log,sysname:import utils;
	Attributes,Service	: import registries;



QUEUE_SIZE		: con 100;
QUEUE_POOL_TIME		: con 1000;	#ms
qevent: array of array of ref Evt;
qlock:chan of int;
qsize:int;
qhead	: array of int;
qtail	: array of int;



#
# Events descriptions: (text, min args)
#
evtable:=array [ ] of {
	("LOC_CHANGED",1),
	("REPL_QUOTA_OVRFLW",0),
	("REPL_LOW_REPLCTN",2),
	("REPL_RESTART",2),
	("REPL_STATUS_CHANGED",3),
	("REPL_TREE_UPDATED",2),
	("REPL_TREE_DUMP",1),
	("REPL_RENEGOTIATE",1),
	("REPL_ABORT",1),
	("CNS_OFFLINE",0),
	("PANIC_ERROR",0),
};

init (d:Dat):string
{
	initmods(d->mods);
	qsize=QUEUE_SIZE;
	qevent=array [MAXQ] of {* => array[qsize] of ref Evt};
	qlock=chan [1] of int;
	qhead=array [MAXQ] of {* => 0};
	qtail=array [MAXQ] of {* => 0};
	return nil;
}


Evt.new (cod:int, args:list of string):ref Evt
{
	e:=ref Evt;
	e.cod=cod;
	e.stamp=sys->millisec();

	(text,max_args):=evtable[e.cod];
	a:=array [max_args] of string;
	for (i:=0; args!=nil; args=tl args)
		a[i++]=hd args;

	e.args=a;

	return e;
}


Evt.argv (e:self ref Evt):array of string
{
	return e.args;
}



Evt.text(e:self ref Evt):string
{
	if (e.cod>MAX_EVT)
		return nil;

	(text,max_args):=evtable[e.cod];
	for (i:=0; i<len e.args; i++)
		text=text+" "+e.args[i];

	return text;
}


Evt.clone(s:self ref Evt):ref Evt
{
	e:=ref Evt;
	e.cod=s.cod;
	e.stamp=s.stamp;
	e.args=s.args;

	return e;
}

