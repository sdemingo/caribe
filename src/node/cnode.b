implement Cnode;

include "mods.m";
	fprint,sprint,write,read:import sys;
	stderr,stdout,log,CSP_PORT,sysname,sysaddr,
		args,panic,cnodelog, splitaddr,killgrp:import utils;
	Attributes,Service	: import registries;
	Evt		: import cevent;
	Replica, Cmodule, OP,SYNC,CORR,INIT,DIED,
		REPL_SERVICE_PH_TIME,CCNULL: import creplica;
	cspsrv,Cmsg,Cop,cspopen,csprpc:import csp;
	getmaxcc, getcc: import cloader;
	Meta: import cmeta;
	rand : import rnd;
	netdelay	: import cdebug;
	Tree		: import ctree;
	quota,replication,newpeer,newcopy	: import auto;


	
DEFAULT_REPOSITORY	: con "/tmp";
REPLICA_MAX	: con 10;
WAIT_FOR_LOCKS	: con 500;
REPLICA_INIT_TIMEOUT : con 15000;
EVT_BUFFER	: con 10;

storage: array of ref Replica;
storage_lock : chan of int;
rid:int;
nrep:int;


# node chan cmds
ADD_REPLICA	: con "add";
FREE_REPLICA	: con "free";
HALT_NODE	: con "halt";


## chan for communications with nodeproc and eventproc
node_chan: chan of (string,string,chan of int);
evchan: chan of ref Evt;


netdebug:int;
node_pid:int;


init (d:Dat,flags:ref Utils->Cmdargs):string
{
	initmods(d->mods);
	return start(flags);
}




start(flags:ref Utils->Cmdargs):string
{
	ex:=chan of string;
	spawn runmod(flags,ex);
	return <-ex;
}



runmod(flags:ref Utils->Cmdargs,e_c:chan of string)
{
	node_pid=sys->pctl(sys->NEWPGRP ,nil);
	storage=array [REPLICA_MAX] of ref Replica;
	storage_lock=chan [1] of int;
	rid=0;
	nrep=0;

	netdebug=0;
	if ((flags!=nil) && (flags.netdbg))
	{
		log(cnodelog,"Net debug mode. ");
		netdebug=1;;	
	}

	error:=chan of string;	
	spawn cspproc(error);

	node_chan=chan of (string, string, chan of int);
	spawn nodeproc(node_chan);

	e:=<-error;
	if (e!=nil)
	{
		e_c<-=e;
		return;
	}

	e=candidate();

	if (e!=nil)
	{
		e_c<-=e;
		return;
	}

	evchan=chan [EVT_BUFFER] of ref Evt;
	spawn eventproc(evchan);

	e_c<-=nil;
	return;
}




candidate():string
{
	me:=utils->sysname();
	loc:=cloc->where(me);	# verdi si queda pillado en el arranque aqui!
	a:=("name","csp")::("sys",me)::("loc",loc)::nil;

	k:=cns->key("c"::me::"csp"::nil);
	ue:=cns->unregister(k);
	sv:=cns->register(k,a);
	if (sv==nil)
		return "Candidate node register failed";

	return nil;
}
	


##
## Quizas esta deberia ir a auto
##

checkpeer (rep:ref Replica):string
{
	if (rep.root==1)
		return nil;	# soy la raiz.
	
	if (rep.peer==nil)
		return "No peer found";

	(p,paddr,nvol):=splitaddr(rep.peer.addr);	#c, peer name, vol name
	if (paddr==sysname())
		return "I'm my peer";

	# Compruebo que mi peer responde a Csp

	mcsp:="tcp!"+paddr+"!"+string (utils->CSP_PORT);
	fd:=cspopen(mcsp);
	if (fd==nil)
		return "No peer fd";

	data:=str->quoted(utils->sysname()::rep.volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_VERS,data));
	if ( (rm==nil) || (rm.cmd!=csp->OK))
		return "Failed vers request to peer";

	# Compruebo que mi peer esta bien montado

	peermeta:=utils->CARIBE_NS+"/"+rep.volume+"/peer/META";
	(e,d):=sys->stat(peermeta);
	if (e<0)
		return "Peer bad mounted";

	return nil; 
}




replica(volume:string, dir:string):string
{
	# dir es ignorado por ahora y metido a mano durante la creacion
	# de la replica

	#reply:=chan [1] of int;

	node_chan<-=(ADD_REPLICA,volume,nil);

	#<-reply;	# wait for died
	return nil;
}



# ¿Por que usas un buffer de 1 en lugar de un canal std?
# El canal con buffer de 1 no es bloqueante. Cuando en nodeproc
# se escribe la respuesta en el canal reply, lo normal, es que
# el proceso que ha ejecutado el free, que es el que esta escuchando
# la respuesta de nodeproc en reply, ya este muerto. Se ha muerto tras
#  killgrp en nodeproc. 
# Si no lo hago asi, nodeproc puede bloquearse porque ha matado al tio
#  esta al otro lado de reply antes de escribir en el propio canal reply

free (volume:string):string
{
	reply:=chan [1] of int;
	node_chan<-=(FREE_REPLICA,volume,reply);
	<-reply;	# wait for died
	return nil;
}



halt():string
{

	# Kill all replicas 
	reps:=all();
	if (reps!=nil)
	{
		for (r:=hd reps; reps!=nil; reps=tl reps)
			if (r!=nil)
			{
				log (cnodelog,"HALTING: replica");
				free(r.volume);	
			}
	}

	node_chan<-=(HALT_NODE,nil,nil);	# kill nodeproc
	killgrp(node_pid);	# Kill rest node threads

	log (cnodelog,"HALTING: kill cnode with all replicas");

	return nil;
}


all():list of ref Replica
{
	l:list of ref Replica = nil;
	for (i:=0;i<len storage;i++)
		if (storage[i]!=nil)
			l=storage[i]::l;

	return l;	
}



nodeproc (ctl:chan of (string, string, chan of int))
{
	err:string=nil;
	for (;;)
	{
		(cmd,volume,reply):=<-ctl;
		case (cmd)
		{		
		FREE_REPLICA =>
			err=free_rep_cmd(volume);
			if (reply!=nil)
				reply<-=0;

		ADD_REPLICA=>
			err=add_rep_cmd(volume);
			if (err!=nil)
			{
				log (cnodelog,err);
				free_rep_cmd(volume);
			}
		
			if (reply!=nil)
				reply<-=0;
		
		HALT_NODE =>
			break;
		}
	}
}



free_rep_cmd(volume:string):string
{
	if (volume==nil)
		return "Volume withouth name can not be free";

	rep:=getreplica(volume);
	if (rep!=nil)
	{
		e:=rep.free();
		if (e!=nil)
			return "Replica can not be unregistered in nodeproc";

		### creplica->unmount(utils->CARIBE_NS+"/"+rep.volume+"/peer");
		delreplica (rep);

		log (cnodelog,sprint("Replica %s killed, unmounted and removed",volume));
		ek:=killgrp(rep.gid);
		if (ek!=nil)
			return sprint("Replica zombie. %s",ek);
	}

	return nil;
}



add_rep_cmd (volume:string):string
{
	if (volume==nil)
		return "Volume name or directory are nil";

	# Inicio de la réplica
	# =====================

	#dir:="/usr/caribe/data/"+volume;
	dir:=utils->CARIBE_VOLS+"/"+volume;
	rep:=Replica.new(volume,dir,rid);
	if (rep==nil)
		return "Replica not started";
		
	rid++;
	err:=addreplica (rep);	
	log (cnodelog,"Replica is starting ...");

	spawn timeout_init(rep);


	# Fase de Negociación
	# ===================

	err=negotiationphase(rep);
	if (err!=nil)
		return sprint("Negotiation phase failed. %s",err);

	# Reef is always instanced
	# err=rep.instance("reef");	
	# if (err!=nil)
	# 	return err;

	rep.starting=0;	# Doy por finalizado el estado de INIT

	err=rep.register();
	if (err!=nil)
		return err;

	# Ahora monto el volumen a través de cproxy en /replica y para usarlo a través de
	# /files. Inicialmente montarle de nuevo el peer mientras mi replica local se pone
	# operativa. 

	err=cproxy->mount(rep.volume);
	if (err!=nil)
		return sprint("Replica was remove and not started. %s",err);	

	##log(stdout,"Fin del montaje del proxy");

	if (rep.root)	# si somo root creo el fichero meta
		rep.meta.write();

	return nil;
}



timeout_init(rep:ref Replica)
{
	c:=utils->timeout(REPLICA_INIT_TIMEOUT);
	<-c;
	if (rep.status()==INIT)
	{
		log (cnodelog,"Replica has exceded the timeout to init");
		free(rep.volume);
	}
}



negotiationphase (rep:ref Replica):string
{
	err,cc:string = nil;

	if (rep==nil)
		return "Nil replica. Error in negotiation";

	log (cnodelog, "Starting negotiation phase");

	err=newpeer(rep);			# Busco peer
	if (err==nil)				# Encuentro nuevo peer
		(cc,err)=setcc(rep);		# Negocio con el un cc

	if ((cc==nil) || (err!=nil))
	{
		log (cnodelog, sprint("Error in negotiation phase: %s",err));	
		rep.root=1;
		rep.peer=nil;
		cc=CCNULL;
	}


	if (rep.peer!=nil)		# No soy root
	{
		log (cnodelog, sprint("Building copy replica via %s",cc));
		(p,paddr,nvol):=splitaddr(rep.peer.addr);	#c, peer name, vol name
		cport:=rep.peer.attrs.get("port");
		peerc:="tcp!"+paddr+"!"+string cport;
		peermp:=utils->CARIBE_NS+"/"+rep.volume+"/peer";
		err=creplica->unmount(peermp);
		err=creplica->mount(peerc,peermp);
		if (err!=nil)
			return err;
	}
	else				# Soy root
	{
		# El peer soy yo mismo. Yo tengo la replica raiz.
		log (cnodelog, "Building root replica");
		peer:=getreplica(rep.volume);	
		if (peer==nil)
			return "Local replica not found";

		peerc:="tcp!"+sysname()+"!"+string peer.port;
		peermp:=utils->CARIBE_NS+"/"+rep.volume+"/peer";
		err=creplica->unmount(peermp);
		err=creplica->mount(peerc,peermp);
		if (err!=nil)	
			return err;
	}


	# Starting to links the coherency modules
	err=rep.instance(cc);	
	if (err!=nil)
		return err;

	log (cnodelog, "Negotiation phase complete");

	return nil;
}





setcc(rep:ref Replica):(string,string)
{
	if (rep.peer==nil)
		return (nil, "Peer not found");
	
	loc:=rep.peer.attrs.get("loc");
	addr:=rep.peer.addr;

	log (cnodelog, sprint("Peer for %s is %s in island %s",rep.volume,rep.peer.addr,loc));
	myloc:=cloc->where(sysname());
	(p,peer,nvol):=splitaddr(addr);	#c, peer name, vol name
	if (peer==utils->sysname())	# el peer soy yo mismo
		return (nil,"I'm the peer");

	peercsp:="tcp!"+peer+"!"+string (utils->CSP_PORT);
	fd:=cspopen(peercsp);
	if (fd==nil)
		return (nil,"Failed to dial peer");

	if (loc == myloc)
	{
		# Solicito usar el más estricto. 
		data:=str->quoted(utils->sysname()::rep.volume::"storm"::nil);
		(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_SET_CC,data));
		if (rm.cmd!=csp->OK)
			return (nil,sprint("Ask for cc failed. %s",rm.data));
		else
			return ("storm",rm.data);			

	}	
	else
	{
		# Solicito usar el menos estricto.
		data:=str->quoted(utils->sysname()::rep.volume::"rain"::nil);
		(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_SET_CC,data));
		if (rm.cmd!=csp->OK)
			return (nil,"Ask for cc failed");
		else
			return ("rain",nil);
	}
}


##
## Funciones del control de la variable global storage
## para el almacenamiento de replicas
##

getreplica (volume:string):ref Replica
{
	if (volume==nil)
		return nil;

	storage_lock<-=1;
	for (i:=0;i<len storage;i++)
		if ( (storage[i]!=nil) && (volume==storage[i].volume) )
		{
			<-storage_lock;
			return storage[i];
		}

	<-storage_lock;
	return nil;
}


addreplica (rep:ref Replica):string
{
	storage_lock<-=1;

	for (i:=0;i<nrep;i++)
		if (storage[i]==nil)
			break;
	storage[i]=rep;
	nrep++;
	<-storage_lock;
	return nil;
}


delreplica (rep:ref Replica):string
{

	if ( (rep==nil) || (nrep==0))
		return "Replica nil not remove from storage";

	storage_lock<-=1;
	for (i:=0;i<nrep;i++)
		if ( (storage[i]!=nil) && (rep.volume==storage[i].volume) )
		{
			storage[i]=nil;
			nrep--;
		}

	<-storage_lock;

	return nil;
}





cspproc(error:chan of string)
{	
	c:=cspsrv(CSP_PORT);
	if (c==nil)
	{
		error<-="Failed csp general service";
		return;
	}
	error<-=nil;	

	for(;;)
	{
		cop:=<-c;
		msg:=cop.msg;
		reply:=cop.reply;

		#log (cnodelog, sprint("Recibo csp: %s",msg.text()));

		case (msg.cmd)
		{		
		csp->REPL_MOUNT =>
			data:=str->unquoted(msg.data);
			source:=hd data;
			vol:=hd (tl data);
			r:=getreplica(vol);

			if ((r==nil) || (r.status()!=OP))
				reply<-=ref Cmsg(csp->ERROR,"Replica not operative");
			else
				reply<-=ref Cmsg(csp->OK,nil);	

			
		csp->REPL_PEER =>
			data:=str->unquoted(msg.data);
			source:=hd data;
			vol:=hd (tl data);
			r:=getreplica(vol);
			peer: string = nil;

			if ((r!=nil) && (r.peer != nil))
				(nil,peer,nil)=splitaddr(r.peer.addr);
			else
				peer=nil;

			if ((r==nil) || (r.status()!=OP) || (source == peer))
				reply<-=ref Cmsg(csp->ERROR,"Replica not operative");
			else
				reply<-=ref Cmsg(csp->OK,nil);	


		csp->REPL_PEER_DIED =>	
			data:=str->unquoted(msg.data);
			name:=hd data;
			vol:=hd (tl data);
			r:=getreplica(vol);

			reply<-=ref Cmsg(csp->OK,nil);	# Primero contesto y luego libero

			if (r!=nil)
			{
				log (cnodelog,sprint("Peer down for %s",vol));
				free(vol);			# mato la replica	
				replica(vol,r.rootpath);	# la reinicio
			}



		csp->REPL_INIT_NEW =>
			data:=str->unquoted(msg.data);
			name:=hd data;
			vol:=hd (tl data);
			r:=getreplica(vol);
			if (r!=nil)	# Ya tengo arrancada esa replica
			{
				reply<-=ref Cmsg(csp->OK,nil);
				continue;
			}

			log (cnodelog,sprint("Me piden que cree una replica de %s",vol));

			rp:=utils->CARIBE_VOLS+"/"+vol;
			e:=replica (vol,rp);	
			if (e!=nil)
			{
				log (cnodelog, sprint("Cannot initializing %s. %s",vol,e));
				reply<-=ref Cmsg(csp->ERROR,"Replica not started");	
			}
			else
			{
				reply<-=ref Cmsg(csp->OK,nil);	
			}



		csp->REPL_GET =>
			data:=str->unquoted(msg.data);
			name:=hd data;
			cc:=hd (tl data);
			vol:=hd (tl (tl data));
			r:=getreplica(vol);

			if ((r==nil)|| (r.status()!=OP))
			{
				reply<-=ref Cmsg(csp->ERROR,"Replica not operative");
			}
			else	
			{
				# Reenvio el mensaje csp a todos los cc de esa replica
				aux:=r.lkmods;
				s:=0;
				while (aux!=nil)
				{
					mod:=hd aux;
					if (mod.ccname == cc)
					{
						mod.csp<-=cop;
						s=1;
						break;
					}		
					aux=tl aux;
				# Cc module must send reply message
				}

				# El modulo no ha sido instanciando antes.
				if (s==0)
					reply<-=ref Cmsg(csp->ERROR,"Module not instancied");
			}


		csp->REPL_VERS =>
			data:=str->unquoted(msg.data);
			name:=hd data;
			vol:=hd (tl data);
			r:=getreplica(vol);
			if (r!=nil)
				reply<-=ref Cmsg(csp->OK,string r.vers);
			else
				reply<-=ref Cmsg(csp->ERROR,"replica unknow");
	

		csp->REPL_GET_CC =>
			data:=str->unquoted(msg.data);
			name:=hd data;
			vol:=hd (tl data);
			r:=getreplica(vol);

			if (r==nil)
			{
				reply<-=ref Cmsg(csp->ERROR,"replica unknow");
			}
			else
			{
				(lw,hg):=r.ccindex();
				ccstr:=str->quoted(string lw::string hg::nil);
				reply<-=ref Cmsg(csp->OK,ccstr);
			}


		csp->REPL_SET_CC =>
			data:=str->unquoted(msg.data);
			name:=hd data;
			vol:=hd (tl data);
			cc:=hd (tl (tl data));

			log (cnodelog, sprint("Recibo solicitud de carga de modulo %s para vol %s",cc,vol));

			r:=getreplica(vol);
			if ((r==nil) || (r.status()!=OP))
			{
				reply<-=ref Cmsg(csp->ERROR,"Replica not operative");
				continue;
			}
			err:=r.instance(cc);
			if (err!=nil)
				reply<-=ref Cmsg(csp->ERROR,sprint("Module not loaded. %s",err));
			else
				reply<-=ref Cmsg(csp->OK,nil);	




		csp->LOC_BEACON =>
			if (netdebug)	# Retraso artificial
				netdelay(0,1,Cdebug->VARY_DELAY);

			reply<-=ref Cmsg(csp->OK,nil);


		* =>
			reply<-=ref Cmsg(csp->ERROR,nil);
		}
	}	
	##log (stdout,"cspproc died");
}




event(e:ref Cevent->Evt)
{
	if (evchan!=nil)
		evchan<-=e;
}



eventproc(evchan:chan of ref Evt)
{
	for (;;)
	{
		e:=<-evchan;
		log (cnodelog,sprint("Evento %s",e.text()));
		err:=eventcc(e);
		if (err!=nil)
			log(cnodelog,sprint("Error procesing event. %s",err));
	}
}



eventcc(e:ref Evt):string
{
	if (e==nil)
		return "Nil event";

	case (e.cod)
	{
	cevent->REPL_RENEGOTIATE =>
		ea:=e.argv();
		if (ea==nil) 
			return "To few arguments for the event";	

		vol:=ea[0];

		r:=getreplica(vol);

		log (cnodelog,sprint("Regenotiate phase for volume %s.",vol));

		err:=negotiationphase(r);
		if (err!=nil)
		{
			free(vol);
			return sprint("Negotiation phase failed. %s",err);	
		}

		err=r.register();
		if (err!=nil)
		{
			free(vol);
			return sprint("Register failed. %s",err);	
		}




	cevent->REPL_LOW_REPLCTN =>
		ea:=e.argv();
		if (ea==nil) 
			return "To few arguments for the event";	

		vol:=ea[0];
		nrep:=ea[1];

		r:=getreplica(vol);
		log (cnodelog,sprint("Low replication level for volume %s. Only %s replicas",vol,nrep));
		err:=newcopy(r);		# Buscar al más cercano
		return sprint("%s",err);
		


	cevent->REPL_RESTART => 
		ea:=e.argv();
		if (ea==nil) 
			return "To few arguments for the event";	

		vol:=ea[0];
		rp:=ea[1];

		log (cnodelog, sprint("Restarting replica %s",vol));

		for (;;)
		{
			free(vol);		# mato la replica existente	
			t:=rand(10)*1000;
			log (cnodelog,sprint("Restarting replica in %d seconds ...",t));
			sys->sleep(t);
			e:=replica (vol,rp);	# la reinicio
			if (e==nil)
				break;
		}



	cevent->REPL_ABORT => 
		ea:=e.argv();
		if (ea==nil) 
			return "To few arguments for the event";	

		vol:=ea[0];
		log (cnodelog, sprint("Aborting replica %s",vol));
		free(vol);
		

		

	cevent->REPL_STATUS_CHANGED =>
		ea:=e.argv();
		if (ea==nil) 
			return "To few arguments for the event";

		vol:=ea[0];
		cc:=ea[1];
		newstatus:=int ea[2];
		r:=getreplica(vol);
		if (r==nil)
			return "Replica just not exists";

		log (cnodelog,sprint("Replica %s change the status to %d",vol,newstatus));

		if (newstatus<0)	
		{	
			if (r.status()==DIED) 
				return nil;	# La replica entera ya esta siendo liberada

			# Simplemente un CC se ha suicidado
			if (checkpeer(r)!=nil)
			{
				free(vol);			# mato la replica	
				replica(vol,r.rootpath);	# la reinicio
			}
			else
			{
				r.unlink(cc);		# descargo y cargo el CC
				r.instance(cc);
				cproxy->mount(vol);	# recargo el proxy
			}
		}
		else
		{	# Actualizamos el CNS con nuestro nuevo estado
			
			r.register();
			if (newstatus==OP)
			{
				newmeta:=Meta.read(vol);
				if (newmeta!=nil)
					r.meta=newmeta;	#actualizo mi META
			}
		}
	}

	return nil;
}