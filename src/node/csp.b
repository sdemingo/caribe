implement Csp;

include "mods.m";
	Connection,sprint,open,read,write,stat,create :import sys;
	stderr,stdout,log,csplog,Cmdargs:import utils;

netdebug:int;


COP_BUF_SZ	: con 100;


# Csp commands descriptions: (text, min args)
#
csptable:=array [ ] of {
	("REPL_PEER",2),	# source, volume
	("REPL_GET",4),		# source, cc, volume, creplica, port
	("REPL_INIT_NEW",2),	# source, volume
	("REPL_MOUNT",2),	# source, volume
	("REPL_VERS",2),	# source, volume
	("REPL_GET_CC",3),	# source, volume
	("REPL_SET_CC",3),	# source, volume, cc
	("REPL_PEER_DIED",3),	# source, volume
	("CC_HISTORY",3),	# source, volume, file
	("LOC_BEACON",2),	# source, island
	("OK",0),
	("ERROR",0),
};


init (d:Dat)
{
	initmods(d->mods);
}


Cop.new (msg:ref Cmsg,reply :chan of ref Cmsg):ref Cop
{
	c: Cop;
	c.msg=msg;
	c.reply=reply;
	return ref c;
}


pstring(a: array of byte, o: int, s: string): int
{
	if (s==nil)
		return 0;
	sa := array of byte s;	# could do conversion ourselves
	n := len sa;
	a[o] = byte n;
	a[o+1] = byte (n>>8);
	a[o+2:] = sa;
	return o+STRSZ+n;
}

gstring(a: array of byte, o: int): (string, int)
{
	if(o < 0 || o+STRSZ > len a)
		return (nil, -1);
	l := (int a[o+1] << 8) | int a[o];
	o += STRSZ;
	e := o+l;
	if(e > len a)
		return (nil, -1);
	return (string a[o:e], e);
}


Cmsg.pack(m:self ref Cmsg):array of byte
{
	a:=array[LENRPC + CMDSZ + STRSZ + len m.data] of byte;
	o:=0;

	# size
	size:=len a;
	a[o++] = byte size;
	a[o++] = byte (size>>8);

	# m.cmd
	a[o++] = byte m.cmd;
	a[o++] = byte (m.cmd>>8);

	# m.data
	o = pstring(a, o, m.data);
	return a;
}


Cmsg.unpack(a:array of byte):ref Cmsg
{
	m:=ref Cmsg;

	if (len(a)<BIT32SZ)
		return nil;

	#size
	size:=(int a[1] << 8) | int a[0];
	
	#m.cmd
	m.cmd= (int a[3] << 8) | int a[2];

	o:=LENRPC + CMDSZ;

	#m.data
	(data,off):=gstring(a,o);
	m.data=data;

	return m;
}


Cmsg.text(m:self ref Cmsg):string
{
	if (validcmd(m)==0)
		return nil;
	(s,args):=csptable[m.cmd];
	s=s+" "+m.data;

	return s;
}


Cmsg.read(fd: ref Sys->FD):ref Cmsg
{
	(a,e):=readmsg(fd,0);
	if (e!=nil)
	{
		log(csplog,e);
		return nil;
	}
	return Cmsg.unpack(a);
}


validcmd(m:ref Cmsg):int
{
	if (m==nil)
		return 0;
	if ((m.cmd>=MAX_CSP_CMD) || (m.cmd<0))
		return 0;
	return 1;
}


readmsg(fd: ref Sys->FD, msglim: int): (array of byte, string)
{
	if(msglim <= 0)
		msglim = MAXRPC;
	if (fd==nil)
		return (nil, "Bad descriptor");
	sbuf := array[LENRPC] of byte;

	n := readn(fd, sbuf, LENRPC);
	if (n==0)
		return (nil,"Csp connection ends");

	if(n != LENRPC)
		return (nil, sprint("Bad csp message format. Read %d bytes",n));

	ml := (int sbuf[1] << 8) | int sbuf[0];
	if(ml <= LENRPC)
		return (nil, "invalid csp message size");
	if(ml > msglim)
		return (nil, "Csp message longer than agreed");
	buf := array[ml] of byte;
	buf[0:] = sbuf;
	if((n = readn(fd, buf[LENRPC:], ml-LENRPC)) != ml-LENRPC)
		return (nil, "Csp message truncated");
	return (buf, nil);
}


readn(fd: ref Sys->FD, buf: array of byte, nb: int): int
{
	for(nr := 0; nr < nb;){
		n := sys->read(fd, buf[nr:], nb-nr);
		if(n <= 0){
			if (nr == 0)
				return n;
			break;
		}
		nr += n;
	}
	return nr;
}


cspsrv (port:int) : chan of ref Cop
{
	copchan:=chan of ref Cop;
	error:=chan of string;
	spawn runserver(copchan,port,error);
	e:=<-error;

	if (e!=nil)	
	{
		log(csplog,"From cloc: "+e);
		return nil;
	}

	log (csplog, sprint("Csp service initialized on %d and listening...",port));
	return copchan;
}



runserver(copchan:chan of ref Cop,port:int, error:chan of string)
{
	(e,conn) := sys->announce("tcp!*!"+string port);
	if (e<0)
	{
		error<-="Csp: announced failed";
		return;	
	}
	error<-=nil;
	while ()
	{
		(er,cn) := sys->listen(conn);
		if (er<0)
			log (csplog,"Csp: listen failed");
		else		
			spawn cspproc(cn,copchan);
	}
}

cspproc(conn : Connection, copchan: chan of ref Cop)
{
	dfd :=sys->open(conn.dir + "/data", Sys->ORDWR);
	if (dfd==nil)
	{
		cerror:=Cop.new(ref Cmsg(ERROR,"Error at open connection"),nil);
		copchan<-=cerror;
		return;
	}
	
	for(;;)
	{
		reply:=chan of ref Cmsg;
		m:=Cmsg.read(dfd);
		if (m==nil)
			return; # si me llega un msg corrupto rompo la conexión
	
		log (csplog,sprint("<=[Csp] %s",m.text()));
		cop:=Cop.new(m,reply);
		copchan<-=cop;
		r:=<-reply;
		a:=r.pack();
		n:=sys->write(dfd,a,len a);
		log (csplog,sprint("=>[Csp] %s",r.text()));
	} 
}


cspopen (addr:string):ref sys->FD
{
	(e,c):=sys->dial(addr,nil);
	if (e<0)
		return nil;
	return c.dfd;
}


csprpc(fd: ref sys->FD, req: ref Cmsg):(string, ref Cmsg)
{
	if ((fd==nil)||(req==nil))
		return ("CSP fd or request are nil", nil);

	m:=req.pack();
	n:=write(fd,m,len m);
	if (n<len m)
		return ("CSP send failed",nil);

	rep:=Cmsg.read(fd);
	return (nil,rep);
}
