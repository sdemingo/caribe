implement Vdb;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,fprint,sprint:import sys;
	stderr,ccmdlog,log,stdout,sysname:import utils;
	Service,Attributes: import registries;
	Db,Dbptr,Dbentry	: import attrdb;



Ventry.text(e: self ref Ventry):string
{
	s: string = nil;
	if (e!=nil)
		s="volume="+e.volume+" dir="+e.dir+" user="+e.user;
	return s;
}


	
init (d:Dat)
{
	initmods(d->mods);
	attrdb->init();	
}


# checkndb()
# {
# 	(er,d):=sys->stat(utils->CARIBE_FS);
# 	if (er<0)
# 		sys->create(utils->CARIBE_FS,sys->OREAD,8r644);
# }


add (f:string, fields: list of (string,string)):string
{
	if (fields==nil)
		return "Cndb: to few fields for this entry";

	fd:ref sys->FD;
	(er,d):=sys->stat(f);
	if (er<0)
		fd=sys->create(f,sys->OWRITE,8r644);
	else
		fd=sys->open(f,sys->OWRITE);
	
	if (fd==nil)
		return "Cndb: ndb file not found";

	o:=big 0;
	o=sys->seek(fd,o,sys->SEEKEND);

	text:="";
	faux:=fields;
	while (faux!=nil)
	{
		(a,v):=hd faux;
		text=text+a+"="+v;
		faux=tl faux;
		if (faux!=nil)
			text=text+" ";
	}
	text=text+"\n";

	# Si la entrada existe no la añado
	buf:=read(f);
	total:=string buf;
	#pre:=str->take(text,total);
	#if  (pre!=nil) 	# existe la linea y no la insertamos en el ndb
	if (exists(total, text))
		return "Cndb: volume entry already exits ind ndb";


	n:=0;
	n=sys->pwrite(fd,array of byte text, len array of byte text,o);
	
	return nil;
}


read(f:string):array of byte
{
	fd:=sys->open(f,sys->OREAD);
	if (fd==nil)
	{
		#log (ccmdlog, "Cndb: ndb file not found\n");
		return nil;
	}
	
	buf:=array [BUF_SZ] of byte;
	n:=sys->read(fd,buf,BUF_SZ);
	if (n<=0)
		return nil;
	return buf[:n];
}


exists(total:string, entry:string):int
{
	lc:=utils->splitlines(total);
	if (entry[(len entry)-1]=='\n')
		entry=entry[:(len entry)-1];

	while (lc!=nil)
	{
		s:=hd lc;
		if (s==entry)
			return 1;
		lc=tl lc;
	}
	return 0;
}


get (f:string): list of ref Ventry
{
	db:=Db.open(f);
	if (db==nil)
	{
		#log (ccmdlog, "Vdb: fs file not found\n");
 		return nil;
	}	
	ptr:ref Dbptr;
	ve:=ref Ventry;
	lve: list of ref Ventry = nil;

	while (ve!=nil)
	{
		(ve,ptr)=readentry(db,ptr);
		lve=ve::lve;
	}
	
	return lve;
}



readentry(db:ref Db, ptr:ref Dbptr): (ref Ventry, ref Dbptr)
{
	e: ref Dbentry;	
	ve:=ref Ventry;	
	(e,ptr)=db.find(ptr,"volume");
	
	if (e==nil)
		return (nil,ptr);

	# Ahora e, apunta a la entrada que tenemos que procesar

	for (tup:=hd e.lines; e.lines!=nil; e.lines=tl e.lines)
	{
		att:=hd tup.pairs;
		ve.volume=att.val;
		tup.pairs=tl tup.pairs;

		att=hd tup.pairs;
		ve.dir=att.val;
		tup.pairs=tl tup.pairs;

		att=hd tup.pairs;
		ve.user=att.val;
		tup.pairs=tl tup.pairs;
	} 

	return (ve,ptr);
}



