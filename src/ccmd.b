implement Ccmd;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,sprint:import sys;
	cspsrv,Cmsg:import csp;
	stderr,stdout,ccmdlog,log,sysname,args,panic,
			Cmdargs:import utils;
	Attributes,Service	: import registries;
	Evt,REPL_TREE_DUMP: import cevent;
	replica,halt: import cnode;
	Replica:import creplica;
	Ventry: import vdb;
	rand: import rnd;

include "sh.m";
	sh:Sh;


cc:Cc;
mod_pid:int;
mod_exit:chan of int;


init (d:Dat)
{
	initmods(d->mods);
	sh=load Sh Sh->PATH;
}


start(next:chan of int,flags:ref Cmdargs)
{
	### 
	#sys->bind("#s", "/chan",sys->MCREATE | sys->MREPL );
	###

	error:=chan of string;
	io:=file2chan("/chan","ccmd");
	if (io!=nil)
	{
		spawn cmdproc(io,flags,error);
		e:=<-error;
		if (e!=nil)
		{
			panic(e);
			next<-=0;
		}
		else
			next<-=1;
	}
	else
	{
		log(ccmdlog,"Ccmd: Command Interface not started");
		next<-=0;
	}	
}	


exec(cmd:string):string
{
	fd:=sys->open("/chan/ccmd",sys->OWRITE);
	if (fd==nil)
		return "Failed exec ccmd";
	n:=sys->write(fd,array of byte cmd,len cmd);
	return nil;
}


cmdproc(fio: ref FileIO,flags:ref Cmdargs,error:chan of string)
{
	fd: ref sys->FD;
	e:="";
	if (flags.loadfs)
		e=loadcmd();

	if (e!=nil)
	{
		error<-="Ccmd: problems with ndb file";
		return;
	}

	error<-=nil;

	for(;;) alt{
		(off,count,fid,rc):=<-fio.read	=>
		if (rc!=nil)
		{
			sts:=array [count] of byte;
			rc<-=(sts, nil);
		}

		(off,data,fid,wc) := <-fio.write	=>
		if (wc!=nil){
			c:=string data;
			arg:=str->unquoted(c);
			argv:=args(arg);
			cmd:=argv[0];

			case cmd
			{
			"mount" => 	e=mountcmd(argv);
			"replica" =>	e=replicacmd(argv);
			"map" =>	e=mapcmd(argv);
			"loc" =>	e=loccmd(argv);
			"help" => 	e=helpcmd(argv);
			"info" => 	e=infocmd(argv);
			"debug" =>	e=debugcmd(argv);
			"halt" =>	e=haltcmd(argv);
			"restart" =>	e=restartcmd(argv,flags);
			* =>		e="Unknow command";
			}

			if (e!=nil)
				log (stderr,sprint("Error: %s",e));

			wc<-= (len data, nil);
		}
	}
}


haltcmd (argv: array of string):string
{
	log (stdout,"Halting node ...");
	cnode->halt();
	log (stdout,"Halting cproxy ...");
	cproxy->halt();
	log (stdout,"Halting cns ...");
	cns->halt();
	log (stdout,"Halting cloc ...");
	cloc->halt();
	log (stdout,"... caribe halted");

	return nil;
}


restartcmd (argv: array of string,flags:ref Cmdargs):string
{
	e:string;

	log (stdout,"Halting node ...");
	cnode->halt();
	log (stdout,"Halting cns ...");
	cns->halt();
	log (stdout,"Halting cloc ...");
	cloc->halt();

	t:=(rand(10)+1)*1000;
	log (stdout,sprint("Restarting node in %d seconds ...",t));
	sys->sleep(t);


	if (flags.netdbg)
		cdebug->start();

	# Restarting active modules (with threads)
	cns->start();
	e=e+cloc->start(flags);
	e=e+cnode->start(flags);

	if (e!=nil)
		log (stdout,"restart errors: "+e);

	if (flags.loadfs)
		e=loadcmd();

	if (e==nil)
		log (stdout,"... Restarting complete");

	return nil;
}


mountcmd (argv: array of string):string
{
	if ( (argv==nil) || (len argv < 2) || (argv[1]==nil) )
		return "volume is nil";
	volume:=argv[1];

	cproxy->mount(volume);

	log(ccmdlog,sprint("mount %s",volume));
	return nil;
}


replicacmd (argv: array of string):string
{
	if (len argv < 2)
		return "Volume need more arguments";
	return replica(argv[0],argv[1]);
}


mapcmd (argv: array of string):string
{
	sys->print("%s",cloc->print());
	return nil;
}


debugcmd (argv: array of string):string
{
	if (len argv < 3)
		return "Dump need more arguments";

	if (argv[1]=="dump")
	{
		log(stdout,sprint("Generamos evento para %s",argv[1]));
		cproxy->event(Evt.new(REPL_TREE_DUMP,argv[2]::nil));
	}
	else if (argv[1]=="netdelay")
	{
		i:=int argv[2];
		cdebug->setdelay(i);		
	}

	return nil;
}


infocmd (argv: array of string):string
{
	sys->print("loc=%s\n",cloc->where(sysname()));
	lr:=cnode->all();
	if (lr==nil)
		return nil;

	for(r:=hd lr;lr!=nil;lr=tl lr)
		if (r!=nil)
			sys->print("%s",r.text());	

	return nil;
}



helpcmd (argv: array of string):string
{
	sys->print("mount\t Mount a remote replica\n");
	sys->print("replica\t Create a new replica on the node\n");
	sys->print("map\t Shows the islands map\n");
	sys->print("loc\t Shows the location of the node\n");
	sys->print("info\t Shows the information about the replicas of the node\n");
	sys->print("help\t Shows this message\n");
	return nil;
}



loccmd (argv: array of string):string
{
	sys->print("%s",cloc->where(utils->sysname()));
	return nil;
}



loadcmd():string
{
	lv:=vdb->get(utils->CARIBE_FS);
	i:=0;
	while (lv!=nil)
	{
		v:=hd lv;
		if (v!=nil)
		{
			argv:= array [] of {v.volume,v.dir};
			e:=replicacmd(argv);
			if (e!=nil)
				log (stderr,sprint("Error at starting replica: %s",e));
			else	
				i++;
		}
		lv=tl lv;
	}

	log(ccmdlog,sprint("%d replicas loaded from fs config file",i));
	return nil;
}




