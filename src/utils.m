Utils:module
{
	PATH	: con "utils.dis";

	# Files and directories
	CARIBE_HOME 	: con "/usr/caribe";
	CARIBE_FS		: con "/usr/caribe/lib/fs";
	CARIBE_LOC 		: con "/usr/caribe/lib/loc";
	CARIBE_LOGS	: con "/usr/caribe/logs";
	CARIBE_VOLS	: con "/usr/caribe/data/vol";
	CARIBE_ERR		: con "/usr/caribe/data/err";
	CARIBE_NS		: con "/n/caribe";

	# General propose port
	CREPLICA_PORT	: con 19888;
	CSP_PORT		: con 19777;
	PROXY_PORT	: con 19666;

	# Coherency  ports
	STORM_PORT	: con 19555;
	WAVE_PORT		: con 19444;

	# Logs 
	stderr,
	stdout,	
	cmntlog,
	ccmdlog,	
	cnslog,
	csplog,
	cloclog,
	synclog,
	cnodelog,
	creplicalog,
	xlog,
	cproxylog,
	stormlog,
	rainlog,
	reeflog,
	autolog,
	ctreelog,
	paniclog,
	MAXLOG: con iota;

	# Ignored paths
	ignored_paths: array of string;


	Cmdargs:adt
	{
		loadfs : int;
		nat     : int;
		static  : int;
		netdbg :int;

		new : fn(args:list of string):(ref Cmdargs,string);
		print :fn(args:self ref Cmdargs);
	};


	init	: fn (d:Dat,debug:int);
	log	: fn (log:int, m:string);
	openlog	: fn (name:string):ref Sys->FD;
	panic	: fn (e:string);
	killgrp	: fn (pid: int):string;

	sysname	: fn ():string;
	sysaddr	: fn ():string;
	resolvname	: fn (name:string):string;
	natip	: fn():string;

	getclient	: fn (conn: Sys->Connection): (string,string);
	splitaddr	: fn (addr:string):(string,string,string);
	splitlines	: fn (text:string):list of string;
	timeout	: fn (time:int): chan of int;
	args	: fn (l:list of string):array of string;

	# path ops
	cleanns	: fn (ns:string):string;
	mkns	: fn (ns:string):string;
	dropprefix	: fn (prefix:string, src:string):string;
	isignoredpath: fn (prefix: array of string, src:string):int;
	copyfile 	: fn (f1,f2:string):int;
	conflictpath	: fn (path:string):string;

	writeline 	: fn (fd: ref Sys->FD, m:string):int;
	size	: fn (name:string):big;
	checkinit	: fn (error:string);

	# Usados solo en modulos
	rawstr2pairs	: fn (raw:string):list of (string,string);
	pairs2rawstr	: fn  (pairs: list of (string,string)):string;
};
