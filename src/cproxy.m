Cproxy : module
{
	PATH	: con "cproxy.dis";

	Mcon	: adt
	{
		fd:	ref sys->FD;
		srv: 	ref Styxservers->Styxserver;
		addr:	string;
		port:	string;
		tc:	chan of ref Styx->Tmsg;
		exit_c:	chan of int;
		gid:	int;
		
		new:	fn(mv:ref Mvol,c : sys->Connection ):ref Mcon;		
	};
	

	Mvol	: adt
	{
		volume	: string;
		replica	: string;	# where the current replica is mounted (normally /replica)
		rep_fd	: ref sys->FD;	# fd to the csp replica service
		srv	: string;	# where the proxy is mounted (normally /files)
		target	: string;	# the name of the replica mounted
		srvport	: int;
		nav_c	: chan of ref Styxservers->Navop;
		xfs	: ref Xsrv->Xfs;
		ctl_c	: chan of (string, chan of string);
		broken	: int;
		gid	: int;
		con_procs	: ref Procs->Ptab;
		
		new	: fn(volume:string):ref Mvol;	# deberia desaparecer
		free	: fn(mv:self ref Mvol);
	};

	init 	: fn (d:Dat):string;
	start	: fn():string;
	halt	: fn():string;

	mount	: fn (volume:string):string;
	unmount	: fn (volume:string):string;
	update	: fn (volume:string):string;
	event	: fn(e:ref Cevent->Evt);
};
