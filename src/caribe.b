implement Caribe;

include "mods.m";
	sprint : import sys;
	stderr,log,panic,stdout, Cmdargs, checkinit	: import utils;
	
Caribe: module
{
	init : fn(nil:ref Draw->Context, args: list of string);
};


init (nil:ref Draw->Context, args: list of string)
{
	# system modules
	sys=load Sys Sys->PATH;
	dat=load Dat Dat->PATH;
	dat->loadmods(sys);
	initmods(dat->mods);

	# system modules
	rnd->init(sys->millisec());
	styx->init();
	styxs->init(styx);
	styxs->traceset(0);

	# modulos sin dependencias
	utils->init(dat,1);
	cdebug->init(dat);
	(flags,e):=Cmdargs.new(args);
	if ((e!=nil) || (flags==nil))
		panic(e);

	flags.print();

	xsrv->init(dat);
	sync->init(dat);
	checkinit(cloader->init(dat));
	csp->init(dat);
	vdb->init(dat);
	ctree->init(dat);
	checkinit(cevent->init(dat));
	creplica->init(dat);
	cns->init(dat);
	cmeta->init(dat);
	cproxy->init(dat);
	auto->init(dat);
	procs->init(dat);
	history->init(dat);
	

	# modulos con dependencias en arranque
	checkinit(cloc->init(dat,flags));
	checkinit(cnode->init(dat,flags));
	ccmd->init(dat);

	next:=chan of int;
	ccmd->start(next,flags);
	<-next;

}




