implement Cc;

include "mods.m";
	Connection,sprint,open,read,write,stat,create :import sys;
	stderr,stdout,log,sysname,panic,CSP_PORT,timeout,splitaddr,
		killgrp,stormlog,dropprefix,rawstr2pairs:import utils;
	Service,Attributes:import registries;
	Tree,Lock: import ctree;
	cspsrv,Cmsg,Cop,cspopen,csprpc:import csp;
	Evt:import cevent;
	Rmsg,Tmsg:import styx;
	Cmodule, Replica, OP, SYNC, CORR,DIED, CC_CMD_FREE,CC_CMD_INFO: import creplica;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	#xpathfile : import xsrv;


include "readdir.m";
	readdir: Readdir;


CC_NAME		: con "storm";
CC_LEVEL	: con 1;
STATUS_REFRESH	: con 10000;	# ms
VERS_REQ_WINDOW	: con 3000;	# ms


init (d:Dat)
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
}



##	
##	Metadata of replica over storm
##

Storminfo:adt
{
	replica	: ref Replica;
	mod	: ref Cmodule;
	peer	: ref Registries->Service;	# parent
	peerip	: string;
	peerloc	: string;
	master	: int;
	childs	: int;
	gid	: int;		# gid of proccess to control this Storminfo
	last_fw	: int;
};


	
###
##	Creación y arranque de un volumen
###


start (rep:ref Replica):ref Cmodule
{
	mod:=Cmodule.new();
	mod.ccname=CC_NAME;
	mod.ccindex=CC_LEVEL;
	log (stormlog,sprint("Add storm coherency to replica of %s",rep.volume));
	ec:=chan of string;
	spawn stormproc(rep,mod,ec);
	error:=<-ec;
	if (error!=nil)
		return nil;
	return mod;	
}


free (mod: ref Creplica->Cmodule): string
{
	log(stdout,"Destroying storm");

	if (mod.status==DIED)
		return nil; 

	reply:=chan of string;
	mod.ctl<-=(CC_CMD_FREE,reply);
	err:=<-reply;
	log (stdout,sprint("Mato grupo %d de storm",mod.gid));
	killgrp(mod.gid);

	return nil;
}


info (mod: ref Creplica->Cmodule): list of (string,string)
{
	if (mod.status==DIED)
		return nil; 	

	reply:=chan of string;
	mod.ctl<-=(CC_CMD_INFO,reply);
	raw:=<-reply;
	l:=rawstr2pairs(raw);

	return l;
}




free_module(st: ref Storminfo,mod:ref Creplica->Cmodule):string
{
	# Primero aviso a todos los childs de que voy a 
	# morirme mandandoles un csp

	spath:=utils->CARIBE_NS+"/"+st.replica.volume+"/storm";
	sd:=sys->open(spath, sys->OREAD);
	if (sd!=nil)
	{
		(childs,n):=readdir->readall(sd,readdir->NAME);
		if (childs!=nil)
		{
			for (i:=0; i<n; i++)
			{
				childip:=utils->resolvname(childs[i].name);
				mcsp:="tcp!"+childip+"!"+string (utils->CSP_PORT);
				fd:=cspopen(mcsp);
				if (fd==nil)
					return "Failed csp comunication to child during the free_module";
				diedreq(fd,st.replica.volume);
				removechild(st,childs[i].name,nil);
			}
		}
	}
	st.childs=0;
	st.peer=nil;
	st.peerloc=nil;
	st.peerip=nil;
	st.master=0;
	mod.changeto(st.replica, DIED);

	return nil;
}


info_module(st: ref Storminfo,mod: ref Cmodule):string
{
	s:="name="+CC_NAME+" ";
	s=s+"ccindex=1"+" ";
	s=s+"status="+sprint("%d",mod.status)+" ";

	ch:="";
	spath:=utils->CARIBE_NS+"/"+st.replica.volume+"/storm";
	sd:=sys->open(spath, sys->OREAD);
	if (sd!=nil)
	{
		(childs,n):=readdir->readall(sd,readdir->NAME);
		for (i:=0; i<n; i++)
			ch=ch+childs[i].name+",";
	}

	if (len(ch)==0)
		s=s+"childs=<nochilds> ";
	else
		s=s+"childs="+ch+" ";

	return s;
}




stormproc(rep:ref Replica, mod: ref Cmodule, error:chan of string)
{
	gid:=sys->pctl(sys->NEWPGRP,nil);

	err: string=nil;
	fd:=ref sys->FD;

	st:=ref Storminfo;
	st.replica=rep;
	st.mod=mod;
	st.childs=0;
	st.peer=nil;
	st.peerloc=nil;
	st.peerip=nil;
	st.master=0;
	st.last_fw=sys->millisec();

	if (rep.root==1)
	{
		st.peer=nil;		# la réplica es root
		st.master=1;
	}
	else
	{
		st.peer=rep.peer;	# la réplica no es root
		st.peerloc=st.peer.attrs.get("loc");

		## OJO: Si el peer no esta en mi isla soy 
		## master de esa isla

		if (st.peerloc != cloc->where(sysname()))
			st.master=1;

		(nil,pname,nil):=splitaddr(st.peer.addr);	#c, peer name, vol name
		st.peerip=utils->resolvname(pname);
	}

	st.gid=gid;
	mod.gid=gid;
	spawn srv(st,mod);
	spawn cspproc(st,mod.csp);

	error<-=nil;

	# En cuanto devolvemos el control a instance en creplica
	# activamos el cerrojo para dejar de oir nada de fuera
	# hasta que este modulo este preparado

	for(;;){

		mod.changeto(rep,CORR);		# mod.status=CORR;		
		(err,fd)=initphase(st,rep,mod);	# Aftter that  the status must be OP
		if (err!=nil)
			break;
		servicephase (st, fd);		# block until some problem happens
	}

	log(stormlog,sprint("Storm panic and died. %s",err));	
	free(mod);
}



initphase(st: ref Storminfo,rep:ref Replica,
		mod: ref Cmodule):(string,ref sys->FD)
{
	log(stormlog,"Storm module: Initialization phase  ... ");
	utils->cleanns(utils->CARIBE_NS+"/"+rep.volume+"/storm");
	utils->mkns(utils->CARIBE_NS+"/"+rep.volume+"/storm");

	fd: ref sys->FD;
	e:string;
	(lcc,hcc):=rep.ccindex();

	# Aqui deberia haber un cerrojo pues dos modulos no pueden
	# entrar en la initphase a la vez. Si ambos chekean el rep.ccindex
	# a la vez tendriamos una cond carrera

	if ((mod.ccindex < lcc) && (st.peer!=nil))
	{	
		log(stormlog,"Replica coherency level is higher. Syncronizing ...");
		mod.changeto(rep,SYNC);		# mod.status=SYNC;
		(e,fd)=syncproc(st);
		if (e!=nil)
			return (e,nil);	
	}
	log(stormlog,"... Syncronizing done");

	# Recargo el creplica pues los mensajes durante la sincronizacion
	# se han modificado directamente el SF el (/usr/caribe/data)
	# sin pasar por el creplica
	st.replica.fill();

	mod.changeto(rep,OP);		# mod.status=OP;

	return (nil,fd);
}


syncproc(st: ref Storminfo):(string,ref sys->FD)
{

	##
	## Creo conexion al peer
	##

	if (st.peer==nil)
		return ("No peer found",nil);
	(p,paddr,nvol):=splitaddr(st.peer.addr);	#c, peer name, vol name

	mcsp:="tcp!"+paddr+"!"+string (utils->CSP_PORT);
	fd:=cspopen(mcsp);
	if (fd==nil)
		return ("Failed csp comunication to peer",fd);

	##
	## Solicito sync por Csp
	##

	data:=str->quoted(utils->sysname()::"storm"::st.replica.volume::string st.replica.port::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_GET,data));
	if ((rm==nil ) || (rm.cmd!=csp->OK))
		return (sprint("Sincronization rejected by %s",st.peer.addr),fd);	


	log (stormlog, "Conexion al peer ok. Empezamos a checkear ficheros");

	##
	## Obtencion de ficheros
	##

	# No remonto a mi peer. Simplemente cogo los archivos
	# de /peer pues hay tengo ya montado a mi padre

	e:="";
	peermp:=utils->CARIBE_NS+"/"+st.replica.volume+"/peer";

	e2:=sync->synctree(st.replica.volume,peermp, st.replica.rootpath);
	if (e2!=nil)
		return (sprint("Failed during syncronization to peer %s",st.peer.addr),fd);


	# Pido el numero de version y actualizo el mio
	(mvers,e4):=versreq(fd,st.replica.volume);
	if (e!=nil)
		return ("Failed vers request to peer during sync",fd);

	st.replica.vers=mvers;
	return (nil, fd);
}





cspproc(stinfo:ref Storminfo, cspchan: chan of ref Cop)
{
	for(;;)
	{
		op:=<-cspchan;
		msg:=op.msg;
		reply:=op.reply;
		case (msg.cmd)
		{
		csp->REPL_GET =>
			log (stormlog,"Storm receive REPL_GET");
			data:=str->unquoted(msg.data);
			child:=hd data;
			cc:=hd (tl data);
			vol:=hd (tl (tl data));
			port:=hd (tl (tl (tl (data))));
			mmp:=utils->CARIBE_NS+"/"+vol+"/storm/"+child;
			(ed,dir):=stat(mmp);
			log (stormlog,"Removing child ...");
			removechild(stinfo,child,nil);
			sadd:="tcp!"+child+"!"+string port;
			log (stormlog,"Mounting child ...");
			e:=creplica->mount(sadd, mmp);
			if (e!=nil)
				reply<-=ref Cmsg(csp->ERROR,nil);
			else
			{
				reply<-=ref Cmsg(csp->OK,nil);
				stinfo.childs++;
				log (stormlog,sprint("Discovered new child named %s",child));
			}

		* =>
			reply<-=ref Cmsg(csp->ERROR,nil);
		}

	}
}				



###
##	Reenvio de mensajes 
###

srv(st:ref Storminfo, mod: ref Cmodule)
{

	for (;;)
	{

	alt{
	(qid,msg,source):=<-mod.req =>

		err:string=nil;

		##file:=st.replica.xfs.tree.getpath(qid);
		##log (stormlog,sprint("<- %s on %s",msg.text(),file));

		err=checklocks(st,qid,msg,source);
		if (err==nil)
		{
			err=forwarder(st,qid,msg,source);
			if (err!=nil)
				log (stormlog,sprint("Msg can not forwarder. %s",err));
			else
				st.last_fw=sys->millisec();
			
		}
		else
			log (stormlog,sprint("Msg not forwarder. %s",err));

		# log (stormlog,sprint(" Contesto con [%s]",err));
		
		mod.reply<-=err;


	(cmd,reply):=<-mod.ctl =>

		case cmd
		{
		CC_CMD_FREE =>	
			err:=free_module(st,mod);
			if (reply!=nil)
				reply<-=err;

		CC_CMD_INFO =>	
			str:=info_module(st,mod);
			if (reply!=nil)
				reply<-=str;

		* =>
			log(stdout,"recibido CMD desconocido");
			if (reply!=nil)
				reply<-=nil;
		}
	}
	}
}


checklocks(st:ref Storminfo, qid:big, msg:ref Tmsg, source:string):string
{
	err:string = nil;

	file:=st.replica.xfs.tree.getpath(qid);	# only for debug msg

	pick m := msg {

	Open =>
		err=secureopen(st,qid,m.mode,source);
		#if (err==nil)
		#	log (stormlog,sprint("Bloqueo fichero %s por open para %s",file,source));
	
	Clunk =>
		#log (stormlog,sprint("Desbloqueo fichero %s",file));
		err=st.replica.xfs.tree.unlock(qid);

	Write =>
		# Comprobar el autor de los mensajes

		lock:=st.replica.xfs.tree.openlock(qid);
		if ((lock.owner!=nil) && (lock.owner != source))
			err="File is locked";
		st.replica.xfs.tree.closelock(qid);

	Wstat =>
		lock:=st.replica.xfs.tree.openlock(qid);
		if ((lock.owner!=nil) && (lock.owner != source))
			err="File is locked";
		st.replica.xfs.tree.closelock(qid);


	* =>

	}	

	return err;

}


secureopen(st:ref Storminfo, qid:big, mode:int, source:string):string
{
	err:string = nil;

	# Si el open de escritura cerramos el fichero, si el open
	# es de lectura comprobamos el cerrojo

	lock:=st.replica.xfs.tree.openlock(qid);

	if ((mode & Sys->OWRITE) || (mode & Sys->ORDWR))
	{
		# Si el nivel del cerrojo mas relajado o
		# el origen del mensaje es el master
		# Expropio:

		if (lock.level > CC_LEVEL) 
			st.replica.xfs.tree.lock(qid,source,CC_LEVEL);
		if ((lock.level == CC_LEVEL) 
			&& (source == st.peer.addr) 
			&& (lock.owner != st.peer.addr))
			st.replica.xfs.tree.lock(qid,source,CC_LEVEL);
	}

	st.replica.xfs.tree.closelock(qid);

	return err;
}



forwarder(st:ref Storminfo, qid:big, msg:ref Tmsg, source:string):string
{
	myloc:=cloc->where(sysname());

	# file is the text path from the rootdir of the replica. 
	# path is the text path from the root of the 
	# local namespace of the node

	file:=st.replica.xfs.tree.getpath(qid);
	path,spath:string;
	err:string = nil;

	if ((st.master==0) && (st.peer!=nil))
	{
		#
		# I'm a copy and forward to my master
		# 
	
		if ((st.peerloc==myloc) && (st.peerip != source))
		{
			spath=utils->CARIBE_NS+"/"+st.replica.volume+"/peer";
			path=spath+file;
			err=forwarder_msg(st,path,msg);
		}
	}
	else
	{
		#
		# I'm a master and forward to my childs
		#
		
		spath=utils->CARIBE_NS+"/"+st.replica.volume+"/storm";
		sd:=sys->open(spath, sys->OREAD);
		if (sd==nil)
			return sprint("Dir %s of volume %s not found",spath,st.replica.volume);

		(childs,n):=readdir->readall(sd,readdir->NAME);
		if ( (n<0) || (st.childs==0) )
			return nil;	# No propago a hijos pero no hay error

		for (i:=0; i<n; i++)
		{
			childip:=utils->resolvname(childs[i].name);
			if (childip == source)
				continue;	# Este hijo es el autor de la sesion

			path=spath+"/"+childs[i].name+file;
			err=forwarder_msg(st,path,msg);
			if (err!=nil)
				removechild(st,childs[i].name, err);
		}
	}

	return err;
}



### 
## Reenvio de mensajes
###

forwarder_msg(st:ref Storminfo, path:string ,msg:ref Tmsg):string
{

	pick m := msg {
	Write =>
		f:=sys->open(path, Sys->OWRITE);
		if (f==nil)
			return sprint("error at open %s",path);

		#log (stormlog,sprint("write (%s)",path));
		n:=sys->pwrite(f,m.data,len m.data,m.offset);
		if (n<len m.data)
			return sprint("error at write %s",path);

	Create =>
		if (path[len path-1] != '/')
			path=path+"/"+m.name;
		else
			path=path+m.name;

		#log (stormlog,sprint("fwd create (%s)",path));
		f:=sys->create(path,m.mode,m.perm);
		if (f==nil)
			return sprint("error at create %s",path);
	
	Remove =>
		#log (stormlog,sprint("fwd remove (%s)",path));
		if (sys->remove(path)<0)
			return sprint("error at remove %s",path);

	Wstat=>
		if (sys->wstat(path,m.stat)<0)
			return sprint("error at wstat %s",path);

	* =>

	}	

	return nil;
}



removechild(st:ref Storminfo, child:string, error:string)
{
	if (error!=nil)
		log (stormlog,sprint("Flushing child %s. %s",child,error));
	else
		log (stormlog,sprint("Flushing child %s",child));
		
	spath:=utils->CARIBE_NS+"/"+st.replica.volume+"/storm/";	
	if (creplica->unmount(spath+child)==nil)
	{
		utils->cleanns(spath+child);
		st.childs--;
	}
}






###
##	Control del estado de la replica (versiones, disco, etc...)
###

servicephase (st:ref Storminfo, fd: ref sys->FD)
{
	log(stormlog,"Storm module: Service phase  ... ");
	v,pv:big;
	e:string;
	for (;;)
	{
		v=st.replica.vers;
		now:=sys->millisec();
		if ((st.master==0) && ((now - st.last_fw) > VERS_REQ_WINDOW))
		{
			(pv,e)=versreq(fd,st.replica.volume);
			## log(stormlog,sprint("Peer version: %d My version: %d",int pv,int v));
			if ((e!=nil) || (v<pv))
				break;	# rompemos el bucle
		}
		sys->sleep(STATUS_REFRESH);

	} # for

	##log(stormlog,sprint("Peer v: %d My v: %d",int pv,int v));
}


versreq (fd: ref sys->FD, volume:string):(big,string)
{
	if (fd==nil)
		return (big -1,"No peer fd");

	e:string;
	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_VERS,data));

	if ((rm==nil) || (rm.cmd!=csp->OK))
		return (big -1,"Failed vers request to peer");

	v:=big rm.data;
	return (v,nil);
}


diedreq (fd: ref sys->FD, volume:string):string
{
	if (fd==nil)
		return "No peer fd";

	e:string;
	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_PEER_DIED,data));

	if ((rm==nil) || (rm.cmd!=csp->OK))
		return "Failed vers request to peer";

	return nil;
}




