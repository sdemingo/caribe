implement Cloader;

include "mods.m";
	fprint,sprint:import sys;
	stderr,stdout,log:import utils;
	MAX_CCINDEX : import creplica;

include "readdir.m";
	readdir: Readdir;


ccmod:adt {
	cc: Cc;
	name: string;
};

ccs: array of ccmod;
loaded:int;

init (d:Dat):string
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;

	fd:=sys->open(MODS_PATH, sys->OREAD);
	if (fd==nil)
		return "Ccloader: cc mods not founded\n";
	
	(ccd,n):=readdir->readall(fd,readdir->NAME);
	if (n<0)
		return "Ccloader: cannot read cc dir\n";

	ccs= array [MAX_CCINDEX] of ccmod;
	loaded=0;
	for (i:=0; i<n; i++)
	{
		ccs[i].name=ccd[i].name;
		ccs[i].cc=load Cc MODS_PATH+"/"+ccd[i].name;
		if (ccs[i].cc !=nil)
		{
			ccs[i].cc->init(d);
			loaded++;
		}
	}
		
	return nil;
}

getmaxcc():int
{
	i:=0;

	while (ccs[i].cc!=nil)
		i++;
	
	return i;
}


getcc(cn:string):(Cc,int) 
{
	if (cn==nil)
		return (nil,-1);

	for (i:=0;i<len ccs;i++)
	{
		(index,n):=str->splitl(ccs[i].name,".");
		if (len(n)>0)
			name:=n[1:];
		if (cn+".dis" == name)
			return (ccs[i].cc,int index);
	}
	return (nil,-1);
}


getccname(cc:int):string
{
	if ( (cc>creplica->MAX_CCINDEX) || (cc>loaded))
		return nil;

	for (i:=0;i<len ccs;i++)
	{
		(index,n):=str->splitl(ccs[i].name,".");
		if ((int index) == cc)
		{
			(name,dis):=str->splitl(n[1:],".");
			return name;
		}	
	}
	return nil;
}

getallcc():list of Cc
{	
	m:list of Cc = nil;
	for (i:=0;i<len ccs;i++)
		if (ccs[i].cc!=nil)
			m=ccs[i].cc::m;
	return m;
}
