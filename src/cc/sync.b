implement Sync;

include "mods.m";
	Connection,sprint,open,read,write,stat,create,wstat,fwstat,remove, FD :import sys;
	stderr,stdout,log,sysname,panic,timeout,splitaddr,
		killgrp,synclog,isignoredpath,copyfile,CARIBE_ERR,CARIBE_NS,CARIBE_VOLS,
		mkns:import utils;
	Service,Attributes:import registries;
	Tree, Navop: import ctree;
	Evt:import cevent;
	Tmsg, Rmsg :import styx;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	HISTORY_PATH,openhis,cmphis,delhis,createhis,copyhis	:import history;
	Hash: import hashtable;

include "readdir.m";
	readdir: Readdir;


include "keyring.m";
	keyring: Keyring;


DEFAULT_QUOTA	: con big 500;	# bytes;



init (d:Dat):string
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
	keyring=load Keyring Keyring->PATH;
	return nil;
}


# SyncEvents types

EV_CONFLICT	: con "CONFLICT";
EV_UPDSRC	: con "UPDATE_SOURCE";
EV_UPDTRG	: con "UPDATE_TARGET";
EV_ERROR	: con "ERROR";


SyncEvent:adt
{
	rpath: string; 		# relative path
	etype: string;		# event type

	new	: fn(rpath:string,etype:string): ref SyncEvent;
	text	: fn(ev: self ref SyncEvent): string;
};

RSyncEvent: type ref SyncEvent;


SyncEvent.new(rpath:string,etype:string): ref SyncEvent
{
	e:=ref SyncEvent;
	e.rpath=rpath;
	e.etype=etype;

	return e;
}

SyncEvent.text(ev: self ref SyncEvent):string
{
	return sprint("[%s] [%s]",ev.rpath,ev.etype);
}



print_events(events:ref Hash[RSyncEvent], src, dest:string){

	aev:=events.all();
	while(aev!=nil){
		ev:=hd aev;
		if (ev!=nil)
		{
			case (ev.val.etype)
			{
			EV_CONFLICT =>
				log(synclog,sprint("[%s] Conflict detected",ev.val.rpath));
			EV_UPDSRC =>
				log(synclog,sprint("[%s] File updated on %s",ev.val.rpath,src));
			EV_UPDTRG =>
				log(synclog,sprint("[%s] File updated on %s",ev.val.rpath,dest));
			EV_ERROR =>
				log(synclog,sprint("[%s] Error detected",ev.val.rpath));	
			}		
		}
		aev=tl aev;
	}
}



conflicts(events:ref Hash[RSyncEvent], volume,src, dest:string)
{

	#checkdir(volume,src);

	if (mkns(CARIBE_ERR+"/"+volume)!=nil)
	{
		log (stderr,sprint("Fallo la creacion de %s",CARIBE_ERR+"/"+volume));
		return;
	}

	aev:=events.all();
	while(aev!=nil){
		ev:=hd aev;
		if ((ev!=nil) && (ev.val.etype==EV_CONFLICT))
		{
			# meto mi version local en conflicts y me
			# quedo con la version del directorio origen
			# que deberia ser la del peer

			conflictfile(volume,ev.val.rpath);
		}
		aev=tl aev;	
	}

}





synctree(volume:string,src:string,dest:string):string
{
	# src: peer
	# dest: local rootpath

	events:=Hash[RSyncEvent].new(50);
	e:int;

	(e,events)=sync_node(events,src,dest,nil);
	(e,events)=sync_node(events,dest,src,nil);

	if (e<0)
		return "Error at sync";

	print_events(events,src,dest);
	conflicts(events,volume,src,dest);
	return nil;
}



conflictfile(volume:string,rpath:string):string
{
	localf:=CARIBE_VOLS+"/"+volume+rpath;
	peerf:=CARIBE_NS+"/"+volume+"/peer"+rpath;
	errf:=CARIBE_ERR+"/"+volume+utils->conflictpath(rpath);

	copyfile(localf,errf);	# save local copy in errs
	copyfile(peerf,localf);	# update the public version on the volume

	src:=CARIBE_NS+"/"+volume+"/peer";
	dest:=CARIBE_VOLS+"/"+volume;
	copyhis(src,dest,rpath);

	log (synclog,sprint("Conflict version of %s. Updated with peer version",dest));
	return nil;
}



remove_node(root,rpath:string):int
{	
	path:=root+rpath;
	(err,stat):=stat(path);
	if (err<0)
		return err;	# No existe path en el src

	if (stat.mode & Sys->DMDIR )
	{
		e:=0;
		(d, n) := readdir->init(path, Readdir->NONE|Readdir->COMPACT);
		for(i := 0; i < n; i++)
			e=remove_node(root,rpath+"/"+d[i].name);
		if (e<0)
			return e;
		
		return sys->remove(path);	# path directory must be empty
	}
	else
	{
		if (delhis(root,rpath)!=nil)
			return -1;
		return sys->remove(path);
	}	
}


create_node(root,rpath:string,dir:sys->Dir):int
{
	path:=root+rpath;

	fd:ref sys->FD;
	if (dir.mode & sys->DMDIR)
		fd=sys->create(path, sys->OREAD, 8r777 | Sys->DMDIR);
	else
		fd=sys->create(path,sys->OWRITE,dir.mode);

	if (fd==nil)
		return -1;

	fd=createhis(root,rpath,dir.mode);
	if (fd==nil)
		return -1;

	return 0;
}




sync_node(ev:ref Hash[RSyncEvent],root1:string,root2:string,path:string):
				(int,ref Hash[RSyncEvent])
{

	if (isignoredpath(utils->ignored_paths,path)){
		return (history->NO_NEWER,ev);
	}

	if (path==nil)
		path="";

	src:=root1+path;		# absolute paths
	dest:=root2+path;

	(serr,sstat):=stat(src);
	if (serr<0)
		return (serr,ev);	# No existe path en el src

	(derr,dstat):=stat(dest);
	if (derr<0)	# No existe destino
	{	
		if (openhis(root2,path)!=nil)	# pero exisitio y lo borraron.
		{
			# lo borramos en origen y terminamos
			# pues tanto si es un dir como un fichero
			# hemos terminado de sync todo bajo el.

			log (synclog,sprint("remove %s in %s",path,root1));

	      		return (remove_node(root1,path),ev); 
	      		
	   	}else{	

			# Si lo creamos en el destino 
			# (usando los metadatos del origen) 
			# no retornamos, pues tanto si es un
			# dir como un directorio
			# ahora hemos de sincronizarlo	
			   
			log (synclog,sprint("create %s in %s",path,root2));
	      		cerr:=create_node(root2,path,sstat);
			if (cerr<0)
				return (cerr,ev);	# fallo la creacion
			
	   	}
	}
	
	if (sstat.mode & Sys->DMDIR )
	{	
		(d, n) := readdir->init(src,Readdir->NONE|Readdir->COMPACT);
		for(i := 0; i < n; i++)
		{
			e:int;
			(e,ev)=sync_node(ev,root1,root2,path+"/"+d[i].name);
			if (e<0)
				log (synclog,"Error at sync "+path+"/"+d[i].name);
		}

	}else{
		e:ref SyncEvent = nil;
	    	(newest,c):=cmphis(root1,root2,path);
	    	case(c)
		{
	    	history->NO_NEWER =>
		  	#log (synclog,sprint("skip %s",path));

	    	history->LEFT_NEWER =>
		 	# el mas nuevo es root1+path
	          	# copy copia tanto el fichero 
	          	# como su historia
			copyfile(src,dest);
			copyhis(root1,root2,path);
			e=SyncEvent.new(path,EV_UPDTRG);

	   	history->RIGHT_NEWER =>
		  	# el mas nuevo es root2+path
			copyfile(dest,src);
			copyhis(root2,root1,path);
			e=SyncEvent.new(path,EV_UPDSRC);

		history->CONFLICT =>
			e=SyncEvent.new(path,EV_CONFLICT);


		history->ERROR => 
			e=SyncEvent.new(path,EV_ERROR);

	    	}
		if (e!=nil)
			ev.set(path,e);	
	}

	return (0,ev);
}



#
# Esta rutina se queda con la version más nueva del fichero
# sobreescribiendo la más antigua.
#

newest (f1,f2:string):int
{
	e:int;
	(e1,df1):=stat(f1);
	(e2,df2):=stat(f2);
	if ( (e1<0) || (e2<0) )
		return -1;

	if (df1.qid.vers < df2.qid.vers)
		e=copyfile(f2,f1);	
	else
		e=copyfile(f1,f2);
	return e;
}




checksum(file:string):string
{

	cksum	 	: string;
	n		: int;
	nbytes		: big;
	fd		: ref Sys->FD;
	digeststate 	: ref Keyring->DigestState;

	if (file == nil)
		return nil;
	
	fd = sys->open(file, Sys->OREAD);
	if (fd == nil)
		return nil;

	buf := array [Sys->ATOMICIO] of byte;
	while ((n = sys->read(fd, buf, len buf)) > 0)
	{
		digeststate = keyring->sha1(buf[:n], n, nil, digeststate);
		nbytes += big n;
	}
	if (n < 0)
		return nil;

	digest := array[keyring->SHA1dlen] of byte;
	keyring->sha1(buf[:n], n, digest, digeststate);

	#cksum += sys->sprint("SHA (%s) = ", file);
	for (i := 0; i < keyring->SHA1dlen; i++)
	{
		cksum += sys->sprint("%2.2ux", int digest[i]);
	}

	return cksum;
}




