implement Cc;

include "mods.m";
	Connection,sprint,open,read,write,stat,create,Dir :import sys;
	stderr,stdout,log,sysname,panic,CSP_PORT,CARIBE_VOLS,CARIBE_NS,
		timeout,splitaddr,killgrp,rainlog,dropprefix,rawstr2pairs:import utils;
	Tree: import ctree;
	cspsrv,Cmsg,Cop,cspopen,csprpc:import csp;
	Service,Attributes:import registries;
	Evt:import cevent;
	Rmsg,Tmsg:import styx;
	Cmodule, Replica, OP, SYNC, CORR, DIED, CC_CMD_FREE,CC_CMD_INFO: import creplica;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	#xpathfile : import xsrv;
	Hash: import hashtable;


include "readdir.m";
	readdir: Readdir;


CC_NAME		: con "rain";
CC_LEVEL		: con 2;
STATUS_REFRESH	: con 10000;	# ms
VERS_REQ_WINDOW	: con 3000;
STABLE_INIT_SIZE	: con 50;
CHILDS_MAX_NUM 	: con 100;

FWD_MAX_TRIES	: con 5;
FWD_TIME_TRY_AGAIN 	: con 350;

SESSION_SIZE_BUF	: con 5000;
SESSION_WAIT_FOR_ASK	: con 500;
SDIRMODE	: con 16r01;
SDIRTY		: con 16r02;	# Que esta sucia y tiene cambios pero por ahora no se sincr
SSYNC		: con 16r04;	# Que se debe sincronizar ahora



init (d:Dat)
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
}


##	
##	Message sessions
##	

Session:adt{
	c  	: chan of ref Styx->Tmsg;
	file 	: string;
	source	: string;
	mq	: array of ref Styx->Tmsg;
	sz	: int;
	flags	: int;

	new	: fn(file:string, source:string, rt: ref Raininfo):ref Session;
	add	: fn(sn: self ref Session, m: ref Styx->Tmsg);
	free	: fn(sn: self ref Session);
	flush	: fn(sn: self ref Session, rt: ref Raininfo):string;
};


RSession: type ref Session;



Session.new(file:string, source:string, rt: ref Raininfo):ref Session
{
	src:=CARIBE_VOLS+"/"+rt.replica.volume+file;
	(e,dir):=sys->stat(src);
	if (e<0)
	{
		log (rainlog, sprint("File %s not found. Session no create", src));
		return nil;
	}

	sn:=ref Session;
	sn.c=chan of ref Styx->Tmsg;
	sn.file=file;
	sn.source=source;
	sn.flags=0;
	sn.mq=array [SESSION_SIZE_BUF] of ref Styx->Tmsg;
	sn.sz=0;

	if (dir.mode & Sys->DMDIR)
		sn.flags|=SDIRMODE;
	
	rt.stable.set(file+source,sn);
	spawn session(rt,sn);
	return sn;
}


Session.add(sn: self ref Session, m: ref Styx->Tmsg)
{
	if (sn.sz == len sn.mq)
	{
		newq:=array [(len sn.mq) + SESSION_SIZE_BUF] of ref Styx->Tmsg;
		for (i:=0;i<len sn.mq; i++)
			newq[i]=sn.mq[i];
		sn.mq=newq;
	}
	sn.mq[sn.sz]=m;
	sn.sz++;	
}



Session.flush(sn: self ref Session, rt: ref Raininfo):string
{

	e:string=nil;

	rt.lock<-=1;
	file:=sn.file;

	#
	# Propago a mi peer. (siempre y cuando este no sea el autor de la sesión)
	#

	fpath:=CARIBE_NS+"/"+rt.replica.volume+"/peer";
	fd:=sys->open(fpath, sys->OREAD);
	if (fd==nil)
	{
		<-rt.lock;
		return sprint("Rain error. No file %s",fpath);
	}

	peer : string =nil;
	if (rt.peer!=nil)
	{
		(nil,pname,nil):=splitaddr(rt.peer.addr);	#c, peer name, vol name
		peer=utils->resolvname(pname);
	}

	myloc:=cloc->where(sysname());

	if (peer!=nil)	# no soy raiz
	{
		if ( (peer != sn.source) && (rt.peerloc!=myloc)) 
		{
			path:=fpath+file;
			for (j:=0;j<sn.sz; j++)	# Para cada mensaje de la sesion
			{
				m:=sn.mq[j];
				er:=forwarder(rt, path, m);
				if (er!=nil)
				{
					log(rainlog, sprint ("Error at forwarder to peer %s. %s",peer,er));
					# Acabo de obtener un error al propagar
					# una sesion a mi padre. Si esa sesion
					# tenia el flag de DIRTY hay cambios no propagados
					# Quiza mi peer tenia el cerrojo echado de ese 
					# fichero y ha rechazado la sesion ¿Que ocurre ahora?
					# El fichero loca puedo pasarlo a conflicts y asi
					# no perderlo pero ¿Y la historia? esta contaminada
					# por un commit no propagado al padre.

					sync->conflictfile(rt.replica.volume,path);
				}
			}
		}
	}



	#
	# Propago a mis hijos
	#

	if (rt.childs==0)
	{
		<-rt.lock;
		return nil;
	}

	spath:=CARIBE_NS+"/"+rt.replica.volume+"/rain";
	sd:=sys->open(spath, sys->OREAD);
	if (sd==nil)
	{
		<-rt.lock;
		return sprint("Rain error. No file %s",spath);
	}
	
	(childs,n):=readdir->readall(sd,readdir->NAME);
	if (n<0)
	{
		<-rt.lock;
		return "Rain error. No rain directory";
	}

	for (i:=0; i<n; i++)	# para cada hijo
	{
		childip:=utils->resolvname(childs[i].name);
		if (childip == sn.source)
			continue;	# Este hijo es el autor de la sesion

		path:=spath+"/"+childs[i].name+file;
		for (j:=0;j<sn.sz; j++)	# Para cada mensaje de la sesion
		{
			m:=sn.mq[j];
			#log (rainlog, sprint("Propago sesion a mi hijo %s",childs[i].name));
			er:=forwarder(rt, path, m);
			if (er!=nil)
			{
				log(rainlog, sprint ("Error at forwarder msg to %s. %s",childs[i].name,er));
				removechild(rt,childs[i].name);
				break;
			}
		}
	}

	<-rt.lock;
	return e;
}



Session.free(sn: self ref Session)
{
	sn.file=nil;
	sn.flags=0;
	sn.mq=nil;
	sn.sz=0;
}







##	
##	Metadata of rain module
##

Raininfo:adt	
{
	replica	: ref Replica;
	mod	: ref Cmodule;
	peer	: ref Registries->Service;	# parent
	peerloc	: string;
	childs	: int;
	gid	: int;				# gid of proccess to control this Storminfo
	stable	: ref Hash[RSession];
	lock	: chan of int;
	last	: int; 	# ultima recepción
};





###
##	Creación y arranque de un volumen
###


start (rep:ref Replica):ref Cmodule
{
	mod:=Cmodule.new();
	mod.ccname=CC_NAME;
	mod.ccindex=CC_LEVEL;
	log (rainlog,sprint("Add rain coherency to replica of %s",rep.volume));
	ec:=chan of string;
	spawn rainproc(rep,mod,ec);
	error:=<-ec;
	if (error!=nil)
		return nil;
	return mod;	
}


free (mod: ref Creplica->Cmodule): string
{ 
	log(stdout,"Destroying rain");
	if (mod.status==DIED)
		return nil; 

	reply:=chan of string;
	mod.ctl<-=(CC_CMD_FREE,reply);
	err:=<-reply;
	log (stdout,sprint("Mato grupo %d de rain",mod.gid));
	killgrp(mod.gid);

	return nil;
}


info (mod: ref Creplica->Cmodule): list of (string,string)
{
	if (mod.status==DIED)
		return nil; 	

	reply:=chan of string;
	mod.ctl<-=(CC_CMD_INFO,reply);
	raw:=<-reply;
	l:=rawstr2pairs(raw);

	return l;
}


free_module(rt: ref Raininfo,mod:ref Creplica->Cmodule):string
{
	# Primero aviso a todos los childs de que voy a 
	# morirme mandandoles un csp

	spath:=CARIBE_NS+"/"+rt.replica.volume+"/rain";
	sd:=sys->open(spath, sys->OREAD);
	if (sd!=nil)
	{
		(childs,n):=readdir->readall(sd,readdir->NAME);
		if (childs!=nil)
		{
			for (i:=0; i<n; i++)
			{
				childip:=utils->resolvname(childs[i].name);
				mcsp:="tcp!"+childip+"!"+string (utils->CSP_PORT);
				fd:=cspopen(mcsp);
				if (fd==nil)
					return "Failed csp comunication to child during the free_module";
				diedreq(fd,rt.replica.volume);
				removechild(rt,childs[i].name);
			}
		}
	}
	rt.childs=0;
	rt.peer=nil;
	rt.peerloc=nil;
	mod.changeto(rt.replica, DIED);
	
	return nil;
}



rainproc(rep:ref Replica, mod: ref Cmodule, error:chan of string)
{
	gid:=sys->pctl(sys->NEWPGRP,nil);

	err: string=nil;
	fd:=ref sys->FD;

	rt:=ref Raininfo;
	rt.mod=mod;
	rt.replica=rep;
	rt.gid=gid;
	mod.gid=gid;
	rt.stable=Hash[RSession].new(STABLE_INIT_SIZE);
	rt.childs=0;
	if (rep.root==1)
	{
		rt.peer=nil;		# la réplica es root
		rt.peerloc=nil;
	}
	else
	{
		rt.peer=rep.peer;	# la réplica no es root
		rt.peerloc=rt.peer.attrs.get("loc");
	}

	rt.lock=chan [1] of int;
	spawn srv (rt, mod);
	spawn cspproc(rt,mod.csp);

	error<-=nil;

	for(;;){
		mod.changeto(rep,CORR);		# Initial status is always CORR 
		(err,fd)=initphase(rt,rep,mod);	# Aftter that  the status must be OP
		if (err!=nil)
			break;
		servicephase (rt, fd);		# block until some problem happens
	}

	log(rainlog,sprint("Rain panic and died. [%s]",err));
	free(mod);
}


initphase(rt: ref Raininfo,rep:ref Replica,mod: ref Cmodule):(string,ref sys->FD)
{
	log(rainlog,"Rain module: Initialization phase  ... ");
	utils->cleanns(CARIBE_NS+"/"+rep.volume+"/rain");

	fd: ref sys->FD;
	e:string;
	(lcc,hcc):=rep.ccindex();

	# Aqui deberia haber un cerrojo pues dos modulos no pueden
	# entrar en la initphase a la vez. Si ambos chekean el rep.ccindex
	# a la vez tendriamos una cond carrera

	if ((mod.ccindex < lcc) && (rt.peer!=nil))
	{	
		log(rainlog,"Replica coherency level is higher. Syncronizing ...");
		mod.changeto(rep,SYNC);	
		(e,fd)=syncproc(rt);
		if (e!=nil)
			return (e,nil);	
	}

	log(rainlog,"... Syncronizing done");

	# Recargo el creplica pues los mensajes durante la sincronizacion
	# se han modificado directamente el SF sin pasar por el creplica
	rt.replica.fill();

	mod.changeto(rep,OP);

	return (nil,fd);
}



syncproc(rt: ref Raininfo):(string,ref sys->FD)
{
	
	##
	## Notificacion por Csp al peer
	##

	if (rt.peer==nil)
		return ("No peer found",nil);
	(p,paddr,nvol):=splitaddr(rt.peer.addr);	#c, peer name, vol name

	mcsp:="tcp!"+paddr+"!"+string (utils->CSP_PORT);
	fd:=cspopen(mcsp);
	if (fd==nil)
		return (sprint("Failed csp comunication to peer %s",mcsp),fd);

	##
	## Solicito sync por Csp
	##

	data:=str->quoted(utils->sysname()::"rain"::rt.replica.volume::string rt.replica.port::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_GET,data));
	if ((rm==nil ) || (rm.cmd!=csp->OK))
		return (sprint("Sincronization rejected by %s",rt.peer.addr),fd);

	log (rainlog, "Conexion al peer ok. Empezamos a checkear ficheros");

	##
	## Obtencion de ficheros
	##

	# No remonto a mi peer. Simplemente cogo los archivos
	# de /peer pues hay tengo ya montado a mi padre

	e:="";

	peermp:=CARIBE_NS+"/"+rt.replica.volume+"/peer";
	me:=rt.replica.rootpath;

	e2:=sync->synctree(rt.replica.volume,peermp, me);
	if (e2!=nil)
		return (sprint("Failed during syncronization to peer %s",rt.peer.addr),fd);

	(mvers,e4):=versreq(fd, rt.replica.volume);
	if (e4!=nil)
		return ("Failed vers request to peer  during sync",fd);

	rt.replica.vers=mvers;

	return (nil, fd);
}





cspproc(rt:ref Raininfo, cspchan: chan of ref Cop)
{
	for(;;)
	{
		op:=<-cspchan;
		msg:=op.msg;
		reply:=op.reply;
		case (msg.cmd)
		{
		csp->REPL_GET =>
			data:=str->unquoted(msg.data);
			child:=hd data;
			cc:=hd (tl data);
			vol:=hd (tl (tl data));
			port:=hd (tl (tl (tl (data))));
			addr:="tcp!"+child+"!"+port;
			mp:=CARIBE_NS+"/"+vol+"/rain/"+child;
			removechild(rt,child);
			sadd:="tcp!"+child+"!"+string port;
			e:=creplica->mount(sadd, mp);
			if (e!=nil)
				reply<-=ref Cmsg(csp->ERROR,nil);
			else
			{
				reply<-=ref Cmsg(csp->OK,nil);
				rt.childs++;
				log (rainlog,sprint("Discovered new child named %s",child));
			}	

			
		* =>
			reply<-=ref Cmsg(csp->ERROR,nil);
		}

	}

}




###
##	Reenvio de mensajes y protocolo de coherencia
###

srv(rt:ref Raininfo, mod:ref Cmodule)
{	
	for (;;)
	{
	alt{
	(qid,msg,source):=<-mod.req =>

		file:=rt.replica.xfs.tree.getpath(qid);
		rt.last=sys->millisec();

		#log (rainlog, sprint("=>[Rain] %s from %s",msg.text(),source));

		err:=checklocks(rt,qid,msg,source);
		if (err!=nil)
		{
			mod.reply<-=err;
			continue;
		}	


		path:=file;
		s:ref Session;
		s=rt.stable.get(file+source);
		if (s==nil)
			s=Session.new(file,source,rt);

		if (s!=nil)
		{
			mod.reply<-=nil;
			s.c<-=msg; 	# enviamos el mensaje a la sesion correspondiente.
		}
		else
		{
			log (rainlog, sprint("=>[Rain] %s from %s ha fallado",msg.text(),source));
			mod.reply<-="Error en rain";
		}	

		#log (rainlog, sprint("<=[Rain] %s",msg.text()));




	(cmd,reply):=<-mod.ctl =>

		case cmd
		{
		CC_CMD_FREE =>	
			err:=free_module(rt,mod);
			if (reply!=nil)
				reply<-=err;

		CC_CMD_INFO =>	
			str:=info_module(rt,mod);
			if (reply!=nil)
				reply<-=str;

		* =>
			log(stdout,"recibido CMD desconocido");
			if (reply!=nil)
				reply<-=nil;
		}
	}
	}
}






checklocks(rt:ref Raininfo, qid:big, msg:ref Tmsg, source:string):string
{
	err:string = nil;

	file:=rt.replica.xfs.tree.getpath(qid);	# only for debug msg

	pick m := msg {

	Open =>
		err=secureopen(rt,qid,m.mode,source);
		#if (err==nil)
		#	log (rainlog,sprint("Bloqueo fichero %s por open para %s",file,source));
	
	Clunk =>
		#log (rainlog,sprint("Desbloqueo fichero %s",file));
		err=rt.replica.xfs.tree.unlock(qid);

	Write =>
		# Comprobar el autor de los mensajes

		lock:=rt.replica.xfs.tree.openlock(qid);
		if ((lock.owner!=nil) && (lock.owner != source))
			err="File is locked";
		rt.replica.xfs.tree.closelock(qid);

	Wstat =>
		lock:=rt.replica.xfs.tree.openlock(qid);
		if ((lock.owner!=nil) && (lock.owner != source))
			err="File is locked";
		rt.replica.xfs.tree.closelock(qid);


	* =>

	}	

	return err;
}


secureopen(st:ref Raininfo, qid:big, mode:int, source:string):string
{
	err:string = nil;

	# Si el open de escritura cerramos el fichero, si el open
	# es de lectura comprobamos el cerrojo

	lock:=st.replica.xfs.tree.openlock(qid);

	if ((mode & Sys->OWRITE) || (mode & Sys->ORDWR))
	{
		# Si el nivel del cerrojo mas relajado o
		# el origen del mensaje es el master
		# Expropio:

		if (lock.level > CC_LEVEL) 
			st.replica.xfs.tree.lock(qid,source,CC_LEVEL);
	}

	st.replica.xfs.tree.closelock(qid);
	return err;
}


session(rt:ref Raininfo,s:ref Session)
{
	# log (rainlog,sprint("Starting new session(%d) over file %s",sys->pctl(0,nil),s.file));
	for (;;)
	{
		msg:=<-s.c;
		s.add(msg);
		pick m := msg {
		Create =>	
			if (s.flags & SDIRMODE)
				s.flags|= SSYNC | SDIRTY;
			else
				s.flags|= SDIRTY;

		Remove =>	
			s.flags|=SSYNC | SDIRTY;

		Write =>
			s.flags|=SDIRTY;

		Clunk =>	
			s.flags|=SSYNC;	

		* =>

		}
		
		if (s.flags & SSYNC)
			break;
	}

	# la quitamos de la tabla para que nadie nos mande nada más
	# mientras dura la sincronización

	rt.stable.del(s.file+s.source);

	# fin de la sesion. Deberiamos iniciar la sincronizacion
	# del fichero para mantener la coherencia.

	if (s.flags & SDIRTY)
	{
		e:=s.flush(rt);
	}
	# log (rainlog,sprint("Free session(%d) over file %s",sys->pctl(0,nil),s.file));
	s.free();
}




forwarder(rt:ref Raininfo, path:string, msg:ref Tmsg):string
{
	e:string=nil;
	pick m := msg {
	Write =>
		#log (rainlog,sprint("open&write (%s)",path));
		f:=sys->open(path, Sys->OWRITE);
		if (f==nil)
		{
			e=sprint("error at open %s",path);
			break;
		}

		n:=sys->pwrite(f,m.data,len m.data,m.offset);
		if (n<len m.data)
			e=sprint("error at write %s",path);
		sys->seek(f,m.offset+big len m.data,sys->SEEKSTART);

	Create =>
		if (path[len path-1] != '/')
			path=path+"/"+m.name;
		else
			path=path+m.name;

		#log (rainlog,sprint("create (%s)",path));
		(es,n):=sys->stat(path);
		if (es<0)
			if (sys->create(path,m.mode,m.perm)==nil)
				e=sprint("error at create %s",path);

	
	Remove =>
		#log (rainlog,sprint("remove (%s)",path));
		if (sys->remove(path)<0)
			e=sprint("error at remove %s",path);

	Wstat=>
		#log (rainlog,sprint("wstat (%s)",path));
		if (sys->wstat(path,m.stat)<0)
			e=sprint("error at wstat %s",path);

	* =>

	}

	return e;
}





removechild(rt:ref Raininfo, child:string)
{
	log (rainlog,sprint("Removing child %s",child));		
	spath:=CARIBE_NS+"/"+rt.replica.volume+"/rain/";	
	if (creplica->unmount(spath+child)==nil)
	{
		utils->cleanns(spath+child);
		rt.childs--;
	}
}





getpath(rt:ref Raininfo, path:big):string
{
	spath:=rt.replica.xfs.tree.getpath(path);
	prefix:="./";
	return "/"+dropprefix(prefix,spath);
}


getparentpath(file:string):string
{
	(ppath,name):=str->splitstrr(file,"/");
	if (ppath!="/")
		return ppath[:(len ppath)-1];
	return ppath;
}


###
##	Control del estado de la replica (versiones, disco, etc...)
###

servicephase (rt:ref Raininfo, fd: ref sys->FD)
{
	log(rainlog,"Rain module: Service phase");
	for (;;)
	{
		## Control de la version

		v:=rt.replica.vers;
		now:=sys->millisec();	
		if ((rt.peer!=nil) && ((now - rt.last) > VERS_REQ_WINDOW))
		{	
			##log(rainlog,"Check peer version ...");
			(pv,e):=versreq(fd, rt.replica.volume);
			if ( (e!=nil) || (v<pv))
			{
				##log (rainlog, sprint("Service phase error: %s",e));
				break;	# rompemos el bucle
			}
		}
		sys->sleep(STATUS_REFRESH);

	} # for
}


info_module(rt: ref Raininfo,mod: ref Cmodule):string
{
	s:="name="+CC_NAME+" ";
	s=s+"ccindex=2"+" ";
	s=s+"status="+sprint("%d",mod.status)+" ";

	ch:="";
	spath:=CARIBE_NS+"/"+rt.replica.volume+"/rain";
	sd:=sys->open(spath, sys->OREAD);
	if (sd!=nil)
	{
		(childs,n):=readdir->readall(sd,readdir->NAME);
		for (i:=0; i<n; i++)
			ch=ch+childs[i].name+",";
	}

	if (len(ch)==0)
		s=s+"childs=<nochilds> ";
	else
		s=s+"childs="+ch+" ";

	return s;
}




versreq (fd: ref sys->FD, volume:string):(big,string)
{
	if (fd==nil)
		return (big -1,"No peer fd");
	e:string;
	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_VERS,data));
	if ((rm==nil) || (rm.cmd!=csp->OK))
	{
		e="Failed vers request to peer";
		return (big -1,e);
	}
	v:=big rm.data;
	return (v,nil);
}





diedreq (fd: ref sys->FD, volume:string):string
{
	if (fd==nil)
		return "No peer fd";

	e:string;
	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_PEER_DIED,data));

	if ((rm==nil) || (rm.cmd!=csp->OK))
		return "Failed vers request to peer";

	return nil;
}


