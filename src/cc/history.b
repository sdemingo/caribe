implement History;

include "mods.m";
	Connection,sprint,open,read,write,stat,create,wstat,fwstat,remove, FD :import sys;
	stderr,stdout,log,sysname,panic,timeout,splitaddr,killgrp,synclog:import utils;
	Service,Attributes:import registries;
	Tree, Navop: import ctree;
	Evt:import cevent;
	Tmsg, Rmsg :import styx;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;

include "readdir.m";
	readdir: Readdir;


ERR_CONFLICT	: con "Files are different";
ERR_NOEXISTS	: con "History file no exists";

LEN_SZ		: con 2;
INT16_SZ	: con 2;
BUF_SZ		: con 1024;

DELETE_FILE_MARK	: con "REMOVE";


init (d:Dat):string
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
	return nil;
}


checkdir(root:string)
{
	(e,d):=sys->stat(root+HISTORY_PATH);
	if (e<0)
	{
		fd:=sys->create(root+HISTORY_PATH, sys->OREAD, 8r777| sys->DMDIR);
		if (fd==nil)
			log (stderr,sprint("Fallo la creacion de %s%s",root,HISTORY_PATH));
	}
}


writehis (root:string,rpath:string):string
{
	checkdir(root);

	hpath:=root+HISTORY_PATH+rpath;

	(e,d):=sys->stat(hpath);
	if (d.mode & sys->DMDIR)
	{
		return nil;
	}


	fd:=open(hpath,sys->OWRITE);
	if (fd==nil)
		fd=createhis(root,rpath,0);	

	if (fd==nil)
	{
		return sprint("Fallo la creacion de %s",hpath);
	}

	ck:=sync->checksum(root+rpath);
	mark:=ck+"\n";

	sys->seek(fd,big 0,sys->SEEKEND);
	sys->write(fd,array of byte mark,len(mark));

	return nil;
}



delhis(root:string, rpath:string):string
{
	#hpath:=root+HISTORY_PATH+rpath;
	#fd:=open(hpath,sys->OWRITE);
	#if (fd==nil)
	#	return "No history file found";

	#mark:=DELETE_FILE_MARK+"\n";
	#sys->seek(fd,big 0,sys->SEEKEND);
	#sys->write(fd,array of byte mark,len(mark));

	return nil;
}




createhis(root:string, rpath:string, perm: int):ref sys->FD
{
	checkdir(root);
	rpath=utils->dropprefix(HISTORY_PATH,rpath);

	##log (stdout,sprint("Creando historia de <root>/history%s",rpath));

	if ((rpath==nil) || (rpath=="") || (rpath=="/"))
		return openhis(root,nil);

	hpath:=root+HISTORY_PATH+rpath;
	h:=openhis(root,rpath);
	if (h!=nil)
		return h;
	else{
		#log (stdout,sprint("Creo historia en %s",hpath));
		if (perm & sys->DMDIR)
			return sys->create(hpath, sys->OREAD, 8r777 | Sys->DMDIR);
		else
			return sys->create(hpath, sys->OWRITE, perm);
	}
}


openhis(root:string, rpath:string):ref sys->FD
{
	if ((rpath==nil) || (rpath=="") || (rpath=="/"))
		rpath="";
	hpath:=root+HISTORY_PATH+rpath;
	return open(hpath,sys->OREAD);
}



#
# 
#
#

copyhis(root1,root2:string, rpath:string):string
{
	
	src:=root1+"/history"+rpath;
	dest:=root2+"/history"+rpath;

	if (utils->copyfile(src,dest)<0)
		return "Failed copy history file";
	return nil;
}





#
# Create a history file and the whole path to it
#

createhis_withpath(root:string, rpath:string):ref sys->FD
{

	s:=rpath;
	for (i:=0;i<len(s);i++){
		if (s[i]=='/')
			s[i]=' ';
	}
		
	steps:=str->unquoted(s);

	cpath:="";
	while(len(steps)>1)
	{
		d:=hd steps;

		cpath=cpath+"/"+d;
		c:=root+HISTORY_PATH+cpath;
		##sys->print("Creamos dir %s\n",c);
		fd:=create(c,sys->OREAD , sys->DMDIR | 8r777);

		steps=tl steps;
	}

	f:=hd steps;
	cpath=cpath+"/"+f;

	c:=root+HISTORY_PATH+cpath;
	##sys->print("Creamos file %s\n",c);
	fd:=create(c,sys->OWRITE,8r644);

	return fd;
}



# It returns a 1 if the file on root1 is the newest 
# or a 2 if the file on root2 is the newest. If the files
# are equals it returns 0. If there is a conflict
# detected it returns -1

cmphis(root1,root2:string, rpath:string):(string,int)
{
	pos:=0;
	buf1:=array [BUF_SZ] of byte;
	buf2:=array [BUF_SZ] of byte;

	f1:=open(root1+"/history"+rpath,sys->OREAD);
	if (f1==nil)
		return (ERR_NOEXISTS+":"+root1+"/history"+rpath,ERROR);

	f2:=open(root2+"/history"+rpath,sys->OREAD);
	if (f2==nil)
		return (ERR_NOEXISTS+":"+root2+"/history"+rpath,ERROR);

	for(;;){
		n1:=read(f1,buf1,BUF_SZ);
		n2:=read(f2,buf2,BUF_SZ);

		pos=cmpbytes(buf1[:n1],buf2[:n2]);

		if ((n1<BUF_SZ) || (n2<BUF_SZ))
		{
			if ((pos<n1) && (pos<n2))
				return (ERR_CONFLICT,CONFLICT);

			if (pos<n1)	#f1 is new
				return (root1+rpath, LEFT_NEWER);

			if (pos<n2)	#f2 is new
				return (root2+rpath, RIGHT_NEWER);
			break;
		}
	}
	
	# At that point files are the same
	return (root1+rpath, NO_NEWER);
}




# It returns the position until two buffers are 
# equal. If they have differents size this 
# position can not be bigger than the minimun size 

cmpbytes(b1, b2:array of byte):int
{
	min:=0;
	if (len(b1)<len(b2))
		min=len(b1);
	else	
		min=len(b2);

	for (i:=0; i<min; i++)
		if (b1[i]!=b2[i])
			return i;

	return i;
}









