implement Cc;

include "mods.m";
	Connection,sprint,open,read,write,stat,create,Dir :import sys;
	stderr,stdout,log,sysname,panic,CSP_PORT,CARIBE_VOLS,CARIBE_NS,
		timeout,splitaddr,killgrp,reeflog,dropprefix,rawstr2pairs:import utils;
	Tree: import ctree;
	cspsrv,Cmsg,Cop,cspopen,csprpc:import csp;
	Service,Attributes:import registries;
	Evt:import cevent;
	Rmsg,Tmsg:import styx;
	Cmodule, Replica, OP, SYNC, CORR, DIED, CC_CMD_FREE,CC_CMD_INFO: import creplica;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	writehis,delhis,createhis	: import history;
	Xfs:import xsrv;


include "readdir.m";
	readdir: Readdir;


CC_NAME		: con "reef";
CC_LEVEL		: con 3;


init (d:Dat)
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
}



##	
##	Metadata of reef module
##

Reefinfo:adt	
{
	replica	: ref Replica;
	mod	: ref Cmodule;
	peer	: ref Registries->Service;	# parent
	peerloc	: string;
	gid	: int;				# gid of proccess to control this Reefinfo
	lock	: chan of int;
};






###
##	Creación y arranque de un volumen
###


start (rep:ref Replica):ref Cmodule
{
	mod:=Cmodule.new();
	mod.ccname=CC_NAME;
	mod.ccindex=CC_LEVEL;
	log (reeflog,sprint("Add reef coherency to replica of %s",rep.volume));
	ec:=chan of string;
	spawn reefproc(rep,mod,ec);
	error:=<-ec;
	if (error!=nil)
		return nil;

	return mod;	
}


free (mod: ref Creplica->Cmodule): string
{ 	
	log(stdout,"Destroying reef");
	if (mod.status==DIED)
		return nil; 

	reply:=chan of string;
	mod.ctl<-=(CC_CMD_FREE,reply);
	err:=<-reply;
	log (stdout,sprint("Mato grupo %d de reef",mod.gid));
	killgrp(mod.gid);
	return nil;
}


info (mod: ref Creplica->Cmodule): list of (string,string)
{
	if (mod.status==DIED)
		return nil; 	

	reply:=chan of string;
	mod.ctl<-=(CC_CMD_INFO,reply);
	raw:=<-reply;
	l:=rawstr2pairs(raw);

	return nil;
}



free_module(rt: ref Reefinfo,mod:ref Creplica->Cmodule):string
{
	rt.peer=nil;
	rt.peerloc=nil;
	mod.changeto(rt.replica, DIED);
	
	return nil;
}



reefproc(rep:ref Replica, mod: ref Cmodule, error:chan of string)
{
	gid:=sys->pctl(sys->NEWPGRP,nil);

	err: string=nil;
	fd:=ref sys->FD;

	rt:=ref Reefinfo;
	rt.mod=mod;
	rt.replica=rep;
	rt.gid=gid;
	mod.gid=gid;
	if (rep.root==1)
	{
		rt.peer=nil;		# la réplica es root
		rt.peerloc=nil;
	}
	else
	{
		rt.peer=rep.peer;	# la réplica no es root
		rt.peerloc=rt.peer.attrs.get("loc");
	}

	rt.lock=chan [1] of int;

	spawn srv (rt, mod);
	spawn cspproc(rt,mod.csp);

	error<-=nil;

	for(;;){
		mod.changeto(rep,CORR);		# Initial status is always CORR 
		(err,fd)=initphase(rt,rep,mod);	# Aftter that  the status must be OP
		if (err!=nil)
			break;
		servicephase (rt, fd);		# block until some problem happens
	}

	log(reeflog,sprint("Reef panic and died. [%s]",err));
	free(mod);
}


initphase(rt: ref Reefinfo,rep:ref Replica,mod: ref Cmodule):(string,ref sys->FD)
{
	log(reeflog,"Reef module: Initialization phase  ... ");
	utils->cleanns(utils->CARIBE_NS+"/"+rep.volume+"/reef");

	fd: ref sys->FD;
	e:string;
	(lcc,hcc):=rep.ccindex();

	# Aqui deberia haber un cerrojo pues dos modulos no pueden
	# entrar en la initphase a la vez. Si ambos chekean el rep.ccindex
	# a la vez tendriamos una cond carrera

	#if ((mod.ccindex < lcc) && (rt.peer!=nil))
	#{	
	#	log(reeflog,"Replica coherency level is higher. Syncronizing ...");
	#	mod.changeto(rep,SYNC);	
	#	(e,fd)=syncproc(rt);
	#	if (e!=nil)
	#		return (e,nil);	
	#}
	# log(reeflog,"... Syncronizing done");

	rt.replica.fill();
	mod.changeto(rep,OP);

	return (nil,fd);
}



syncproc(rt: ref Reefinfo):(string,ref sys->FD)
{
	
	##
	## Notificacion por Csp al peer
	##

	if (rt.peer==nil)
		return ("No peer found",nil);
	(p,paddr,nvol):=splitaddr(rt.peer.addr);	#c, peer name, vol name

	mcsp:="tcp!"+paddr+"!"+string (utils->CSP_PORT);
	fd:=cspopen(mcsp);
	if (fd==nil)
		return (sprint("Failed csp comunication to peer %s",mcsp),fd);

	##
	## Solicito sync por Csp
	##

	data:=str->quoted(utils->sysname()::"rain"::rt.replica.volume::string rt.replica.port::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_GET,data));
	if ((rm==nil ) || (rm.cmd!=csp->OK))
		return (sprint("Sincronization rejected by %s",rt.peer.addr),fd);

	log (reeflog, "Conexion al peer ok. Empezamos a checkear ficheros");

	##
	## Obtencion de ficheros
	##

	# No remonto a mi peer. Simplemente cogo los archivos
	# de /peer pues hay tengo ya montado a mi padre

	e:="";

	peermp:=utils->CARIBE_NS+"/"+rt.replica.volume+"/peer";
	me:=rt.replica.rootpath;

	e2:=sync->synctree(rt.replica.volume,peermp, me);
	if (e2!=nil)
		return (sprint("Failed during syncronization to peer %s",rt.peer.addr),fd);

	#e3:=sync->cleantree(si,me);
	#if (e3!=nil)
	#	return ("Failed during cleaning tree",fd);

	(mvers,e4):=versreq(fd, rt.replica.volume);
	if (e4!=nil)
		return ("Failed vers request to peer  during sync",fd);

	rt.replica.vers=mvers;

	return (nil, fd);
}



cspproc(rt:ref Reefinfo, cspchan: chan of ref Cop)
{
	for(;;)
	{
		op:=<-cspchan;
		msg:=op.msg;
		reply:=op.reply;
		case (msg.cmd)
		{
		csp->REPL_GET =>
			reply<-=ref Cmsg(csp->OK,nil);
				
		* =>
			reply<-=ref Cmsg(csp->ERROR,nil);
		}

	}

}



srv(rt:ref Reefinfo, mod:ref Cmodule)
{	
	for (;;)
	{
	alt{
	(qid,msg,source):=<-mod.req =>

		##log (reeflog,sprint( "Recibo un %s", msg.text()));
		mod.reply<-=nil;
		


	(cmd,reply):=<-mod.ctl =>

		case cmd
		{
		CC_CMD_FREE =>	
			err:=free_module(rt,mod);
			if (reply!=nil)
				reply<-=err;

		CC_CMD_INFO =>	
			str:=info_module(rt,mod);
			if (reply!=nil)
				reply<-=str;

		* =>
			log(stdout,"recibido CMD desconocido");
			if (reply!=nil)
				reply<-=nil;
		}
	}
	}
}



###
##	Control del estado de la replica (versiones, disco, etc...)
###

servicephase (rt:ref Reefinfo, fd: ref sys->FD)
{
	for (;;)
	{

		sys->sleep(2000);
	}
	
}


info_module(rt: ref Reefinfo,mod: ref Cmodule):string
{
	s:="name="+CC_NAME+" ";
	s=s+"ccindex=3"+" ";
	s=s+"status="+sprint("%d",mod.status)+" ";

	return s;
}




versreq (fd: ref sys->FD, volume:string):(big,string)
{
	if (fd==nil)
		return (big -1,"No peer fd");
	e:string;
	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_VERS,data));
	if ((rm==nil) || (rm.cmd!=csp->OK))
	{
		e="Failed vers request to peer";
		return (big -1,e);
	}
	v:=big rm.data;
	return (v,nil);
}





diedreq (fd: ref sys->FD, volume:string):string
{
	if (fd==nil)
		return "No peer fd";

	e:string;
	data:=str->quoted(utils->sysname()::volume::nil);
	(err,rm):=csprpc(fd,ref Cmsg(csp->REPL_PEER_DIED,data));

	if ((rm==nil) || (rm.cmd!=csp->OK))
		return "Failed vers request to peer";

	return nil;
}
