implement Dat;
include "mods.m";
	

loadmods(s: Sys)
{
	sys = mods.sys = s;
	mods.env = checkload (load Env Env->PATH, Env->PATH);
	mods.styx= checkload (load Styx Styx->PATH, Styx->PATH);

	#mods.styxs=checkload(load Styxservers Styxservers->PATH, Styxservers->PATH);
	mods.styxs=checkload(load Styxservers "../dis/styxservers.dis", "../dis/styxservers.dis");

	mods.xsrv=checkload(load Xsrv Xsrv->PATH, Xsrv->PATH);
	mods.random=checkload(load Rand Rand->PATH, Rand->PATH);
	mods.math=checkload (load Math Math->PATH, Math->PATH);
	mods.registries=checkload (load Registries Registries->PATH, Registries->PATH);
	# mods.hash=checkload (load Hash Hash->PATH, Hash->PATH);
	mods.str=checkload (load String String->PATH, String->PATH);
	mods.cfg=checkload (load Cfg Cfg->PATH, Cfg->PATH);
	mods.attrdb=checkload (load Attrdb Attrdb->PATH, Attrdb->PATH);
	mods.ccmd=checkload (load Ccmd Ccmd->PATH, Ccmd->PATH);
	mods.cproxy=checkload (load Cproxy Cproxy->PATH, Cproxy->PATH);
	mods.cns=checkload (load Cns Cns->PATH, Cns->PATH);
	mods.utils=checkload (load Utils Utils->PATH, Utils->PATH);
	mods.cloc=checkload (load Cloc Cloc->PATH, Cloc->PATH);
	# mods.cc=checkload(load Cc Cc->PATH, Cc->PATH);
	mods.cloader=checkload(load Cloader Cloader->PATH, Cloader->PATH);
	mods.vdb=checkload(load Vdb Vdb->PATH, Vdb->PATH);	
	mods.ctree=checkload(load Ctree Ctree->PATH, Ctree->PATH);
	mods.cevent=checkload(load Cevent Cevent->PATH, Cevent->PATH);
	mods.cnode=checkload(load Cnode Cnode->PATH, Cnode->PATH);
	mods.csp=checkload(load Csp Csp->PATH, Csp->PATH);
	mods.creplica=checkload(load Creplica Creplica->PATH, Creplica->PATH);
	mods.sync=checkload(load Sync Sync->PATH, Sync->PATH);
	mods.cmeta=checkload(load Cmeta Cmeta->PATH, Cmeta->PATH);
	mods.cdebug=checkload(load Cdebug Cdebug->PATH, Cdebug->PATH);
	mods.auto=checkload(load Auto Auto->PATH, Auto->PATH);
	mods.procs=checkload(load Procs Procs->PATH, Procs->PATH);
	mods.history=checkload(load History History->PATH, History->PATH);
	mods.hashtable=checkload(load HashTable HashTable->PATH, HashTable->PATH);
}

checkload[T](x: T, p: string): T
{
	if(x == nil)
		sys->print("cannot load %s: %r", p);
	return x;
}

