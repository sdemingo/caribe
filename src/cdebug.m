Cdebug:module
{
	PATH	: con "cdebug.dis";

	DIVERGE_DELAY	: con 1;
	CONVERGE_DELAY	: con 2;
	RANDOM_DELAY	: con 3;
	VARY_DELAY	: con 4;

	init 	: fn(d:Dat);
	start	: fn();
	netdelay	: fn(min:int, max:int, fun:int);
	setdelay	: fn(delay:int);

};
