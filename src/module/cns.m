Cns : module
{
	PATH	: con "cns.dis";

	init 	: fn(d:Dat);
	halt 	: fn();
	start	: fn();
	key	: fn (s: list of string):string; 
	register	: fn (key:string, att: list of (string,string)):ref Registries->Service;
	update	: fn (key:string, att: list of (string,string)): string;
	unregister	: fn (key:string):string;
	find	: fn (att: list of (string,string)):list of ref Registries->Service;
	version	: fn ():int;
};

