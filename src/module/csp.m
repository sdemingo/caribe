
##
## Notas:
## Caribe Simple Protocol

Csp : module
{
	PATH	: con "csp.dis";

	BIT8SZ:	con 1;
	BIT16SZ:	con 2;
	BIT32SZ:	con 4;
	BIT64SZ:	con 8;

	MAXFDATA:	con 512;
	CMDSZ	: con BIT16SZ;
	STRSZ	: con BIT16SZ;
	LENRPC	: con BIT16SZ;
	MAXRPC:	con LENRPC+CMDSZ+STRSZ+MAXFDATA;	# usable default for fversion and iounit

	DELIM	: con " ";
	
	# cmds
	REPL_PEER,
	REPL_GET,
	REPL_INIT_NEW,
	REPL_MOUNT,
	REPL_VERS,
	REPL_GET_CC,
	REPL_SET_CC,
	REPL_PEER_DIED,
	CC_HISTORY,
	LOC_BEACON,
	OK,
	ERROR,
	MAX_CSP_CMD: con iota;



	Cmsg : adt
	{
		cmd:int;
		data:string;

		read	: fn(fd: ref Sys->FD):ref Cmsg;
		unpack	: fn(a:array of byte):ref Cmsg;
		pack	: fn(m:self ref Cmsg):array of byte;
		text	: fn(m:self ref Cmsg):string;
	};

	Cop : adt
	{
		msg: ref Cmsg;
		reply: chan of ref Cmsg;

		new	: fn (msg: ref Cmsg,reply: chan of ref Cmsg):ref Cop;
	};

	init 	: fn(d:Dat);
	cspsrv	: fn(port:int):chan of ref Cop;
	cspopen	: fn(addr:string):ref sys->FD;
	csprpc	: fn(fd: ref sys->FD, req: ref Cmsg):(string, ref Cmsg);
};
