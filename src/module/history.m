History: module {
	PATH: con "history.dis";

	HISTORY_DIRNAME	: con "history";
	HISTORY_PATH	: con "/"+HISTORY_DIRNAME;

	# Return values from cmphis
	NO_NEWER,
	LEFT_NEWER,
	RIGHT_NEWER,
	CONFLICT,
	ERROR : con iota;

	init	: fn(d:Dat):string;

	createhis	: fn(root:string, rpath:string, mode: int):ref sys->FD;
	openhis	: fn(root:string, rpath:string):ref sys->FD;
	writehis	: fn(root:string, rpath:string):string;
	cmphis	: fn(root1,root2,rpath:string):(string,int);
	copyhis	: fn(root1,root2,rpath:string):string;
	delhis	: fn(root:string, rpath:string):string;
};
