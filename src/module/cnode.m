Cnode : module
{
	PATH	: con "cnode.dis";

	init 	: fn(d:Dat,flags:ref Utils->Cmdargs):string;
	start	: fn(flags:ref Utils->Cmdargs):string;
	halt	: fn():string;
	replica	: fn(volume:string,dir:string):string;
	free	: fn(volume:string):string;
	all	: fn():list of ref Creplica->Replica;	
	event	: fn(e:ref Cevent->Evt);
};
