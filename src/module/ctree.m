Ctree: module {
	PATH: con "ctree.dis";

	Q_ROOT	: con big 0;


	Tree: adt {
		root	:string;
		c	:chan of ref Treeop;
		reply	:chan of string;
		kreply	:chan of ref Lock;
		up_mutex	:chan of int;

		quit:	fn(t: self ref Tree);
		create:	fn(t: self ref Tree, parent: big, d: Sys->Dir): string;
		wstat:	fn(t: self ref Tree, path: big, d: Sys->Dir): string;
		remove:	fn(t: self ref Tree, path: big): string;
		

		# Change over Nametree of Styxservers
		loadtree: 	fn(t: self ref Tree, path: big): string;
		getpath:	fn(t: self ref Tree, path: big): string;
		getppath:	fn(t: self ref Tree, path: big): string;
		getqid:	fn(t: self ref Tree, file:string): big;
		lock:	fn(t: self ref Tree, path: big,owner:string,level:int):string;	# lock/unlock files
		unlock:	fn(t: self ref Tree, path: big): string;
		openlock:	fn(t: self ref Tree, path:big): ref Lock;		# to work with Lock adt
		closelock:	fn(t: self ref Tree, path:big): string;
		dump:	fn(t: self ref Tree):string;			# only for debug
	};


	Lock: adt{
		exec : chan of int;
		owner: string;
		level:int;
	};


	Treeop: adt {
		q: big;
		pick {
		Create or
		Wstat =>
			d: Sys->Dir;
			reply: chan of string;
		Remove =>
			reply: chan of string;
		Getpath =>
			reply: chan of string;
		Getppath =>
			reply: chan of string;
		Load =>
			rootdir:string;
			reply: chan of string;			
		Lock =>
			owner:string;
			level:int;
			reply: chan of string;
		Openlock=>
			kreply:chan of ref Lock;
		Closelock =>
			reply:chan of string;
		Unlock =>
			reply:chan of string;
		Dump =>
			reply:chan of string;
		}
	};


	Navop: adt {
		reply:	chan of (ref Sys->Dir, string);	# channel for reply
		path:		big;		# file or directory path
		pick {
		Stat =>
		Walk =>
			name: string;
		Readdir =>
			offset:	int;	# index (origin 0) of first directory entry to return
			count: 	int;	# number of directory entries requested
		}
	};


	init:		fn(d:Dat);
	start:		fn(rootpath:string): (ref Tree, chan of ref Navop);
};
