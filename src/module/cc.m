Cc : module
{
	init 	: fn(d:Dat);
	start	: fn(rep:ref Creplica->Replica): ref Creplica->Cmodule;
	free	: fn(mod: ref Creplica->Cmodule): string;
	info	: fn(mod: ref Creplica->Cmodule): list of (string,string);
};
