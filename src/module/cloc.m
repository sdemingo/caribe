Cloc : module
{
	PATH	: con "cloc.dis";
	NAME	: con "cloc";
	MAX_LOC_BYTES	: con 128;

	CLOSENESS	: con 150;		# millisg
	UNKNOW 		: con 10000000;
	MEDIUM_ISLAND_SIZE	: con 3;	# nodes
	LARGE_ISLAND_SIZE		: con 7;	# nodes
	LOC_MAX_LEN		: con 128;	

	init 	: fn(d:Dat, flags:ref Utils->Cmdargs):string;
	start	: fn(flags:ref Utils->Cmdargs):string;
	halt 	: fn();
	where	: fn(addr:string):string;
	distance	: fn(island:string):int;
	sort	: fn(area:list of ref Registries->Service):list of ref Registries->Service;
	map	: fn():list of (string,int);
	print	: fn():string;
	event	: fn(e:ref Cevent->Evt);
};
