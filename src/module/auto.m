Auto:module
{
	PATH	: con "auto.dis";

	# 
	# Autonomous desicion-making
	# 

	init 	: fn(d:Dat);

	quota 	: fn (rep: ref Creplica->Replica);
	replication	: fn (rep: ref Creplica->Replica);
	newpeer	: fn (rep: ref Creplica->Replica):string;
	newcopy	: fn (rep: ref Creplica->Replica):string;
};
