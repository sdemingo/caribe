Sync : module
{
	PATH	: con "sync.dis";

	init 	: fn(d:Dat):string;
	synctree	: fn(volume:string, src:string, dest:string): string;
	conflictfile	: fn(volume:string, file:string):string;
	checksum	: fn(file:string):string;
};
