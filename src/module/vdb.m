Vdb : module
{
	PATH	: con "vdb.dis";

	BUF_SZ	: con 1024;
	SRV_SZ	: con 10;


	Ventry: adt{

		volume:string;
		dir:string;
		user:string;

		text : fn(e:self ref Ventry):string;
	};

	init 	: fn(d:Dat);
	add	: fn(ndb:string, attrs: list of (string,string)):string;
	get	: fn(ndb:string): list of ref Ventry;
};


