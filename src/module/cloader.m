Cloader : module
{
	PATH	: con "cloader.dis";

	MODS_PATH	: con "./cc";

	init 	: fn(d:Dat):string;
	getmaxcc	: fn():int;
	getcc	: fn(cc:string):(Cc,int);
	getccname	: fn(cc:int):string;
	getallcc	: fn():list of Cc;
	
};
