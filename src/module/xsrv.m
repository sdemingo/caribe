Xsrv:module
{
	PATH	: con "xsrv.dis";

	xlock: chan of int;

	# 
	# Esta libreria encapsula la manipulación de un componente Styxserver junto
	# con un arbol CTree que no es más que una copia apenas modificada del
	# documentado en Styxserver->namespace.
	# 


	Xop: adt {
		s:ref Styxservers->Styxserver;
		reply: chan of string;

		pick {
		Create =>
			m: ref Styx->Tmsg.Create;
		Wstat =>
			m: ref Styx->Tmsg.Wstat;
		Write =>
			m: ref Styx->Tmsg.Write;
		Read =>
			m: ref Styx->Tmsg.Read;
		Remove =>
			m: ref Styx->Tmsg.Remove;
		Stat =>
			m: ref Styx->Tmsg.Stat;
		Clunk =>
			m: ref Styx->Tmsg.Clunk;
		Walk =>
			m: ref Styx->Tmsg.Walk;
		Open =>
			m: ref Styx->Tmsg.Open;
		Attach =>
			m: ref Styx->Tmsg.Attach;
		Upload =>
		}

		text	: fn(xm: self ref Xop):string;
	};



	Xfs	: adt
	{
		id	: string;
		tree	: ref Ctree->Tree;
		c	: chan of ref Xop;
		reply	: chan of string;
		
		new	: fn (id:string, tree:ref Ctree->Tree): ref Xfs;
		xwrite	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Write):string;
		xread	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver,m: ref Styx->Tmsg.Read):string;
		xcreate	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Create):string;
		xremove	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Remove):string;
		xwstat	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Wstat):string;
		xstat	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Stat):string;
		xclunk	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Clunk):string;
		xwalk	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Walk):string;	
		xopen	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Open):string;	
		xattach	: fn (x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Attach):string;
		xupload	: fn (x: self ref Xfs):string;
		
	};

	init 	: fn(d:Dat);

	xreply	: fn (s:ref Styxservers->Styxserver,m: ref Styx->Rmsg);

};
