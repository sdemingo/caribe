include "sys.m";
	sys: Sys;
	
include "draw.m";

include "dat.m";
	dat : Dat;

include "styx.m";
	styx: Styx;

include "styxservers.m";
	styxs:Styxservers;

include "xsrv.m";
	xsrv: Xsrv;

include "env.m";
	env : Env;

include "rand.m";
	rnd:Rand;

include "math.m";
	math: Math;

include "registries.m";
	registries: Registries;

#include "hash.m";
#	hash: Hash;

include "hashtable.m";
	hashtable: HashTable;

include "string.m";
	str: String;

include "cfg.m";
	cfg : Cfg;

include "bufio.m";
include "attrdb.m";
	attrdb: Attrdb;

include "ccmd.m";
	ccmd:Ccmd;

include "ctree.m";
	ctree: Ctree;

include "cproxy.m";
	cproxy: Cproxy;

include "cns.m";
	cns: Cns;

include "cc.m";
#	cc: Cc;

include "cloader.m";
	cloader : Cloader;

include "cloc.m";
	cloc: Cloc;

include "creplica.m";
	creplica: Creplica;

include "vdb.m";
	vdb: Vdb;

include "cevent.m";
	cevent: Cevent;

include "cnode.m";
	cnode: Cnode;

include "csp.m";
	csp: Csp;

include "cmeta.m";
	cmeta: Cmeta;

include "sync.m";
	sync : Sync;

include "utils.m";
	utils: Utils;

include "cdebug.m";
	cdebug:Cdebug;

include "auto.m";
	auto:Auto;

include "procs.m";
	procs: Procs;

include "history.m";
	history: History;



	
initmods(m: Dat->Mods)
{
	sys = m.sys;
	env=m.env;
	styx=m.styx;
	styxs=m.styxs;
	xsrv=m.xsrv;
	rnd=m.random;
	math=m.math;
	registries=m.registries;
	#hash=m.hash;
	hashtable=m.hashtable;
	str=m.str;
	cfg=m.cfg;
	attrdb=m.attrdb;
	ccmd=m.ccmd;
	cproxy=m.cproxy;
	cns=m.cns;
	cloader=m.cloader;
	cloc=m.cloc;
	vdb=m.vdb;
	cevent=m.cevent;
	ctree=m.ctree;
	cnode=m.cnode;
	csp=m.csp;
	creplica=m.creplica;
	utils=m.utils;
	sync=m.sync;
	cmeta=m.cmeta;
	cdebug=m.cdebug;
	auto=m.auto;
	procs=m.procs;
	history=m.history;
	##cc=nil	it must be loaded by ccloader
}
