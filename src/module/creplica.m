Creplica : module
{
	PATH	: con "creplica.dis";


	Cmodule : adt
	{
		req 	: chan of (big, ref Styx->Tmsg,string);	#qid, message, source
		reply 	: chan of string;
		csp	: chan of ref Csp->Cop;
		ctl	: chan of (string, chan of string);
		status	: int;
		ccname 	: string;
		ccindex	: int;
		gid	: int;

		new	: fn(): ref Cmodule;
		changeto	: fn(mod: self ref Cmodule, rep:ref Replica, status:int);
	};


	# replica status
	INIT,
	CORR,
	SYNC,
	OP	: con iota;
	DIED	: con -1;

	MAX_CCINDEX	: con 10;
	DEFAULT_CCINDEX	: con 2;
	CCNULL		: con "nullcc";

	SBUF_MAX_SIZE	: con 10*1024;
	SBUF_POOL_TIME	: con 100;

	REPL_SERVICE_PH_TIME	: con 3000;


	# Cc controls commands
	CC_CMD_FREE	: con "free";
	CC_CMD_INFO: con "info";


	#
	# The client's control structure
	# 

	Xcon: adt
	{
		fd:	ref sys->FD;
		srv: 	ref Styxservers->Styxserver;
		addr:	string;
		port:	string;
		buf:	ref Sbuf;
		tc:	chan of ref Styx->Tmsg;
		rep:	ref Creplica->Replica;
		ctl_c:	chan of int;
		gid:	int;
		
		new	:fn(rep:ref Creplica->Replica, c : sys->Connection ):ref Xcon;
	};


	Sbuf : adt
	{
		source	: string;	# este atributo sobra
		data	: array of ref Styx->Tmsg;
		head	: int;
		tail	: int;
		in	: chan of ref Styx->Tmsg;
		out	: chan of ref Styx->Tmsg;
		gid	: int;
		lock	: chan of int;
		end	: int;

		new	: fn(source:string): ref Sbuf;
		start	: fn(sb:self ref Sbuf, rep: ref Replica);
	};



	#
	# The replica's control structure
	# 

	Replica : adt
	{
		volume	: string;
		rootpath	: string;
		port	: int;
		loc	: string;

		nav_c	: chan of ref Styxservers->Navop;
		xfs	: ref Xsrv->Xfs;
		xcon_procs  : ref Procs->Ptab;

		vers	: big;
		lkmods	: list of ref Cmodule;
		gid	: int;		# gid of proccess to control this Replica
		meta	: ref Cmeta->Meta;
		peer	: ref Registries->Service;
		root	: int;		# es nodo raiz
		starting	: int;		# esta iniciandose
		freeing	: int;		# esta muriendose
		
		new	: fn(volume:string,dir:string,id:int):ref Replica;
		fill	: fn(rep:self ref Replica):string;
		register	: fn(rep:self ref Replica):string;
		status	: fn(rep:self ref Replica):int;
		size	: fn(rep:self ref Replica):big;
		ccindex	: fn (rep:self ref Replica):(int,int);	# lowest cc, highest cc
		link	: fn(rep:self ref Replica, m : ref Cmodule);
		unlink	: fn(rep:self ref Replica, name: string): string;
		instance	: fn(rep:self ref Replica, name: string): string;
		free	: fn(rep:self ref Replica):string;
		text	: fn(rep:self ref Replica):string;
	};
	
	init 	: fn(d:Dat);
	mount	: fn(addr:string, mp:string):string;
	unmount  	: fn(mp:string):string;
};
