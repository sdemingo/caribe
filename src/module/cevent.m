Cevent : module
{
	PATH	: con "cevent.dis";

	#
	# Events codes from
	#

	LOC_CHANGED,		# cloc
	REPL_QUOTA_OVRFLW,		# cc
	REPL_LOW_REPLCTN,
	REPL_RESTART,
	REPL_STATUS_CHANGED,
	REPL_TREE_UPDATED,
	REPL_TREE_DUMP,		# Solo para depurar
	REPL_RENEGOTIATE,
	REPL_ABORT,
	CNS_OFFLINE,		# cns
	PANIC_ERROR,		# general
	MAX_EVT: con iota;


	# Events sources
	QCLOC,
	QCNS,
	QCC,
	MAXQ	: con iota;



	Evt : adt
	{
		cod:int;
		stamp:int;
		args:array of string;

		new	: fn (cod:int,args:list of string):ref Evt;
		argv	: fn (e:self ref Evt):array of string;
		clone	: fn (e:self ref Evt):ref Evt;
		text	: fn (e:self ref Evt):string;
	};

	init 	: fn(d:Dat):string;
	#event	: fn(e:ref Evt, source:int);
	#listen	: fn(source:int):chan of ref Evt;
};
