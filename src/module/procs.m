Procs : module
{
	PATH	: con "procs.dis";

	NPROCS	: con 200;

	Ptab:adt
	{
		tab : array of int;
		n: int;

		new	: fn():ref Ptab;
		add	: fn(tab:self ref Ptab, pid:int);
		del	: fn(tab:self ref Ptab, pid:int);
		get	: fn(tab:self ref Ptab):array of int;
		text	: fn (p:self ref Ptab):string;
	};

	init 	: fn(d:Dat):string;

};
