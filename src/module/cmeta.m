Cmeta : module
{
	PATH	: con "cmeta.dis";

	META_FILE	: con "META";

	META_DEFAULT_CCMAX:		con 2;
	META_DEFAULT_MINREP:		con 1;
	META_DEFAULT_MAXREP:	con 3;
	META_DEFAULT_ISLANDS:	con 2;
	META_DEFAULT_QUOTA :	con 1024;	#Kilobytes;

	MParser:adt
	{
		cfg:Cfg;
		file:string;
	
		new	: fn(file:string):ref MParser;
		value	:fn (p: self ref MParser, attr:string):string;
	};


	Meta: adt
	{
		volume	:string;
		parser	:ref MParser;
		ccmax 	:int;
		minrep	:int;
		maxrep	:int;
		islands	:int;
		quota	:big;

		new	: fn(volume:string):ref Meta;
		read	: fn(volume:string):ref Meta;
		write	: fn(m:self ref Meta):string;
		text	: fn(m:self ref Meta):string;
	};


	init 	: fn(d:Dat):string;
};
