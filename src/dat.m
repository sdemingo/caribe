Dat: module {
	PATH: con "dat.dis";

	Mods: adt {
		sys	: Sys;
		styx	: Styx;
		styxs	: Styxservers;
		xsrv	: Xsrv;
		env	: Env;
		random	: Rand;
		math 	: Math;
		registries	: Registries;
		#hash	: Hash;
		str	: String;
		cfg	: Cfg;
		attrdb	: Attrdb;
		ccmd	: Ccmd;
		cproxy	: Cproxy;
		cns	: Cns;
		#cc	: Cc;
		cloader	: Cloader;
		cloc	: Cloc;
		vdb	: Vdb;
		cevent	: Cevent;
		cnode	: Cnode;
		ctree	: Ctree;
		csp	: Csp;
		creplica	: Creplica;
		sync	: Sync;
		cmeta	: Cmeta;
		utils	: Utils;
		cdebug	: Cdebug;
		auto	: Auto;
		procs	: Procs;
		history	: History;
		hashtable	: HashTable;
	};

	loadmods		: fn(s: Sys);
	checkload		: fn[T](x: T, p: string): T;
	mods		: Mods;
};
