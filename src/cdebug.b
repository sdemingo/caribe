implement Cdebug;

include "mods.m";
	Connection,sprint,open,read,write,stat,create, FD :import sys;
	stderr,stdout,log,creplicalog,sysname,panic,CSP_PORT,timeout,splitaddr,
	killgrp,dropprefix,size,xlog:import utils;
	rand : import rnd;
	sin	: import math;


last_delay:int;
extern_delay:int;
diverge:int;

init (d:Dat)
{
	initmods(d->mods);
	start();
}

start()
{
	last_delay=-1;
	diverge=1;
}

setdelay(delay:int)
{
	extern_delay=delay;
}


netdelay(min:int, max:int, fun:int)
{
	if ((min <0) || (max<0) || (min>=max))
		return;

	delay:=0;
	case (fun)
	{
	CONVERGE_DELAY =>
		if (last_delay<0)  #first delay
			last_delay=max;

		delay=int(0.8*(real last_delay));

		if (delay<min)
			delay=min;

	DIVERGE_DELAY =>
		if (last_delay<0)  #first delay
			last_delay=min;
		delay=int(1.5*(real last_delay));

		if (delay>max)
			delay=max;
	

	RANDOM_DELAY =>
		dif:=max-min;
		delay=rand(dif)+min;

	VARY_DELAY =>
		delay=extern_delay;
		if (delay<0)
			delay=0;	

	* =>
		delay=0;
	}

	last_delay=delay;
	#log (stdout,sprint("Net delay introduced: %d",delay));
	c:=timeout(delay);
	<-c;
}