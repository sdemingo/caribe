implement Creplica;

include "mods.m";
	Connection,sprint,open,read,write,stat,create, FD,NEWPGRP :import sys;
	stderr,stdout,log,creplicalog,sysname,panic,CSP_PORT,timeout,splitaddr,
				killgrp,dropprefix,size,
				isignoredpath:import utils;
	Service,Attributes:import registries;
	Tree: import ctree;
	Evt:import cevent;
	Tmsg, Rmsg :import styx;
	Cop: import Csp;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	Xfs,xreply:import xsrv;
	getenv:import env;
	rand : import rnd;
	getmaxcc: import cloader;
	Meta : import cmeta;
	Mvol	: import cproxy;
	Ptab	: import procs;
	createhis	: import history;



init (d:Dat)
{
	initmods(d->mods);
}



	#
	#	Xcon and Sbuf
	#


Xcon.new(rep:ref Creplica->Replica, c : sys->Connection ):ref Xcon
{
	x:=ref Xcon;

	x.ctl_c=chan of int;
	x.gid=sys->pctl(NEWPGRP,nil);
	x.rep=rep;

	rep.xcon_procs.add(x.gid);

	navigator:=Navigator.new(rep.nav_c);
	x.fd=sys->open(c.dir + "/data", Sys->ORDWR);
	(x.tc,x.srv)= Styxserver.new(x.fd,navigator,big 0);
	(x.addr,x.port)=utils->getclient(c);

	x.buf=Sbuf.new(x.addr);
	x.buf.start(x.rep);

	spawn recvproc(x);
	spawn dispacher(x);	

	return x;
}



Sbuf.new(source:string):ref Sbuf
{
	sb:=ref Sbuf;

	sb.data=array [SBUF_MAX_SIZE] of ref Tmsg;
	sb.head=0;
	sb.tail=0;
	sb.in=chan of ref Styx->Tmsg;
	sb.out=chan of ref Styx->Tmsg;
	sb.lock=chan [1] of int;
	sb.source=source;
	sb.end=0;

	return sb;
}

realloc (sb: ref Sbuf):int
{
	ndata:=array [(len sb.data)*2] of ref Tmsg;
	for (i:=0;i<len sb.data;i++)
		ndata[i]=sb.data[i];	

	sb.data=ndata;		
	return i;	
}


Sbuf.start(sb:self ref Sbuf, rep: ref Replica)
{
	spawn sbinput(sb,rep);
	spawn sboutput(sb, rep);
}



sbinput (sb: ref Sbuf, rep: ref Replica)
{
	for (;;)
	{
		tmsg:=<-sb.in;
		if (tmsg==nil)		# Debo morirme
		{	
			sb.end=1;
			alt
			{
			sb.lock<-=1=>
				# Despierto a sboutput si esta dormido
			}
			break;
		}

		sb.data[sb.tail]=tmsg;
		sb.tail++;
		if (sb.tail == SBUF_MAX_SIZE)	
			sb.tail=0;

		if (sb.head==sb.tail)	# cola totalmente llena. Recolocamos
		{
			n:=realloc (sb);
			sb.tail=n;
		}

		alt
		{
		sb.lock<-=1=>
			# Despierto a sboutput si esta dormido
		}
	}
	## log(creplicalog,"Sbufin terminado");
}



sboutput (sb: ref Sbuf, rep: ref Replica)
{
	peer:="";
	if (rep.peer!=nil)
	{
		(nil,pname,nil):=splitaddr(rep.peer.addr);	#c, peer name, vol name
		peer=utils->resolvname(pname);
	}


	for (;;)
	{	
		if (sb.tail != sb.head)
		{
			if ((rep.status()==OP) 
				|| (rep.status()==INIT)) # Salen todos los mensajes
			{ 
				sb.out<-=sb.data[sb.head];
				sb.head++;
				if (sb.head == SBUF_MAX_SIZE)
					sb.head=0;
			}

			if ( (rep.status()==SYNC) 
				&& (sb.source==peer)) # Salen solo los del peer		
			{	
				sb.out<-=sb.data[sb.head];
				sb.head++;
				if (sb.head == SBUF_MAX_SIZE)
					sb.head=0;
			}
		}
		else
		{
			# buffer vacio. Compruebo si la xcon esta acabada y si no
			# me bloqueo hasta que sbinput me avise de que hay nuevos
			# mensajes

			if (sb.end)
				return; # me muero
			
			<-sb.lock;	# espero a que sbinput me avise
		}
	}

	##log(creplicalog,"Sbufout terminado");
}









	#
	#	Cmodule
	#


Cmodule.new(): ref Cmodule
{
	b: Cmodule;
	b.req=chan of (big, ref Tmsg,string);
	b.reply=chan of string;
	b.csp=chan of ref Cop;
	b.ctl=chan of (string, chan of string);	#ctl_cmd and reply chan
	b.status=CORR;
	return ref b;
}



Cmodule.changeto(mod: self ref Cmodule, rep:ref Replica,status:int)
{
	mod.status=status;
	e:=Evt.new(cevent->REPL_STATUS_CHANGED,rep.volume::mod.ccname::string status::nil);
	cnode->event(e);
	cproxy->event(e);
}







	#
	#	Replica
	#


Replica.new (volume:string,dir:string,id:int):ref Replica
{
	r:= ref Replica;
	r.volume=volume;	

	r.gid=-1;

	r.xcon_procs=Ptab.new();
	r.rootpath=dir;
	sd:=sys->open(dir, sys->OREAD);
	if (sd==nil)
	{
		if (sys->create(dir, sys->OREAD , sys->DMDIR | 8r777)==nil)
		{
			log(creplicalog,"Cannot create root dir for replica");
			return nil;
		}
	}
	r.root=0;
	r.lkmods = nil;
	r.vers=big 0;
	if (createhis(dir,nil,sys->DMDIR | 8r777)==nil)
	{
		log(creplicalog,"Cannot create history dir for replica");
			return nil;
	}
	
	r.meta=Meta.new(volume);	# Creo un META por defecto
	
	if (r.rootpath[len r.rootpath-1]=='/')
		r.rootpath[len r.rootpath-1]='\0';

	c := chan of int;
	log (creplicalog,sprint ("Starting new replica for %s",r.volume));

	r.starting=1;
	r.freeing=0;
	r.port=utils->CREPLICA_PORT+id;
	r.loc=cloc->where(utils->sysname());

	err:=r.register();		# Register replica with INIT status
	if (err!=nil)
	{
		log(creplicalog,sprint("Replica not register: %s",err));
		return nil;
	}


	ec:=chan of string;
	spawn initcreplica(r,ec);
	err=<-ec;

	if (err!=nil)
	{
		log(creplicalog,sprint("Replica not run %s",err));
		return nil;
	}
	else
		return r;

}


Replica.fill(rep:self ref Replica):string
{
	if (rep.xfs!=nil)
		return rep.xfs.xupload();
	return "xfs not init";
}




Replica.link(rep:self ref Replica, mod: ref Cmodule)
{
	rep.lkmods=mod::rep.lkmods;	
}


Replica.unlink(rep:self ref Replica, name: string): string
{
	a:=rep.lkmods;
	b:list of ref Cmodule =nil;
	if (a==nil)
		return nil;

	for (m:=hd a; a!=nil; a=tl a)
		if (m.ccname!=name)	
			b=m::b;

	rep.lkmods=b;
	return nil;
}


Replica.instance(rep:self ref Replica, name: string): string
{	
	if ((name==nil) || (name==CCNULL))
		return nil; 	# no cargamos nada, pero no hay error.

	mods:=rep.lkmods;
	if (mods!=nil)
	{
		for (m:=hd mods;mods!=nil;mods=tl mods)
			if (m.ccname==name)
				return nil;  # modulo ya instanciado
	}

	if ( (rep.status()!=OP) &&  (rep.status()!=INIT))
		return "Replica not operative";

	
	(cc,ind):=cloader->getcc(name);
	if (cc==nil)
		return "Bad cc module name";

	mod:=cc->start(rep);
	if (mod==nil)
		return "Module bad instanced";

	rep.link(mod);

	return nil;
}


Replica.size(rep:self ref Replica):big
{
	return size(rep.rootpath);
}


Replica.status(rep:self ref Replica): int
{
	if (rep.starting==1)	# replica iniciandose
		return INIT;

	if (rep.freeing==1)
		return DIED;	# replica muriendose

	lm:=rep.lkmods;
	stat:=OP;
	if (lm!=nil)
	{
		# El estado de la replica es el menor de los
		# estados de los modulos (ignorando estado -1 o DIED)

		for (mod:=hd lm; lm!=nil; lm=tl lm)
			if ((mod.status>=0) && (stat > mod.status))
				stat=mod.status;
	}
	
	return stat;
}



Replica.ccindex (rep:self ref Replica):(int,int)
{
	lw:=MAX_CCINDEX;	#lower cc index
	hg:=0;			#higher cc index

	lm:=rep.lkmods;
	if (lm==nil)
		return (lw,lw);

	for (mod:=hd lm; lm!=nil; lm=tl lm)
		if ( (mod.status == OP) && ( mod.ccindex < lw) )
			lw=mod.ccindex;

	lm=rep.lkmods;
	if (lm==nil)
		return (lw,lw);

	for (mod=hd lm; lm!=nil; lm=tl lm)
		if ( (mod.status == OP) && ( mod.ccindex > hg) )
			hg=mod.ccindex;
	return (lw,hg);
}


Replica.register(rep:self ref Replica):string
{
	a:=("sys",utils->sysname())::nil;
	a=("status",string rep.status())::a;
	a=("loc",rep.loc)::a;
	a=("port",string rep.port)::a;
	a=("vol",rep.volume)::a;
	k:=cns->key("c"::utils->sysname()::rep.volume::nil);

	sv: ref Service;
	while (1)
	{
		sv=cns->register(k,a);
		if (sv!=nil)
			break;

		log (creplicalog,"Waiting to register replica ...");
		sys->sleep(5000);
	}

	return nil;
}


Replica.free(rep:self ref Replica):string
{
	# envio el mandato de muerte a los cc para que avisen
	# a sus posibles hijos

	rep.freeing=1;

	log (creplicalog,"Destroying replica ...");

	lm:=rep.lkmods;
	if (lm!=nil)
	{
		##for (mod:=hd lm; lm!=nil; lm=tl lm)
		while (lm!=nil){
			mod:=hd lm;
			(cc,ind):=cloader->getcc(mod.ccname);
			if (cc!=nil)	
				cc->free(mod);
			lm=tl lm;
		}
	}

	rep.lkmods=nil;

	log (creplicalog,"... Coherency modules off");

	c:=rep.xcon_procs.get();
	for (i:=0; i<len(c); i++)
	{
		if (killgrp(c[i])!=nil)
			log (creplicalog,"Some Replica clients conexions are zombies");

	}

	log (creplicalog,"... Connections off");

	k:=cns->key("c"::utils->sysname()::rep.volume::nil);
	sv:=cns->unregister(k);
	if (sv==nil)
		return "Replica unregister failed";

	log(creplicalog,"Replica totally destroyed");

	return nil;
}


Replica.text(rep:self ref Replica):string
{
	s:="";
	if (rep==nil)
		return s;
	s=s+"volume="+rep.volume+"\n";
	s=s+"\t rootpath="+rep.rootpath+"\n";	
	s=s+"\t port="+string rep.port+"\n";
	if (rep.peer!=nil)
		s=s+"\t peer="+rep.peer.addr+"\n";
	else
		s=s+"\t peer=not found yet. Maybe root\n";


	#############
	# Dump del arbol entero (solo para depurar)
	#s=s+"\n\n Dump del arbol\n";
	#s=s+rep.xfs.tree.dump();
	#############

	s=s+"\t vers: "+string rep.vers+"\n";

	lm:=rep.lkmods;
	if (lm==nil)
		return s;

	while (lm != nil)
	{
		mod:=hd lm;
		(cc,ind):=cloader->getcc(mod.ccname);
		if (cc!=nil)
		{
			s=s+"\t +"+mod.ccname+"\n";
			im:=cc->info(mod);

			while(im!=nil)
			{
				(att,val):=hd im;
				s=s+"\t  "+att+"="+val+"\n";
				im=tl im;
			}
		}	
		lm=tl lm;
	}

		
	return s;
}








	#
	#	Creplica service
	#



initcreplica(rep:ref Replica,error:chan of string)
{
	e:string;

	rep.gid=sys->pctl(sys->NEWPGRP ,nil);
	log (creplicalog, sprint("Init replica of %s (gid=%d) ", rep.volume,rep.gid));
	(n,d):=sys->stat(rep.rootpath);
	if (n<0)
	{
		error<-=sprint ("Directory %s not found",rep.rootpath);
		return;
	}

	# Arranco el Xfs
	(tree,navc):=ctree->start(rep.rootpath);
	rep.nav_c=navc;
	rep.xfs=Xfs.new("creplica",tree);

	# Guardo el volumen exportado en el ndb	
	at:=("volume",rep.volume)::("dir",rep.rootpath)::("user","caribe")::nil;
	en:=vdb->add(utils->CARIBE_FS,at);
	if (en!=nil)
		log (creplicalog,"Volume entry already exists in ndb");


	rep.fill();


	# Comienzo a exportar
	ce:=chan of string;
	spawn creplicaproc(rep,ce);
	e=<-ce;
	spawn creplicaservice(rep);

	if (e!=nil)
	{
		log (creplicalog,sprint("Start failed. Replica not initialized:%s",e));
		error<-=sprint("Start failed. Replica not initialized:%s",e);
	}
	else
	{
		error<-=nil;
	}

}




creplicaproc (rep: ref Replica,error: chan of string)
{	
	(e,conn) := sys->announce("tcp!*!"+string rep.port);
	if (e<0)
	{
		error<-="Creplica: announced failed\n";
		exit;	
	}
	error<-=nil;

	log (creplicalog,sprint("Replica %s serving styx ...",rep.volume));

	while ()
	{
		(er,c) := sys->listen(conn);
		if (er<0)
		{
			log (creplicalog,"Creplica: fallo el listen");
			exit;	
		}		
		spawn clientproc(rep,c);
	}
	rep.xfs.tree.quit();
}



creplicaservice(rep:ref Replica)
{
	log (creplicalog, sprint("Replica %s scanning enviroment ...",rep.volume));
	err:string=nil;

	for(;;)
	{
		v:=cns->version();

		sys->sleep(REPL_SERVICE_PH_TIME);

		if (rep.meta==nil)
			continue;

		nv:=cns->version();

		#if (nv != v)
		#{	
			# New events in the registry
			# newpeer(rep);
			# Si encuentro nuevo peer deberia invocar un evento de renegociacion
			# lo cual es raro, invocar una REPL_RENEGOTIATE haria repertir
			# el procesode negociacion y el propio metodo newpeer
		#}

		#if (checkpeer(rep)!=nil)
		#	event(Evt.new(cevent->REPL_RESTART,rep.volume::rep.rootpath::nil));
		# quota(rep);		# Control de quota (en kilobytes)
		# replication(rep);	# Control de replicas minimas y maximas
	}

}




clientproc(rep:ref Replica, conn : Connection)
{
	xcon:=Xcon.new(rep,conn);	
	<-xcon.ctl_c;

	log (creplicalog,sprint("End connection from client %s",xcon.addr));

	xcon.buf.in<-=nil;	# mato sbinput

	rep.xcon_procs.del(xcon.gid);	# mato todas las conexiones de los clientes
	if (killgrp(xcon.gid)!=nil)
		log(creplicalog,"A Xcon is zombie");
	
}


recvproc(x: ref Xcon)
{
	while((tmsg:=<-x.tc)!=nil)
		x.buf.in<-=tmsg;
}



dispacher(x: ref Xcon)
{
	end:=0;

	for(;;){
		if (end)
			break;

		gm:=<-x.buf.out;

		#log (creplicalog,sprint("<- %s",gm.text()));

		err:string = nil;
		ccerr:string = nil;

		pick m := gm {	
		Read =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
				err=x.rep.xfs.xread(x.srv,m);



		Write =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
			{
				err=x.rep.xfs.xwrite(x.srv,m);
				if (err==nil)
					incvers(x,gm,m.fid);
					#x.rep.vers++;
			}


		Create =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
			{
				err=x.rep.xfs.xcreate(x.srv,m);
				if (err==nil)
					incvers(x,gm,m.fid);
					#x.rep.vers++;
			}


		Remove =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
			{
				err=x.rep.xfs.xremove(x.srv,m);
				if (err==nil)
					incvers(x,gm,m.fid);
					#x.rep.vers++;
			}

		Wstat =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
			{
				err=x.rep.xfs.xwstat(x.srv,m);
				if (err==nil)
					incvers(x,gm,m.fid);
					#x.rep.vers++;
			}


	
		Clunk =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
			{
				err=x.rep.xfs.xclunk(x.srv,m);
				if ((len x.srv.allfids())==0)
					end=1;
			}


		Stat =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
				x.rep.xfs.xstat(x.srv,m);


	
		Walk =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
				x.rep.xfs.xwalk(x.srv,m);

		Open =>
			ccerr=sendmods(x,gm,m.fid);
			if (ccerr!=nil)
				xreply(x.srv,ref Rmsg.Error(m.tag, ccerr));
			else
				x.rep.xfs.xopen(x.srv,m);


		Attach =>
			x.rep.xfs.xattach(x.srv,m);


		* =>
			x.srv.default(m);
		}
	}

	x.ctl_c<-=end;
}



sendmods (x:ref Xcon, m: ref Tmsg, fid:int):string
{
	if (x.rep.status()==SYNC)
	{
		return nil;
	}

	c := x.srv.getfid(fid);
	if (c == nil)
		return sprint("%s. Propagating to coherency modules",Ebadfid);


	#
	# Ignore propagation below the ignored paths
	#

	file:=x.rep.xfs.tree.getpath(c.path);
	if (isignoredpath(utils->ignored_paths,file)){
		return nil;
	}
	
	
	a:=x.rep.lkmods;
	repl:string;
	while (a!=nil)
	{
		b:=hd a;
		b.req<-=(c.path,m,x.addr);	# aviso a los cc de que les voy a mandar mensajes
		repl=<-b.reply;			# recibo la respuesta de los modulos.
		if (repl!=nil)			# Si es error no propago mas y lo devuelvo.
			return repl;
		a=tl a;
	}
	return nil;
}



# 
# Increment version if the message not perform on 
# ignored paths
#

incvers(x:ref Xcon, m: ref Tmsg, fid:int)
{
	c := x.srv.getfid(fid);
	if (c == nil)
		return;

	#
	# Ignore propagation below the ignored paths
	#

	file:=x.rep.xfs.tree.getpath(c.path);
	if (isignoredpath(utils->ignored_paths,file)){
		return;	
	}
	
	x.rep.vers++;
	#log(stdout,sprint("vers: %d",int x.rep.vers));
}


mount (addr:string, mp:string):string
{
	if ((addr==nil) || (mp==nil))
		return "Creplica: mount has failed. Addr or mp are nil";

	es:=utils->mkns(mp);
	if (es!=nil)
		return es;
	
	(e,c):=sys->dial(addr,nil);
	if (e<0)
		return sprint("Creplica: dial failed to %s",addr);

	if (sys->mount(c.dfd,nil,mp,Sys->MBEFORE | Sys->MCREATE,nil)<0)
		return sprint("Creplica: mounted failed to %s on %s\n",addr,mp);

	return nil;
}


unmount (mp:string):string
{
	if (sys->unmount(nil, mp)<0)
		return "Unmount failed";
	return nil;
}






