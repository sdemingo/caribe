implement Procs;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,fprint,sprint:import sys;
	stderr,stdout,log,CSP_PORT,CARIBE_VOLS,sysname,panic,cloclog,
		writeline,splitaddr:import utils;
	Service,Attributes:import registries;
	find : import cns;
	rand : import rnd;
	cspopen, Cmsg: import csp;
	Evt, LOC_CHANGED,CNS_OFFLINE: import cevent;


init (d:Dat):string
{
	initmods(d->mods);
	return nil;
}



Ptab.new():ref Ptab
{
	p:=ref Ptab;

	p.tab=array [NPROCS] of int;
	p.n=0;

	return p;

}


Ptab.add(p:self ref Ptab, pid:int)
{
	if (p.n<NPROCS)
	{
		p.tab[p.n]=pid;
		p.n++;
	}
	else
		log(stderr,"Ptab is full. Pid not save");

}


Ptab.del(p:self ref Ptab, pid:int)
{
	z:=p.n;
	for (i:=0;i<p.n;i++)
		if (p.tab[i]==pid)
			z=i;

	for (i=z;i<p.n;i++)
		if (i<NPROCS-1)
			p.tab[i]=p.tab[i+1];

	if (p.n>0)
		p.n--;	
}


Ptab.get(p:self ref Ptab):array of int
{
	return p.tab[:p.n];
}


Ptab.text(p:self ref Ptab):string
{
	s:="";
	for (i:=0;i<p.n;i++)
		s=s+sprint("[%d] %d\n",i,p.tab[i]);
	return s;
}



