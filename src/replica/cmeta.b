implement Cmeta;

include "mods.m";
	file2chan,FileIO,Rwrite,Rread,fprint,sprint:import sys;
	stderr,stdout,log,CSP_PORT,CARIBE_VOLS,CARIBE_NS,sysname,panic,cloclog,
		writeline,splitaddr:import utils;
	Service,Attributes:import registries;
	find : import cns;
	rand : import rnd;
	cspopen, Cmsg: import csp;
	Evt, LOC_CHANGED,CNS_OFFLINE: import cevent;


init (d:Dat):string
{
	initmods(d->mods);
	return nil;
}

MParser.new(file:string):ref MParser
{
	p:=ref MParser;
	p.cfg=load Cfg Cfg->PATH;
	p.cfg->init(file);
	p.file=file;

	return p;
}


MParser.value(p: self ref MParser, attr:string):string
{
	cfg:=p.cfg;

	ls:=cfg->lookup(attr);
	if (ls==nil)
		return nil;

	
	(prim,rec):=hd ls;
	return prim;
}


Meta.new(volume:string):ref Meta
{
	err:string;
	
	#mfile:=CARIBE_VOLS + "/"+volume+"/"+META_FILE;

	#(e,nil):=sys->stat(mfile);
	#if (e<0)
	#	err=newmetafile(volume);	# Creo el fichero meta en el volumen

	#if (err!=nil)
	#	return nil;

	m:=ref Meta;

	m.parser=nil;
	m.volume=volume;
	m.ccmax=META_DEFAULT_CCMAX;
	m.minrep=META_DEFAULT_MINREP;
	m.maxrep=META_DEFAULT_MAXREP;
	m.islands=META_DEFAULT_ISLANDS;
	m.quota=big META_DEFAULT_QUOTA;

	return m;
}



Meta.write(m:self ref Meta):string
{

	mfile:=CARIBE_NS + "/"+m.volume+"/files/"+META_FILE;

	#fd:=sys->create(mfile,sys->OWRITE, 8r644);
	fd:=sys->open(mfile,sys->OWRITE);
	if (fd==nil){
		fd=sys->create(mfile,sys->OWRITE, 8r644);
		#return sprint("Cannot create a metafile of volume %s",m.volume);
	}


	e:=writeline(fd,sprint("volume=%s",m.volume));
	e=writeline(fd,sprint("ccindex=%d",m.ccmax));
	e=writeline(fd,sprint("minrep=%d",m.minrep));
	e=writeline(fd,sprint("maxrep=%d",m.maxrep));
	e=writeline(fd,sprint("islands=%d",m.islands));
	e=writeline(fd,sprint("quota=%d",int m.quota));

	if (e<0)
		return sprint("Cannot write on metafile of volume %s",m.volume);

	return nil;
}



Meta.read(volume:string):ref Meta
{
	m:=ref Meta;
	err:string;
	
	mfile:=CARIBE_VOLS + "/"+volume+"/"+META_FILE;

	(e,nil):=sys->stat(mfile);
	if (e<0)	
		return nil;

	m.parser=MParser.new(mfile);
	m.volume=volume;
	m.ccmax=int m.parser.value("ccindex");
	m.minrep=int m.parser.value("minrep");
	m.maxrep=int m.parser.value("maxrep");
	m.islands=int m.parser.value("islands");
	m.quota=big m.parser.value("quota");

	if (m.ccmax<=0)
		m.ccmax=META_DEFAULT_CCMAX;
	if (m.minrep<=0)
		m.minrep=META_DEFAULT_MINREP;
	if (m.maxrep<=0)
		m.maxrep=META_DEFAULT_MAXREP;
	if (m.islands<=0)
		m.islands=META_DEFAULT_ISLANDS;
	if (m.quota<=big 0)
		m.quota=big META_DEFAULT_QUOTA;

	return m;
}



Meta.text(m:self ref Meta):string
{
	if (m==nil)
		return nil;

	s:="";
	s=s+"ccmax="+string m.ccmax+"\n";
	s=s+"minrep="+string m.minrep+"\n";
	s=s+"maxrep="+string m.maxrep+"\n";
	s=s+"quota="+string m.quota+"\n";

	return s;
}


newmetafile (volume:string):string
{
	mfile:=CARIBE_VOLS + "/"+volume+"/"+META_FILE;

	fd:=sys->create(mfile,sys->OWRITE,8r644);
	if (fd==nil)
		return sprint("Cannot create a metafile of volume %s",volume);

	e:=writeline(fd,sprint("volume=%s",volume));
	e=writeline(fd,sprint("ccindex=%s",string META_DEFAULT_CCMAX));
	e=writeline(fd,sprint("minrep=%s",string META_DEFAULT_MINREP));
	e=writeline(fd,sprint("maxrep=%s",string META_DEFAULT_MAXREP));
	e=writeline(fd,sprint("islands=%s",string META_DEFAULT_ISLANDS));
	e=writeline(fd,sprint("quota=%s",string META_DEFAULT_QUOTA));

	if (e<0)
		return sprint("Cannot write on metafile of volume %s",volume);

	return nil;
}




