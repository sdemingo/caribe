implement Ctree;

include "mods.m";
	Enotfound, Eexists, Eaccess: import Styxservers;
	MAX_CCINDEX : import creplica;
	dropprefix,log,stdout,ctreelog,isignoredpath:import utils;
	sprint: import sys;
	getenv:import env;


include "readdir.m";
	readdir: Readdir;

include "daytime.m";
	daytime:Daytime;


Fholder: adt {
	parentqid	: big;
	d	: Sys->Dir;
	lock	: Lock;
	child	: cyclic ref Fholder;
	sibling	: cyclic ref Fholder;
	hash	: cyclic ref Fholder;
};
	

nullpath:=big 0;

init(d:Dat)
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
	styx=load Styx Styx->PATH;
	styxs=load Styxservers Styxservers->PATH;
	daytime=load Daytime Daytime->PATH;
	
}

start(rootpath:string): (ref Tree, chan of ref Styxservers->Navop)
{
	fs := ref Tree(rootpath,chan of ref Treeop,chan of string,chan of ref Lock,chan[1] of int);
	c := chan of ref Styxservers->Navop;
	spawn fsproc(c, fs.c,fs.up_mutex);
	return (fs, c);
}

Tree.quit(t: self ref Tree)
{
	t.c <-= nil;
}

Tree.create(t: self ref Tree, parentq: big, d: Sys->Dir): string
{
	t.c <-= ref Treeop.Create(parentq, d,t.reply);
	return <-t.reply;
}

Tree.remove(t: self ref Tree, q: big): string
{
	t.c <-= ref Treeop.Remove(q,t.reply);
	return <-t.reply;
}

Tree.loadtree(t: self ref Tree, q: big): string
{	
	t.c <-= ref Treeop.Load(q,t.root,t.reply);
	return <-t.reply;
}

Tree.wstat(t: self ref Tree, q: big, d: Sys->Dir): string
{
	t.c <-= ref Treeop.Wstat(q, d,t.reply);
	return <-t.reply;
}

Tree.lock(t: self ref Tree, q: big, owner:string, level:int): string
{
	t.c <-= ref Treeop.Lock(q,owner,level,t.reply);
	return <-t.reply;
}

Tree.openlock(t: self ref Tree, q: big): ref Lock
{
	t.c <-= ref Treeop.Openlock(q,t.kreply);
	return <-t.kreply;

}

Tree.closelock(t: self ref Tree, q: big): string
{
	t.c <-= ref Treeop.Closelock(q,t.reply);
	return <-t.reply;

}


Tree.unlock(t: self ref Tree, q: big): string
{
	t.c <-= ref Treeop.Unlock(q,t.reply);
	return <-t.reply;
}


Tree.getpath(t: self ref Tree, q: big): string
{
	t.c <-= ref Treeop.Getpath(q,t.reply);
	spath:=<-t.reply;
	prefix:="./";
	return "/"+dropprefix(prefix,spath);
}


Tree.getppath(t: self ref Tree, q: big): string
{
	t.c <-= ref Treeop.Getppath(q,t.reply);
	spath:=<-t.reply;
	prefix:="./";
	return "/"+dropprefix(prefix,spath);
}


Tree.getqid (t: self ref Tree, file:string): big
{
	# Para saber el qid no miramos el arbol de metadatos
	# Podemos preguntarle directamente al sistema de ficheros
	# con un stat. El qid del Tree y del sistema de ficheros
	# deben coincidir pues al crear el fichero usamos el mismo.

	(e,d):=sys->stat(t.root+file);
	if (e<0)
		return big -1;
	else
		return d.qid.path;
}

Tree.dump (t: self ref Tree): string
{
	t.c <-= ref Treeop.Dump(nullpath,t.reply);	# no qid field neccesary
	dump:=<-t.reply;
	return dump;
}




fsproc(c: chan of ref Styxservers->Navop, fsc: chan of ref Treeop,mutex: chan of int)
{
	tab := array[23] of ref Fholder;

	for (;;) 
	{
		alt {
		grq := <-c =>
			if (grq == nil)
				exit;
			(q, reply) := (grq.path, grq.reply);
			fh := findfile(tab, q);
			if (fh == nil) {
				reply <-= (nil, Enotfound);
				continue;
			}

			mutex<-=1;	# close mutex to avoid tree reloads

			pick rq := grq {
			Stat =>
				reply <-= (ref fh.d, nil);

			Walk =>
				d := fswalk(tab, fh, rq.name);
				if (d == nil)
					reply <-= (nil, Enotfound);
				else
					reply <-= (d, nil);
			Readdir =>
				(start, end) := (rq.offset, rq.offset + rq.count);
				fh = fh.child;
				for (i := 0; i < end && fh != nil; i++) {
					if (i >= start)
						reply <-= (ref fh.d, nil);
					fh = fh.sibling;
				}
				reply <-= (nil, nil);
			* =>
				panic(sys->sprint("unknown op %d\n", tagof(grq)));
			}

			<-mutex;	# free the mutex to avoid reloads


		grq := <-fsc =>
			if (grq == nil)
				exit;

			##(q, reply) := (grq.q, grq.reply);

			pick rq := grq {
			Create =>
				rq.reply <-= fscreate(tab, rq.q, rq.d);
			Remove =>
				rq.reply <-= fsremove(tab, rq.q);
			Load =>
				rq.reply <-= fsload(tab, rq.q,rq.rootdir,mutex);
			Wstat =>
				rq.reply <-= fswstat(tab, rq.q, rq.d);
			Getpath =>
				rq.reply <-= fsgetpath(tab, rq.q);
			Getppath =>
				rq.reply <-= fsgetppath(tab, rq.q);
			Dump =>
				rq.reply <-= fsdump(tab);
			Lock =>
				rq.reply <-= fslock(tab, rq.q, rq.owner,rq.level);
			Openlock =>
				rq.kreply <-= fsopenlock(tab, rq.q);
			Closelock =>
				rq.reply <-= fscloselock(tab, rq.q);
			Unlock =>
				rq.reply <-= fsunlock(tab, rq.q);
			* =>
				panic(sys->sprint("unknown fs op %d\n", tagof(grq)));
			}
		}
	}
}


hashfn(q: big, n: int): int
{
	h := int (q % big n);
	if (h < 0)
		h += n;
	return h;
}


findfile(tab: array of ref Fholder, q: big): ref Fholder
{
	for (fh := tab[hashfn(q, len tab)]; fh != nil; fh = fh.hash)
		if (fh.d.qid.path == q)
			return fh;
	return nil;
}



fsgetpath(tab: array of ref Fholder, q: big): string
{
	fh := findfile(tab, q);
	if (fh == nil)
		return nil;
	s := fh.d.name;
	while (fh.parentqid != fh.d.qid.path) {
		fh = findfile(tab, fh.parentqid);
		if (fh == nil)
			return nil;
		s = fh.d.name + "/" + s;
	}
	return s;
}


fsgetppath(tab: array of ref Fholder, q: big): string
{
	fh := findfile(tab, q);
	if (fh == nil)
		return nil;	

	return fsgetpath(tab,fh.parentqid);
}



fswalk(tab: array of ref Fholder, fh: ref Fholder, name: string): ref Sys->Dir
{
	if (name == "..")
		return ref findfile(tab, fh.parentqid).d;
	for (fh = fh.child; fh != nil; fh = fh.sibling)
		if (fh.d.name == name)
			return ref fh.d;
	return nil;
}


fsremove(tab: array of ref Fholder, q: big): string
{
	prev: ref Fholder;

	# remove from hash table
	slot := hashfn(q, len tab);
	for (fh := tab[slot]; fh != nil; fh = fh.hash) {
		if (fh.d.qid.path == q)
			break;
		prev = fh;
	}
	if (fh == nil)
		return Enotfound;
	if (prev == nil)
		tab[slot] = fh.hash;
	else
		prev.hash = fh.hash;
	fh.hash = nil;

	# remove from parent's children
	parent := findfile(tab, fh.parentqid);
	if (parent != nil) {
		prev = nil;
		for (sfh := parent.child; sfh != nil; sfh = sfh.sibling) {
			if (sfh == fh)
				break;
			prev = sfh;
		}
		if (sfh == nil)
			panic("child not found in parent");
		if (prev == nil)
			parent.child = fh.sibling;
		else
			prev.sibling = fh.sibling;
	}
	fh.sibling = nil;

	# now remove any descendents
	sibling: ref Fholder;
	for (sfh := fh.child; sfh != nil; sfh = sibling) {
		sibling = sfh.sibling;
		sfh.parentqid = sfh.d.qid.path;		# make sure it doesn't disrupt things.
		fsremove(tab, sfh.d.qid.path);
	}
	return nil;
}



fscreate(tab: array of ref Fholder, q: big, d: Sys->Dir): string
{
	parent := findfile(tab, q);
	if (findfile(tab, d.qid.path) != nil)
		return Eexists;

	# allow creation of a root directory only if its parent is itself
	if (parent == nil && d.qid.path != q)
		return Enotfound;
	fh: ref Fholder;
	lock:=Lock(chan [1] of int,nil,MAX_CCINDEX);
	if (parent == nil)
		fh = ref Fholder(q, d, lock, nil, nil, nil);
	else {
		if (fswalk(tab, parent, d.name) != nil)
			return Eexists;
		fh = ref Fholder(parent.d.qid.path, d, lock, nil, nil, nil);
		fh.sibling = parent.child;
		parent.child = fh;
	}
	slot := hashfn(d.qid.path, len tab);
	fh.hash = tab[slot];
	tab[slot] = fh;
	
	##log(stdout,sprint("Qid en tree: {%x,%d,%x}",int fh.d.qid.path,fh.d.qid.vers,fh.d.qid.qtype));

	
	return nil;
}


fswstat(tab: array of ref Fholder, q: big, d: Sys->Dir): string
{
	fh := findfile(tab, q);
	if (fh == nil)
		return Enotfound;

	d = applydir(d, fh.d);

	# if renaming a file, check for duplicates
	if (d.name != fh.d.name) {
		parent := findfile(tab, fh.parentqid);
		if (parent != nil && parent != fh && fswalk(tab, parent, d.name) != nil)
			return Eexists;
	}
	fh.d = d;
	fh.d.qid.path = q;		# ensure the qid can't be changed

	## log(stdout,sprint("Qid en tree: {%x,%d,%x}",int fh.d.qid.path,fh.d.qid.vers,fh.d.qid.qtype));
	return nil;
}


fslock(tab: array of ref Fholder, q: big, user:string, i:int): string
{
	fh := findfile(tab, q);
	if (fh == nil)
		return Enotfound;

	fh.lock.owner=user;
	fh.lock.level=i;

	return nil;
}



fsunlock(tab: array of ref Fholder, q: big): string
{
	fh := findfile(tab, q);
	if (fh == nil)
		return Enotfound;

	fh.lock.owner=nil;
	fh.lock.level=MAX_CCINDEX;

	return nil;
}



fsopenlock(tab: array of ref Fholder, q: big): ref Lock
{
	fh := findfile(tab, q);
	if (fh == nil)
		return nil;			# return nil tuple

	fh.lock.exec<-=0;

	return ref fh.lock;
}


fscloselock(tab: array of ref Fholder, q: big): string
{
	fh := findfile(tab, q);
	if (fh == nil)
		return Enotfound;

	<-fh.lock.exec;

	return nil;
}


fsdump(tab: array of ref Fholder):string
{
	s:="";
	for (i:=0;i<len tab;i++)
	{
		if (tab[i]!=nil)
		{
			for (fh := tab[i]; fh != nil; fh = fh.hash)
			{
				s=s+fhtext(fh)+"\n";
			}
		}
	}	
	return s;	
}


fhtext(fh: ref Fholder):string
{
	return sys->sprint("%s Qid: {%x,%d,%x} Parent path: %d",fh.d.name,
					int fh.d.qid.path,
					int fh.d.qid.vers,
					int fh.d.qid.qtype,
					int fh.parentqid);
}


fsload(tab: array of ref Fholder, q: big, rootdir:string,mutex:chan of int): string
{
	mutex<-=1;	# close mutex to avoid any other styxervers op (nav_op)

	fsremove(tab,q);
	fscreate(tab,q,sdir(".", 8r777|Sys->DMDIR, q));

	dir:string=nil;
	if (q!=Q_ROOT)
	{
		dir=fsgetpath(tab, q);
		prefix:="./";
		dir=rootdir+"/"+dropprefix(prefix,dir);
	}
	else
		dir=rootdir;

	(es,rd):=sys->stat(dir);
	if (es>=0)
		loaddir(tab,dir,rootdir);

	<-mutex;

	return nil;
}


loaddir(tab: array of ref Fholder, name:string, rootdir:string)
{	
	parent:big;
	parentd:sys->Dir;
	e:int;

	if (name==rootdir)
		parent=Q_ROOT;
	else
	{
		(e,parentd)=sys->stat(name);
		parent=parentd.qid.path;
	}

	(d, n) := readdir->init(name, Readdir->NONE | Readdir->COMPACT);
	if (n<0)
		return;


	for(i := 0; i < n; i++){
		if(d[i].mode & Sys->DMDIR)
		{
			path := name+"/"+d[i].name;
			log (ctreelog, sprint ("load dir: %s ",path));
			fscreate(tab, parent,*d[i]);
			loaddir(tab,path, rootdir);
		}
		else
		{
			path := name+"/"+d[i].name;
			log (ctreelog, sprint ("load file: %s ",path));	
			fscreate(tab, parent,*d[i]);	
		}
	}
}



applydir(d: Sys->Dir, onto: Sys->Dir): Sys->Dir
{
	if (d.name != nil)
		onto.name = d.name;
	if (d.uid != nil)
		onto.uid = d.uid;
	if (d.gid != nil)
		onto.gid = d.gid;
	if (d.muid != nil)
		onto.muid = d.muid;
	if (d.qid.vers != ~0)
		onto.qid.vers = d.qid.vers;
	#if (d.qid.qtype != ~0)
	if (d.qid.qtype != 16rff)
		onto.qid.qtype = d.qid.qtype;
	if (d.mode != ~0)
		onto.mode = d.mode;
	if (d.atime != ~0)
		onto.atime = d.atime;
	if (d.mtime != ~0)
		onto.mtime = d.mtime;
	if (d.length != ~big 0)
		onto.length = d.length;
	if (d.dtype != ~0)
		onto.dtype = d.dtype;
	if (d.dev != ~0)
		onto.dev = d.dev;
	return onto;
}


sdir(name: string, perm: int, qid: big): Sys->Dir
{
     	d := sys->zerodir;
     	d.name = name;
	if (perm & Sys->DMDIR)
     		d.qid.qtype = Sys->QTDIR;
     	else
     		d.qid.qtype = Sys->QTFILE;

	user:=getenv("user");
	if (user!=nil)
	{
     		d.uid = user;
     		d.gid = user;
	}
	else
	{		
		d.uid = "caribe";
     		d.gid = "caribe";
	}
     	d.qid.path = qid;
	d.mtime=daytime->now();
	d.atime=daytime->now();
     	d.mode = perm;

     	return d;
}



panic(s: string)
{
	sys->fprint(sys->fildes(2), "panic: %s\n", s);
	raise "panic";
}
