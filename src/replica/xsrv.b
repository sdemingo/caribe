implement Xsrv;

include "mods.m";
	Connection,sprint,open,read,write,stat,create, FD :import sys;
	stderr,stdout,log,creplicalog,sysname,panic,CSP_PORT,timeout,splitaddr,
	killgrp,size,xlog:import utils;
	Service,Attributes:import registries;
	Tree: import ctree;
	Evt:import cevent;
	Tmsg, Rmsg :import styx;
	Cop: import Csp;
	Fid,Styxserver, Navigator,Eperm, Ebadfid,Enotfound:import styxs;
	getenv:import env;
	rand : import rnd;
	getmaxcc: import cloader;
	Meta : import cmeta;
	writehis,delhis,createhis,HISTORY_PATH	: import history;

include "readdir.m";
	readdir: Readdir;

include "daytime.m";
	daytime:Daytime;

Qroot: con big iota;	# paths
Nroot:	con ".";



init (d:Dat)
{
	initmods(d->mods);
	readdir=load Readdir Readdir->PATH;
	daytime=load Daytime Daytime->PATH;

}


Xop.text(xm: self ref Xop):string
{
	s:="";


	pick xop := xm {
		Create=>
			s=s+xop.m.text();
		Write=>
			s=s+xop.m.text();
		Read=>
			s=s+xop.m.text();
		Remove=>
			s=s+xop.m.text();
		Clunk=>
			s=s+xop.m.text();
		Stat=>
			s=s+xop.m.text();
		Wstat=>
			s=s+xop.m.text();
		Open=>
			s=s+xop.m.text();
		Upload=>
			s=s+"upload xop";
		Walk=>
			s=s+xop.m.text();
		Attach=>
			s=s+xop.m.text();
	
	}

	return s;
}




xreply (s:ref Styxserver, m: ref Rmsg)
{
	log (xlog,sprint("<=[out] %s",m.text()));
	s.reply(m);
}



Xfs.new(n:string, t:ref Ctree->Tree): ref Xfs
{
	x:= ref Xfs;
	x.tree=t;
	x.c = chan of ref Xop;
	x.reply = chan of string;
	x.id=n;

	spawn xfsproc(x);

	return x;
}

Xfs.xwrite(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Write):string
{
	x.c <-= ref Xop.Write(s,x.reply,m);
	return <-x.reply;
}

Xfs.xread(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Read):string
{
	x.c <-= ref Xop.Read(s,x.reply,m);
	return <-x.reply;
}


Xfs.xcreate(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Create):string
{
	x.c <-= ref Xop.Create(s,x.reply,m);
	return <-x.reply;
}

Xfs.xremove(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Remove):string
{
	x.c <-= ref Xop.Remove(s,x.reply,m);
	return <-x.reply;
}

Xfs.xwstat(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Wstat):string
{
	x.c <-= ref Xop.Wstat(s,x.reply,m);
	return <-x.reply;
}

Xfs.xstat(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Stat):string
{
	x.c <-= ref Xop.Stat(s,x.reply,m);
	return <-x.reply;
}

Xfs.xclunk(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Clunk):string
{
	x.c <-= ref Xop.Clunk(s,x.reply,m);
	return <-x.reply;
}


Xfs.xwalk(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Walk):string
{
	x.c <-= ref Xop.Walk(s,x.reply,m);
	return <-x.reply;
}

Xfs.xopen(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Open):string
{
	x.c <-= ref Xop.Open(s,x.reply,m);
	return <-x.reply;
}

Xfs.xattach(x: self ref Xfs, s:ref Styxservers->Styxserver, m: ref Styx->Tmsg.Attach):string
{
	x.c <-= ref Xop.Attach(s,x.reply,m);
	return <-x.reply;
}

Xfs.xupload(x: self ref Xfs):string
{
	x.c <-= ref Xop.Upload(nil,x.reply);
	return <-x.reply;
}







xfsproc(x: ref Xfs)
{
	for(;;)
	{
		#log (stdout,sprint(" xfsproc de  %s esperando ... ",x.id));

		xm:=<-x.c;

		#log (stdout,sprint(" xfsproc de %s recibe:  %s ",x.id,xm.text()));

		if (xm == nil)
			break;

		pick xop := xm {
		Create=>
			xop.reply <-= xfs_xcreate(xop.s,x.tree,xop.m);
		Write=>
			xop.reply <-= xfs_xwrite(xop.s,x.tree,xop.m);
		Read=>
			xop.reply <-= xfs_xread(xop.s,x.tree,xop.m);
		Remove=>
			xop.reply <-= xfs_xremove(xop.s,x.tree,xop.m);
		Clunk=>
			xop.reply <-= xfs_xclunk(xop.s,x.tree,xop.m);
		Stat=>
			xop.reply <-= xfs_xstat(xop.s,x.tree,xop.m);
		Wstat=>
			xop.reply <-= xfs_xwstat(xop.s,x.tree,xop.m);
		Open=>
			xop.reply <-= xfs_xopen(xop.s,x.tree,xop.m);
		Upload=>
			xop.reply <-= xfs_xupload(x.tree);
		Walk=>
			xop.reply <-= xfs_xwalk(xop.s,x.tree,xop.m);
		Attach=>
			xop.reply <-= xfs_xattach(xop.s,x.tree,xop.m);
		* =>
			log(stdout," panic!!!!!!! ");
	
		}
	}
	##log (stdout,"xfsproc break");
}




	#
	# Metodos privados llamados desde el proceso secuenciador
	#




xfs_xupload(t:ref Ctree->Tree):string
{
	t.loadtree(ctree->Q_ROOT);
	return nil;
}



xfs_xwrite (s:ref Styxservers->Styxserver,tree: ref Ctree->Tree, m: ref Tmsg.Write):string
{
	log (xlog,sprint("[in]=> %s",m.text()));
	err:string=nil;
	n:=0;

	fid := s.getfid(m.fid);
	if  (fid == nil)
		return "xwrite: "+Ebadfid;

	(err,n)=writeondisc(s,tree,fid,m,err);
	if (err!=nil)
		err="ondisc: "+err;


	err=writeontree(s,tree,fid,err);
	if (err==nil)
	{
		xreply(s,ref Rmsg.Write(m.tag,n));
		return nil;
	}
	else
	{
		xreply(s,ref Rmsg.Error(m.tag, err));
		return "xwrite: "+err;
	}
}



xfs_xread (s:ref Styxservers->Styxserver,tree: ref Ctree->Tree, m: ref Tmsg.Read):string
{
	log (xlog,sprint("[in]=> %s",m.text()));
	err:string=nil;	
	c:=s.getfid(m.fid);
	if (c == nil)
	{
		xreply(s,ref Rmsg.Error(m.tag, Ebadfid));
		return Ebadfid;
	}
	else
	{
		if((c.qtype & Sys->QTDIR) == 0)	
			err=readfromdisc(s,tree,m);
		else				
			s.read(m);
	}

	return err;
}


xfs_xcreate (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Create):string
{
	##log (xlog,sprint("[in]=> %s",m.text()));
	dir:sys->Dir;
	err:string=nil;	

	(err,dir)=createondisc(s,tree,m,err);
	if (err!=nil)
		err="ondisc "+err;

	err=createontree(s,tree, m,dir,err);
	if (err==nil)
	{
		xreply(s,ref Rmsg.Create(m.tag,dir.qid,8192));
		return nil;
	}
	else
	{
		xreply(s,ref Rmsg.Error(m.tag, err));
		return "xcreate: "+err;
	}

}


xfs_xremove (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Remove):string
{

	log (xlog,sprint("[in]=> %s",m.text()));
	(fid,parentq,err):=s.canremove(m);
	err=removeondisc(s,tree, fid,err);
	if (err!=nil)
		err="ondisc: "+err;
	err=removeontree(s,tree,fid,err);

	if (err==nil)
	{
		xreply(s,ref Rmsg.Remove(m.tag));
		return nil;
	}
	else
	{
		xreply(s,ref Rmsg.Error(m.tag, "Not removed"));
		return "xremove: "+err;
	}

}


xfs_xwstat (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Wstat):string
{
	log (xlog,sprint("[in]=> %s",m.text()));
	err:string=nil;	
	fid := s.getfid(m.fid);
	err=wstatondisc(s,tree,fid,m.stat);
	err=wstatontree(s,tree,fid,m.stat,err);
	if (err==nil)
	{
		xreply(s,ref Rmsg.Wstat(m.tag));
		return nil;
	}
	else
	{
		xreply(s,ref Rmsg.Error(m.tag, sprint("Wstat failed. %s",err)));
		return  sprint("Wstat failed. %s",err);
	}

}


xfs_xstat (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Stat):string	
{
	log (xlog,sprint("[in]=> %s",m.text()));
	s.default(m);
	log (xlog,"<=[out] Rstat");
	return nil;
}



xfs_xclunk (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Clunk):string	
{
	log (xlog,sprint("[in]=> %s",m.text()));
	fid := s.getfid(m.fid);
	if(fid == nil) 
	{
		xreply(s,ref Rmsg.Error(m.tag, Ebadfid));
		return Ebadfid;
	}
	else
	{
		s.delfid(fid);
		xreply(s,ref Rmsg.Clunk(m.tag));
		return nil;
	}		
}


xfs_xwalk (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Walk):string
{
	log (xlog,sprint("[in]=> %s",m.text()));
	s.walk(m);
	log (xlog,"<=[out] Rwalk");
	return nil;
}


xfs_xopen (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Open):string
{
	log (xlog,sprint("[in]=> %s",m.text()));

	fid := s.getfid(m.fid);
	if  (fid == nil)
		return Ebadfid;

	if (s.open(m)!=nil)
	{
		recoverymode(s,m);	 # recovery the OTRUNC flag if it's necesary
		log (xlog,"<=[out] Ropen");
		return nil;
	}
	else
	{
		log (xlog,"<=[out] Rerror");
		return nil;
	}
	
}


xfs_xattach (s:ref Styxservers->Styxserver, tree: ref Ctree->Tree, m: ref Tmsg.Attach):string
{
	log (xlog,sprint("[in]=> %s",m.text()));
	s.attach(m);
	log (xlog,"<=[out] Rattach");
	return nil;
}











	# Metodos para la manipulación del árbol ctree de metadados
	# y para la gestión del espacio de nombres o directorio sobre el 
	# que se está trabajando


getpath(tree: ref Ctree->Tree, path:big):string
{
	return tree.getpath(path);
}




readfromdisc(srv: ref Styxserver, tree: ref Tree, m: ref Tmsg.Read):string
{
	# CUIDADO con ficheros  grandes. Deberia leerlos en varios
	# rpcs no?

	rmsg:ref Rmsg;
	(c, err) := srv.canread(m);
	if(c == nil){
		xreply(srv,ref Rmsg.Error(m.tag, err));
		return err;
	}
	a := array[m.count] of byte;
	pathfile:=tree.root+getpath(tree,c.path);
	if (pathfile==nil)
	{
		xreply(srv,ref Rmsg.Error(m.tag, sys->sprint("readfromdisc. no path %s founded",pathfile)));
		return sys->sprint("readfromdisc. no path %s founded",pathfile);
	}

	fd:=sys->open(pathfile,Sys->OREAD);
	if (fd==nil)
	{
		xreply(srv,ref Rmsg.Error(m.tag, sys->sprint("readfromdisc. open error. %r")));
		return sys->sprint("open error. %r");
	}

	if (m.offset > big 0)
		sys->seek(fd,m.offset,Sys->SEEKSTART);
	
	d := sys->read(fd,a,m.count);
	xreply(srv, ref Rmsg.Read(m.tag, a[0:d]));

	return nil;
}




writeontree(srv: ref Styxserver, tree: ref Tree, fid:ref Fid,e:string):string
{
	if (e!=nil)
		return e;

	pathfile:=tree.root+getpath(tree,fid.path);
	(err,dir):=sys->stat(pathfile);
	if (err<0)
		return Enotfound;

	tree.wstat(fid.path,dir);

	return nil;
}


writeondisc(srv: ref Styxserver, tree: ref Tree, fid:ref Fid, m: ref Tmsg.Write,e:string):(string,int)
{
	rmsg:ref Rmsg;
	if ( (fid==nil) || (fid.isopen==0) || (e!=nil) )
		return (Ebadfid,0);

	pfile:=getpath(tree,fid.path);
	if (pfile==nil)
		return (Ebadfid,0);		


	pathfile:=tree.root+pfile;
	fd:=sys->open(pathfile,fid.mode);	
	if (fd==nil) 
	{
		return (pathfile+" "+Enotfound,0);
	}

	if (m.offset > big 0)
		sys->seek(fd,m.offset,Sys->SEEKSTART);

	d:=sys->write(fd,m.data,len m.data);

	################ Modifico la historia #############

	writehis(tree.root,pfile);

	#################################################

	return (nil,d);
}


createontree(srv: ref Styxserver, tree: ref Tree, m: ref Tmsg.Create,dir:sys->Dir,e:string):string
{
	if (e!=nil)
		return e;

	fid:=srv.getfid(m.fid);
	if(fid == nil)
		return Ebadfid;

	# 
	# fid.path es el qid del padre donde añadir el nuevo dir
	#

	tree.create(fid.path,dir);
	fid.walk(dir.qid);		# ahora fid apunta a dir.qid
	fid.open(m.mode,dir.qid);	# dejo abierto el fid de dir.qid

	(e2,hdir):=stat(HISTORY_PATH);	# dir del fichero historia
	tree.loadtree(hdir.qid.path);

	return nil;
}




createondisc(srv: ref Styxserver, tree: ref Tree, m: ref Tmsg.Create,e:string):(string,sys->Dir)
{
	if (e!=nil)
		return (e,sys->zerodir);

	fid2:=srv.getfid(m.fid);
	ppfile:=getpath(tree,fid2.path);
	if (ppfile==nil)
		return (Ebadfid,sys->zerodir);
	

	(fid,mode,newdir,er):=srv.cancreate(m);
	if(fid == nil)
	{
		return ("cancreate: "+er,sys->zerodir);
	}


	pathparent:=tree.root+ppfile;
	p:string;
	if (ppfile!="/")
		p=pathparent+"/"+newdir.name;
	else
		p=pathparent+newdir.name;

	fd:ref sys->FD;

	# BUG: Los permisos deberian respetar los del padre y la propia mascara
	# no inventados y cableados como ahora

	if(m.perm & Sys->DMDIR)
	{
		mod:=8r777 | Sys->DMDIR;
		fd=sys->create(p,Sys->OREAD,mod);
	}
	else
	{
		mod:=8r666;
		fd=sys->create(p,Sys->OWRITE,mod);
	}
	if (fd==nil)
		return (sprint("File not created. %r"),sys->zerodir);


	
	####### Creo aqui ahora la historia en el disco #######

	#pathparent=tree.root+"/history"+ppfile;
	#h:string;
	#if (ppfile!="/")
	#	h=pathparent+"/"+newdir.name;
	#else
	#	h=pathparent+newdir.name;
	#if(m.perm & Sys->DMDIR)
	#{
	#	mod:=8r777 | Sys->DMDIR;
	#	fd=sys->create(h,Sys->OREAD,mod);
	#}
	#else
	#{
	#	mod:=8r666;
	#	fd=sys->create(h,Sys->OWRITE,mod);
	#}
	#if (fd==nil)
	#	return (sprint("His not created. %r"),sys->zerodir);

	rpath:="";
	if ( (ppfile=="/") || (ppfile=="/.")) 
		rpath="/"+newdir.name;
	else
		rpath=ppfile+"/"+newdir.name;
	if (createhis(tree.root,rpath,m.perm)==nil)
	{
		log(stdout,sprint("Failed createhis on %s/history%s",tree.root,rpath));
	}
	###################################################


	(err,dir):=stat(p);
	if (err<0)
		return ("stat after create:"+Enotfound,sys->zerodir);

	return (nil,dir);
}


removeontree(srv: ref Styxserver, tree: ref Tree,fid:ref Fid,e:string):string
{
	if (e!=nil) 
		return e;
	# Lo borramos del arbol
	e=tree.remove(fid.path);
	if (e==nil)
		srv.delfid(fid);
	return e;	
}


removeondisc(srv: ref Styxserver, tree: ref Tree,fid:ref Fid,e:string):string
{
	if (e!=nil) 
		return e;
	filepath:=tree.root+getpath(tree,fid.path);
	rmdir(filepath);

	delhis(tree.root,getpath(tree,fid.path));

	return nil;
}


rmdir(name: string)
{
	allwrite := Sys->nulldir;
	allwrite.mode = 8r777 | Sys->DMDIR;

	(d, n) := readdir->init(name, Readdir->NONE|Readdir->COMPACT);
	for(i := 0; i < n; i++){
		path := name+"/"+d[i].name;
		if(d[i].mode & Sys->DMDIR)
			rmdir(path);
		else
			sys->remove(path);
	}
	sys->remove(name);
}


wstatontree(srv: ref Styxserver, tree: ref Tree,fid:ref Fid,dir:sys->Dir ,e:string):string
{
	if ( (e!=nil) || (fid==nil) )
		return sprint("wstatontree: %s",Ebadfid);

	tree.wstat(fid.path,dir);
	return nil;
}


wstatondisc(srv: ref Styxserver, tree: ref Tree,fid:ref Fid, dir:sys->Dir):string
{
	if (fid==nil)
		return sprint("wstatondic: %s",Ebadfid);

	filepath:=tree.root+getpath(tree,fid.path);
	er:=sys->wstat(filepath, dir);
	if (er<0)
		return sprint("wstatondic: wstat(%s) return -1",filepath);
	return nil;
}





recoverymode(s:ref Styxserver, m: ref Tmsg.Open)
{
	# Usado en xopen:
	# Aqui intentamos reconstruir
	# el flag OTRUNC en el caso de
	# que el mensaje styx de open lo llevara
	# Esto es necesario pues la operación
	# openmode de /appl/lib/styxservers:582
	# le elimina este flag.

	fid:= s.getfid(m.fid);
	if (fid==nil)
		return;
	
	if (m.mode & sys->OTRUNC)
		fid.mode = fid.mode | sys->OTRUNC;
	
}

