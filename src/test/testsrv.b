implement Testsrv;

# 
# Modulo para probar el funcionamiento
#  de una replica de forma independiente.
# 	
#  Para montarla desde una sh usamos:
# 	
# 	mount -Ac tcp!maquina!19888 /mnt/prueba
# 


include "mods.m";
	sprint : import sys;
	Ptab	: import procs;
	stderr,log,panic,stdout, Cmdargs, checkinit	: import utils;
	Replica, Cmodule, OP,SYNC,CORR,INIT,DIED,
		REPL_SERVICE_PH_TIME,CCNULL: import creplica;

Testsrv: module
{
	init : fn(nil:ref Draw->Context, args: list of string);
};



init (nil:ref Draw->Context, args: list of string)
{
	# system modules
	sys=load Sys Sys->PATH;
	dat=load Dat Dat->PATH;
	dat->loadmods(sys);
	initmods(dat->mods);

	# system modules
	rnd->init(sys->millisec());
	styx->init();
	styxs->init(styx);
	styxs->traceset(1);

	# modulos sin dependencias
	utils->init(dat,1);
	cdebug->init(dat);
	(flags,e):=Cmdargs.new(args);
	if ((e!=nil) || (flags==nil))
		panic(e);

	flags.print();

	xsrv->init(dat);
	sync->init(dat);
	checkinit(cloader->init(dat));
	csp->init(dat);
	vdb->init(dat);
	ctree->init(dat);
	checkinit(cevent->init(dat));
	creplica->init(dat);
	cns->init(dat);
	cmeta->init(dat);
	cproxy->init(dat);
	auto->init(dat);
	procs->init(dat);
	history->init(dat);
	

	# Inicio de la réplica
	# =====================

	dir:="/tmp/prueba";
	rep:=Replica.new("prueba",dir,0);
	if (rep==nil){
		sys->print("Replica no iniciada");
		return;
	}


	sys->sleep(30000);
	sys->print("%s\n",rep.text());
}