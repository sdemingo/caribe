implement Testislands;

include "sys.m";
	sys: Sys;

include "draw.m";
	draw: Draw;

include "readdir.m";
	readdir: Readdir;

include "string.m";
	str: String;


FD:import sys;


#
#	Test que provoca retrasos de red artificiales dentro de caribe
#
#



Testislands : module
{
	init : fn (nil : ref Draw->Context, args:list of string);

};

TIME_TO_NEW_DELAY: con 60000;

#delays := array [] of {5000,200,8000,100,6000,400};
delays := array [] of {5000};


init (ctxt: ref Draw->Context, args:list of string)
{
	sys=load Sys Sys->PATH;
	readdir=load Readdir Readdir->PATH;
	str=load String String->PATH;
	err:string;

	if (len args<1)
	{
		sys->print ("usage: testislands\n");
		return;
	}

	for (i:=0; i<len(delays); i++)
	{
		f:=sys->open("/chan/ccmd", sys->OWRITE);
		if (f!=nil)
		{
			sys->print("Setting net delay to %d ms\n",delays[i]);
			msg:=sys->sprint("debug netdelay %d\n",delays[i]);
			sys->write(f,array of byte msg, len msg);
			sys->sleep(TIME_TO_NEW_DELAY);
		}
	}
}










