implement Killn;

include "sys.m";
	sys: Sys;

include "draw.m";
	draw: Draw;

include "readdir.m";
	readdir: Readdir;

include "string.m";
	str: String;

Killn : module
{
	init : fn (nil : ref Draw->Context, args:list of string);

};

names:=array [ ] of  {"Cc", "Creplica", "Cns", "Cnode","Ccmd","Killn","Cproxy","Utils", "Cfs","Cloc"};

init (ctxt: ref Draw->Context, args:list of string)
{
	sys=load Sys Sys->PATH;
	readdir=load Readdir Readdir->PATH;
	str=load String String->PATH;
	err:string;

	(d, n) := readdir->init("/prog", Readdir->NONE|Readdir->COMPACT);
	if (n<0)
		return;

	sys->print ("Checking %d proccess\n",n);

	for(i := 0; i < n; i++){
		pid:=d[i].name;
		fd:=sys->open("/prog/"+pid+"/status",Sys->OREAD);
		if (fd!=nil)
		{
			a := array[128] of byte;
			sys->seek(fd,big 57,Sys->SEEKSTART);
			n := sys->read(fd,a,128);
			proc:=string a[:n];
			if (checkname(proc,names))
			{
				err=killproc(pid);
				if (err==nil)
					sys->print ("Proc %s killed\n",proc);
				else	
					sys->print ("%s\n",err);
			}				
		}
	}
	sys->print ("done\n");
}



checkname (proc:string, names:array of string):int
{
	for (i:=0; i<len names; i++)
	{
		(p,nil):=str->splitl(proc,"[");
		if (names[i]==p)
			return 1;
	}
	return 0;
}



killproc (pid:string):string
{
	msg:="killgrp";
	procfile:="/prog/"+pid+"/ctl";
	(e,d):=sys->stat(procfile);
	if (e<0)	
		return sys->sprint("Killn failed on %s",procfile);

	fd:=sys->open(procfile, sys->OWRITE);
	if (fd==nil)
		return sys->sprint("Killn failed on %s",procfile);
	else
		n:=sys->write (fd,array of byte msg, len msg);

	return nil;
}



