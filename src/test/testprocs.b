implement Testprocs;

include "mods.m";
	sprint : import sys;
	Ptab	: import procs;
	log,stdout	: import utils;

	
Testprocs: module
{
	init : fn(nil:ref Draw->Context, args: list of string);
};


init (nil:ref Draw->Context, args: list of string)
{
	# system modules
	sys=load Sys Sys->PATH;
	dat=load Dat Dat->PATH;
	dat->loadmods(sys);
	initmods(dat->mods);

	utils->init(dat,1);
	procs->init(dat);

	t:=Ptab.new();
	t.add(5);
	t.add(8);
	t.add(9);
	t.add(10);

	log (stdout,sprint("\n%s",t.text()));

	t.del(8);
	t.del(10);

	log (stdout,sprint("\n%s",t.text()));

}

