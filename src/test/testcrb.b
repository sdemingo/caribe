implement Testcrb;

include "sys.m";
	sys: Sys;

include "draw.m";
	draw: Draw;

include "readdir.m";
	readdir: Readdir;

include "string.m";
	str: String;

include "env.m";
	env: Env;


FD:import sys;


#
#	Para usar este test se necesita haber arrancado
#	antes una réplica caribe. El modo de uso es:
#
#	Probar creación:	testcrb.dis <create> <volumen> <numero_ficheros>
#	Probar borrado: 	testcrb.dis <remove> <volumen> <numero_ficheros>
#	Probar modificaciòn:	testcrb.dis <write>  <volumen> <numero_ficheros>
#	Probar escritura lenta:	testcrb.dis <writeb>  <volumen> <numero_ficheros>
#	Probar bloqueos:	testcrb.dis <createb> <volumen> <segundos>
#



Testcrb : module
{
	init : fn (nil : ref Draw->Context, args:list of string);

};


FILE_SIZE	: con 2*1024;
WRITE_OFFSET	: con 10;

init (ctxt: ref Draw->Context, args:list of string)
{
	sys=load Sys Sys->PATH;
	readdir=load Readdir Readdir->PATH;
	str=load String String->PATH;
	env=load Env Env->PATH;
	err:string;

	if (len args<4)
	{
		sys->print ("usage: testcrb cmd volume num\n");
		return;
	}

	argv:=array [10] of string;
	v:=0;
	while (args!=nil)
	{
		argv[v]=hd args;
		v++;
		args=tl args;
	}
	
	cmd:=argv[1];
	volume:=argv[2];
	nfiles:=int argv[3];
	path:="/n/caribe/"+volume+"/files";

	if (cmd=="create")
	{
		for (i:=0;i<nfiles;i++)
			createfile(sys->sprint("%s/f%d",path,i));
	}
	else if (cmd=="remove")
	{
		for (i:=0;i<nfiles;i++)
			removefile(sys->sprint("%s/f%d",path,i));
	}
	else if (cmd=="write")
	{
		for (i:=0;i<nfiles;i++)
			writefile(sys->sprint("%s/f%d",path,i));
	}
	else if (cmd=="writeb")
	{
		for (i:=0;i<nfiles;i++)
			writeandsleep(sys->sprint("%s/f%d",path,i));
	}
	else if (cmd=="createb")
	{
		secs:=nfiles;
		createandsleep(sys->sprint("%s/fblock",path),secs);
	}
	

	#createatestndsleep(sys->sprint("%s/fsleep",path));
}



createfile(file:string)
{
	fd:=sys->create(file, sys->ORDWR, 8r644);
	n:=sys->seek(fd,big FILE_SIZE, sys->SEEKSTART);
	m:=sys->write(fd,array of byte "0",1);
	sys->print ("create %s\n",file);
}


writefile(file:string)
{
	fd:=sys->open(file, sys->ORDWR);
	n:=sys->seek(fd,big WRITE_OFFSET, sys->SEEKSTART);
	msg:="\nMensaje introducido en mitad del fichero\n";
	m:=sys->write(fd,array of byte msg,1);
	sys->print ("write on %s\n",file);
}



writeandsleep(file:string)
{
	fd:=sys->open(file, sys->ORDWR);
	msg:="c";

	sys->print ("write and sleep on %s during 120 secs\n",file);

	for (i:=1;i<120;i++){
		m:=sys->write(fd,array of byte msg,1);
		sys->sleep(1000);
	}

	sys->print ("write and sleep over %s done\n",file);
}



removefile(file:string)
{
	fd:=sys->remove(file);
	sys->print ("remove %s\n",file);
}



createandsleep(file:string,secs:int)
{	
	fd:ref FD;
	(i,e):=sys->stat(file);
	if (i<0)
		fd=sys->create(file, sys->ORDWR, 8r644);
	else
		fd=sys->open(file, sys->ORDWR);

	msg:=sys->sprint("Write on %s\n",env->getenv("sysname"));

	for (n:=0;n<secs;n++){
		sys->sleep(1000);
		m:=sys->write(fd,array of byte msg,len msg);
	}
}





