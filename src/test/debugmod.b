implement Debugmod;

include "mods.m";
	sprint : import sys;
	prefix : import str;

include "debug.m";
	debug: Debug;
	Prog, Exp: import debug;

include "readdir.m";
	readdir: Readdir;

include "sh.m";
	sh: Sh;

	
Debugmod: module
{
	init : fn(nil:ref Draw->Context, args: list of string);
};


init (nil:ref Draw->Context, args: list of string)
{
	# system modules
	sys=load Sys Sys->PATH;
	dat=load Dat Dat->PATH;
	dat->loadmods(sys);
	initmods(dat->mods);

	debug=load Debug Debug->PATH;
	readdir=load Readdir Readdir->PATH;
	sh=load Sh Sh->PATH;
	debug->init();


	if (len args!=2)
	{
		sys->print ("usage: debugmod <module>\n");
		return;
	}

	dmod:=hd (tl args);

	sys->print("Depuramos el modulo %s\n",dmod);

	sd:=sys->open("/prog", sys->OREAD);
	if (sd==nil)
	{
		sys->print ("/prog no mounted\n");
		return;
	}

	nprocs:=0;

	(pids,n):=readdir->readall(sd,readdir->NAME);
	for (i:=0; i<n; i++)
	{
		pid:=int pids[i].name;
		(p,err):=debug->prog(pid);
		(nil,nil,nil,mod):=p.status();
		if (prefix(dmod,mod))	
		{
			nprocs++;
			traceback(p,pid);
		}
	}
	sys->print("\n - %d threads for module %s\n",nprocs,dmod);
}



traceback(p:ref Prog,pid:int)
{
	(grp,nil,nil,mod):=p.status();
	sys->print ("--------- Thread %d (Group:%d) ---------\n",pid,grp);
	sh->system(nil,sprint("stack -v %d\n",pid));	
}

