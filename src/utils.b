implement Utils;

include "mods.m";

include "daytime.m";
	daytime:Daytime;
	now,local,time,gmt,text:import daytime;
	Db, Dbentry, Tuples : import attrdb;
	sprint:import sys;
	rand : import rnd;
	getenv:import env;

include "readdir.m";
	readdir: Readdir;

include "sh.m";
	sh:Sh;

include "arg.m";
	arg : Arg;

	
vdebug:int;
ip: string = nil;


LENBUF		: con 5*1024;	# bytes;

#
# Ignored paths for the coherence mechanism
#

ignored_paths = array [] of {
		history->HISTORY_PATH
	};





Log: adt
{
	fd: ref sys->FD;
	name: string;
};

logtable:= array [MAXLOG] of Log;

flags:ref Cmdargs = nil;

init(d:Dat,deb:int)
{
	initmods(d->mods);
	daytime=load Daytime Daytime->PATH;
	readdir=load Readdir Readdir->PATH;
	arg=load Arg Arg->PATH;
	sh=load Sh Sh->PATH;	
	vdebug=deb;
	sout:=sys->fildes(1);
	

	logtable[stdout].fd = sout;
	#logtable[stderr].fd = sout;
	logtable[stderr].fd = openlog(CARIBE_LOGS+"/errors");


	if (vdebug)
	{
		p:=CARIBE_HOME+"/logs";
		(er,d):=sys->stat(p);
		if (er<0)
			sys->create(p,sys->OREAD,8r777| sys->DMDIR);

		logtable[ccmdlog].fd =  sout;
		logtable[ccmdlog].name= "Ccmd"; 

		logtable[cmntlog].fd =  openlog("/dev/null");
		logtable[cmntlog].name= "Cmount";

		logtable[cnslog].fd =   openlog("/dev/null");
		logtable[cnslog].name= "Cns";

		logtable[csplog].fd = openlog("/dev/null");
		logtable[csplog].name= "Csp";

		logtable[cloclog].fd =   openlog("/dev/null");
		logtable[cloclog].name= "Cloc";

		logtable[xlog].fd = openlog("/dev/null");
		logtable[xlog].name= "Xsrv";

		logtable[synclog].fd =  openlog("/dev/null");
		logtable[synclog].name= "Sync";

		logtable[cnodelog].fd = sout;
		logtable[cnodelog].name= "Cnode";

		logtable[creplicalog].fd = sout;
		logtable[creplicalog].name= "Creplica";

		logtable[cproxylog].fd =   openlog("/dev/null");
		logtable[cproxylog].name= "Cproxy";

		logtable[stormlog].fd = sout;
		logtable[stormlog].name= "Storm";

		logtable[rainlog].fd = sout;
		logtable[rainlog].name= "Rain";

		logtable[reeflog].fd = openlog("/dev/null");
		logtable[reeflog].name= "Reef";

		logtable[ctreelog].fd = openlog("/dev/null");
		logtable[ctreelog].name= "Ctree";

		logtable[autolog].fd = openlog("/dev/null");
		logtable[autolog].name= "Auto";

		#logtable[paniclog].fd=openlog(CARIBE_LOGS+"/panic");
		logtable[paniclog].fd=sout;
		logtable[paniclog].name="Panic";
	}
}



Cmdargs.new(args:list of string):(ref Cmdargs,string)
{
	cmd:Cmdargs;
	cmd.loadfs=0;
	cmd.nat=0;
	cmd.static=0;
	cmd.netdbg=0;

	arg->init(args);
	while ((c := arg->opt()) !=0 )
		case c {
		'l' => cmd.loadfs=1;
		'n' => cmd.nat=1;
		's' => cmd.static=1;
		'd' => cmd.netdbg=1;
		* => return (nil,sprint("Unknow option: %c",c));
		}

	return (ref cmd,nil);
}


Cmdargs.print(cmd:self ref Cmdargs)
{
	if (cmd.loadfs==1)
		sys->print ("Load fs flag is activated\n");
	if (cmd.nat==1)
		sys->print ("Nat mode is activated\n");
	if (cmd.static==1)
		sys->print ("Static mode is activated\n");
	if (cmd.netdbg==1)
		sys->print ("Net debugging mode is activated\n");
}


checkinit(error:string)
{
	if (error!=nil)
		panic(error);
}


sysname():string
{
	if ( (flags!=nil) && (flags.nat))
	{
		if (ip!=nil)
			ip=natip();
		return ip;
	}

	# Only de hostname of the machine
	fd:=sys->open("/dev/sysname",sys->OREAD);
	if (fd==nil)
		return nil;
	buf:=array [255] of byte;
	n:=sys->read (fd, buf, len buf);

	sysname:=string buf[:n];

	# The qualified name of the machine (with dom)
	domname:=getenv("domname");
	if (domname!=nil)
		return domname;
	else
		return sysname;

}




splitaddr (addr:string):(string,string,string)
{
	for (i:=0;i<len addr;i++)
		if (addr[i]=='!')
			addr[i]=' ';

	ls:=str->unquoted(addr);
	if (ls==nil)
		return (nil,nil,nil);
	case (len ls)
	{
		1=>
			return (nil,hd ls, nil);
		2=>
			return (nil,hd ls,hd (tl ls));
		3 =>
			return (hd ls,hd (tl ls), hd(tl (tl ls)));
		* =>
			return (nil,nil,nil);
	}		
}


timeout (time:int):chan of int
{
	c:=chan [1] of int;
	spawn tout(time,c);
	return c;
}


tout(time:int,c:chan of int)
{
	sys->sleep(time);
	c<-=0;
}


args(l:list of string):array of string
{
	if (l==nil)
		return nil;
	a:=array [len l] of string;
	aux:=l;
	for (i:=0; i< len l; i++){
		a[i]=hd aux;
		aux=tl aux;
	}
	return a;
}


splitlines(text:string):list of string
{
	lines:list of string = nil;
	b:=0;

	for (i:=0;i<len text;i++)
		if (text[i]=='\n')
		{
			lines=text[b:i]::lines;
			b=i+1;
		}
	return lines;
}


cleanns(ns:string):string
{
	allwrite := Sys->nulldir;
	allwrite.mode = 8r777 | Sys->DMDIR;

	(d, n) := readdir->init(ns, Readdir->NONE|Readdir->COMPACT);
	for(i := 0; i < n; i++){
		path := ns+"/"+d[i].name;
		if(d[i].mode & Sys->DMDIR)
			cleanns(path);
		else
			sys->remove(path);
	}
	sys->remove(ns);	

	return nil;
}


mkns (ns:string):string
{
	for (i:=0;i<len ns;i++)
		if (ns[i]=='/')
			ns[i]=' ';

	ls:=str->unquoted(ns);
	if (ls==nil)
		return sprint("mkns: ns path bad formed: %s",ns);
	
	path:="";
	while (ls!=nil)
	{
		d:=hd ls;
		path=path+"/"+d;
		(es,dir):=sys->stat(path);
		if (es<0)
			if (sys->create(path,sys->OREAD,8r777| sys->DMDIR)==nil)
				return sprint("mkns: error creating ns %s",path);
		ls=tl ls;
	}

	return nil;
}


conflictpath(path:string):string
{
	s:="/";
	if (len(path)==0)
		return nil;

	for(i:=1;i<len(path);i++)
	{
		if (path[i]=='/')
			s[i]='!';
		else
			s[i]=path[i];
	}
	return s;
}



copyfile (f1,f2:string):int
{
	# Abro fichero origen
	fd1:=sys->open(f1,sys->OREAD);
	if (fd1==nil)
		return -1;
	(e,d1):=sys->stat(f1);

	# Abro fichero destino
	fd2:=sys->create (f2,sys->OWRITE | sys->OTRUNC,d1.mode);
	if (fd2==nil)
		return -2;

	# Copio contenido
	buf:=array [LENBUF] of byte;
	while ((nr := sys->read(fd1,buf,len buf))>0)
		if (sys->write(fd2,buf,nr) != nr)
			return -1;
	if (nr <0)
		return -3;

	# Ahora f1 y f2 son copias exactas. Los metadatos no los
	# actualizo pues wstat y fwstat estaban dando fallo. Algunos
	# metadatos no pueden ser actalizados (man stat)
	
	return 0;		
}



log (log:int, m:string)
{
	if ((log<0) || (log>MAXLOG))
		return;
	fd:=logtable[log].fd;
	name:=logtable[log].name;

	if ( (fd==nil) || (!vdebug) )
		return;

	t:=text(local(now()));
	ts:=str->unquoted(t);
	for (i:=0;i<3;i++)
		ts=tl ts;
	sys->fprint (fd,"[%s][%s] %s\n",hd ts,name,m);
	
	## solo para depurar en los casos en que la consola
	## desaparece
	if (log==stdout)
		sys->fprint (logtable[stderr].fd,"[%s][%s] %s\n",hd ts,name,m);
}


writeline (fd: ref Sys->FD, m:string):int
{
	if (fd!=nil)
	{
		m=m+"\n";	
		buf:=array of byte m;
		return sys->write(fd, buf, len buf);
	}
	return -1;
}



openlog(name:string):ref sys->FD
{
	lg := sys->open(name,sys->OWRITE | sys->OTRUNC);
	if (lg == nil)
		lg=sys->create(name,sys->OWRITE,8r644);
	return lg;
}




panic (e:string)
{
	log (paniclog,sprint("Panic error: %s",e));

	pid:=sys->pctl(0,nil);
	p:="/prog/"+string pid+"/ctl";
	b:="killgrp";

	pfd:=sys->open(p,sys->OWRITE);
	sys->write(pfd,array of byte b,len b);
}



killgrp(pid:int):string
{
	buf:= array [100] of byte;

	pr:=sys->open("/prog", sys->OREAD);
	if (pr==nil)
		return "killgrp cannot open /prog";

	(procs,n):=readdir->readall(pr,readdir->NAME);
	if ((procs==nil) || (n<0))
		return "killgrp cannot read /prog/";

	for (i:=0; i<n; i++)
	{
		pfile:="/prog/"+procs[i].name+"/pgrp";
		fd:=sys->open(pfile, sys->OREAD);
		if (fd==nil)
			continue;
		b:=sys->read(fd,buf,100);
		if (b<0)
			return sprint("killgrp cannot read proc file %s",pfile);
		(grp,err):=str->toint(string buf[:b],10);
		if ( (err==nil) && (grp==pid))
		{
			(pd,err):=str->toint(procs[i].name,10);
			if (err==nil)
				if (killproc(pd)<0)
					return sprint("killgrp cannot kill prog %d",pd);
		}
	}
	
	return nil;
}



killproc(pid:int):int
{
	msg:="kill";
	procfile:="/prog/"+string pid+"/ctl";
	(e,d):=sys->stat(procfile);
	if (e<0)	# no existe el fichero
		return -1;

	fd:=sys->open(procfile, sys->OWRITE);
	if (fd!=nil)
		n:=sys->write (fd,array of byte msg, len msg);
	
	return 0;
}



dropprefix(prefix:string, src:string):string
{
	l:=len src - len prefix;
	rpath:="";
	if (str->prefix(prefix,src)){
		for (i:=0; i<l ;i++)
			rpath[i]=src[i+len prefix];
		return rpath;
	}else{
		return src;
	}
}


getclient (conn: Sys->Connection): (string,string)
{
	cbuf:=array [512] of byte;
	rfd :=sys->open(conn.dir + "/remote", Sys->OREAD);
	if (rfd==nil)
		return (nil,nil);

	n := sys->read(rfd,cbuf, len cbuf);
	if (n<0)
		return (nil,nil);

	client:=string cbuf[:n];
	(ip,nil):=str->splitl(client,"!");
	(nil,port):=str->splitr(client,"!");

	return (ip,port);

}

#
# Devuelve una direccion siempre y cuando
# el nombre este en el servicio de DNS
#

resolvname (name:string):string
{
	if (name==nil)
		return nil;

	query:=name+" ip";
	dns:=sys->open("/net/dns", Sys->ORDWR);
	if (dns==nil)	
	{
		log (stdout,"No se encontro /net/dns");
		return nil;
	}
	b:= array of byte query;
	if (sys->write(dns,b,len b)>0)
	{
		sys->seek(dns,big 0, Sys->SEEKSTART);
		buf:=array[256] of byte;
		n:=sys->read(dns,buf,len buf);
		msg:=string buf[0:n];
		l:=str->unquoted(msg);
		ip:=hd (tl tl( l));
		return ip;
	}
	
	log (stdout,"No se resolvio");
	return nil;	
}


sysaddr ():string
{
	return resolvname(sysname());
}


#
# Devuelve una direccion siempre y cuando
# el nombre este registrado en el ndb/local
# de la máquina
#

# hostip (host:string):string
# {
# 	db:=Db.open("/lib/ndb/local");
# 	
# 	(entry,ptr):=db.findpair(nil,"sys",host);
# 	if (entry!=nil)
# 	{
# 		tuplist:=entry.findpair("sys","bach");
# 		for (tup:=hd tuplist; tuplist!=nil; tuplist=tl tuplist);
# 		{
# 			att:=tup.find ("ip");
# 			if (att!=nil)
# 				return (hd att).val;
# 		}
# 	}
# 
# 	(entry,ptr)=db.findpair(nil,"dom",host);
# 	if (entry!=nil)
# 	{
# 		tuplist:=entry.findpair("dom","bach.lsub.org");
# 		for (tup:=hd tuplist; tuplist!=nil; tuplist=tl tuplist);
# 		{
# 			att:=tup.find ("ip");
# 			if (att!=nil)
# 				return (hd att).val;
# 		}
# 	}
# 
# 	return nil;
# }




#
# Devuelve la dirección pública del host, utilizando
# un servicio web externo
#

natip():string
{
	service:="http://whatismyip.org";

	sh->run(nil,"webgrab"::"-o"::"/tmp/ip"::service::nil);
	
	fd:=sys->open("/tmp/nat_ip",Sys->OREAD);
	if (fd==nil)
		return nil;

	buf:=array [16] of byte;
	n:=sys->read(fd,buf,16);
	ip:=string buf[:n];
	sys->remove("/tmp/nat_ip");

	return ip;
}



size (name:string): big
{
	sz: big;

	(rc,d):=sys->stat(name);
	if (rc<0)
		return big -1;

	if (d.mode & sys->DMDIR)
		sz=dirsize(name);
	else
		sz=d.length;

	return sz/big 1024;
}


dirsize(dirname: string): big
{
	prefix := dirname+"/";
	if(dirname==".")
		prefix = nil;
	sum := big 0;
	(de, nde) := readdir->init(dirname, readdir->NAME);
	if(nde < 0)
		return big -1;
	

	for(i := 0; i < nde; i++) {
		s := prefix+de[i].name;
		if(de[i].mode & Sys->DMDIR){
			size := dirsize(s);
			sum = sum+size;
		}else{
			l := de[i].length;
			sum = sum+l;
		}
	}
	return sum;
}



rawstr2pairs(raw:string):list of (string,string)
{
	lp: list of (string,string);

	(t,err):=attrdb->parseline(raw,0);
	if (t!=nil)
	{
		while (t.pairs!=nil)
		{
			p:=hd t.pairs;
			lp=(p.attr,p.val)::lp;
			t.pairs=tl t.pairs;
		}
	}

	return lp;
}


pairs2rawstr (pairs: list of (string,string)):string
{
	s:="";
	while (pairs!=nil)
	{
		(att,val):=hd pairs;
		s=s+att+"="+val+" ";
		pairs=tl pairs;
	}

	return s;
}


isignoredpath(ignored: array of string, path:string):int
{
	for (i:=0; i<len(ignored); i++)
		if (str->prefix(ignored[i],path))
			return 1;
	return 0;

}





